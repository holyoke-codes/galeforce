# GaleForce

GaleForce is a educational robotics adventure game designed to teach players about robotics as they help prepare for and respond to a hurricance as part of a team of first responders.

## Getting started

Learn more about GaleForce, get lesson plans, use the dashboard to manage the game, and download the game at [GaleForceGame.com](https://galeforcegame.com).

## Installation
GaleForce runs on Windows and Mac. Chromebooks are not supported. Linux is not currently supported due to a dependency on Vuplex. 

[Installation instructions](https://docs.google.com/document/d/1z15faTxmZZjhHjYTaihYUP8POXsG4rf1xM6sNtFBTyg/edit)

## Support
Support is available through gitlab issues or the contact form on galeforcegame.com.

## Contributing
Contributions are welcome! The game is built with Unity 2020.1.17f1. Please get in touch if you're interested in being involved in the project.

## Authors and acknowledgment
This project was created by Jacob Bashista, Andrew Pasquale, Beryl Hoffman, and Lissie Fein in collaboration with UMass and Elms College as part of a project funded by a grant from the NSF and The REBLS Network.
