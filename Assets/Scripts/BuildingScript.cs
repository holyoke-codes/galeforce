﻿using UnityEngine;
using System.Collections;

public class BuildingScript : MonoBehaviour
{
    public Color EmissionColorValue;
    public float Intensity;
    public bool Cleared = false;
    private Material _mat;

    // Start is called before the first frame update
    void Start()
    {
        if (GetComponent<Renderer>())
        {
            _mat = GetComponent<Renderer>().material;
        }
    }

    public void Clear()
    {
        if (!Cleared)
        {
            if (_mat)
            {
                _mat.SetVector("_EmissionColor", EmissionColorValue * Intensity);
                _mat.EnableKeyword("_EMISSION");
            }
        }
        Cleared = true;
    }

    public bool IsCleared => Cleared;

    public void Reset()
    {
        StartCoroutine(ResetCoroutine());
    }

    IEnumerator ResetCoroutine()
    {
        yield return new WaitForSeconds(0.1f);
        _mat.DisableKeyword("_EMISSION");
        Cleared = false;
    }

    private void OnDestroy()
    {
        Destroy(_mat);
    }
}
