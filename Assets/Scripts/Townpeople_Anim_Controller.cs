﻿using System.Collections;
using UnityEngine;


public class Townpeople_Anim_Controller : MonoBehaviour
{
    public Animator Anim;
    public float duration = 5;
    private int rnd;
    private bool w = true;

    void Start()
    {
        Anim = GetComponent<Animator>();
    }

    void Update()
    {
        if (w == true)
        {
            w = false;
            StartCoroutine("Timer");
        }
    }

    IEnumerator Timer()
    {
        yield return new WaitForSeconds(duration);
        rnd = Random.Range(1, 8);
        //Debug.Log(rnd);
        switch (rnd)
        {
            case 1:
                Anim.SetTrigger("Waving");
                break;
            case 2:
                Anim.SetTrigger("Cheering");
                break;
            case 3:
                Anim.SetTrigger("Backflip");
                break;
            case 4:
                Anim.SetTrigger("Silly Dance");
                break;
            case 5:
                Anim.SetTrigger("Twist Dance");
                break;
            case 6:
                Anim.SetTrigger("Fist Pump");
                break;
            case 7:
                Anim.SetTrigger("Step Dance");
                break; 
        }
        w = true;
    }


}
