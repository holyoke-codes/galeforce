﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClampRotation : MonoBehaviour
{
    public float maxRotation = 0.3f;

    // Update is called once per frame
    void Update()
    {
        // Clamp rotation so you can't flip upside down or roll over.
        float x = Mathf.Clamp(transform.rotation.x, -maxRotation, maxRotation);
        float z = Mathf.Clamp(transform.rotation.z, -maxRotation, maxRotation);
        transform.rotation = new Quaternion(x, transform.rotation.y, z, transform.rotation.w);
    }

}
