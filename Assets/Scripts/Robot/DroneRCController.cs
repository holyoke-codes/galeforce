﻿using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(PlayerInput))]
[RequireComponent(typeof(Rigidbody))]

public class DroneRCController : MonoBehaviour
{
    [Header("Control Properties")]
    [SerializeField] public float rotationSpeed = 90f;
    [SerializeField] private float minMaxPitch = 30f;
    [SerializeField] private float minMaxRoll = 30f;
    [SerializeField] private float yawPower = 4f;
    [SerializeField] private float maxPower = 4f;
    [SerializeField] private float lerpSpeed = 2f;

    private Rigidbody _rb;
    private Vector2 _cyclic;
    private float _heading;
    private float _throttle;

    public Vector2 Cyclic { get => _cyclic; }
    public float Heading { get => _heading; }
    public float Throttle { get => _throttle; }

    protected float startDrag;
    protected float startAngularDrag;

    private float _finalPitch;
    private float _finalRoll;
    private float _yaw;
    private float _finalYaw;

    private void Start()
    {
        _rb = GetComponent<Rigidbody>();
        startDrag = _rb.drag;
        startAngularDrag = _rb.angularDrag;
    }

    public void FixedUpdate()
    {
        HandleMovement();
    }

    protected virtual void HandleMovement()
    {
        // Manage throttle automatically during pitch or roll movement
        Vector3 _up = transform.up;
        _up.x = 0;
        _up.z = 0;
        float diff = 1 - _up.magnitude;

        _rb.AddForce(transform.up * ((_rb.mass * Physics.gravity.magnitude) + (diff * Physics.gravity.magnitude) + (Throttle * maxPower)), ForceMode.Force);

        float pitch = Cyclic.y * minMaxPitch;
        float roll = Cyclic.x * -minMaxRoll;
        _yaw += Heading * yawPower;

        _finalPitch = Mathf.Lerp(_finalPitch, pitch, Time.deltaTime * lerpSpeed);
        _finalRoll = Mathf.Lerp(_finalRoll, roll, Time.deltaTime * lerpSpeed);
        _finalYaw = Mathf.Lerp(_finalYaw, _yaw, Time.deltaTime * lerpSpeed);

        Quaternion rot = Quaternion.Euler(_finalPitch, _finalYaw, _finalRoll);
        _rb.MoveRotation(rot);
    }

    private void OnCyclic(InputValue value)
    {
        _cyclic = value.Get<Vector2>();
    }

    private void OnHeading(InputValue value)
    {
        _heading = value.Get<float>();
    }

    private void OnThrottle(InputValue value)
    {
        _throttle = value.Get<float>();
    }
}