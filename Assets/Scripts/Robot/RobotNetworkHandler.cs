﻿using Photon.Pun;
using UnityEngine;

public class RobotNetworkHandler : MonoBehaviour, IPunObservable
{
    public bool isDriving;

    public PhotonView view;


    private void Start()
    {
        view = GetComponent<PhotonView>();
    }


    void IPunObservable.OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(isDriving);
        }
        else if (stream.IsReading)
        {
            isDriving = (bool)stream.ReceiveNext();
        }
    }

    private void Update()
    {
        if (isDriving)
        {
            // Disable gravity if someone else is controlling the drone
            if (gameObject.GetComponent<DroneRCController>())
            {
                GetComponent<Rigidbody>().useGravity = (view.IsMine) ? true : false;
            }

        }
    }
}