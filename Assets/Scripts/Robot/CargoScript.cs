﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CargoScript : MonoBehaviour
{

    public void LoadCargo(string type)
    {
        transform.Find(type).gameObject.SetActive(true);
    }

    public void UnloadCargo(string type)
    {
        transform.Find(type).gameObject.SetActive(false);
    }
}
