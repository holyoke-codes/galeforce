﻿using UnityEngine;

public class RobotRCController : MonoBehaviour
{
    public float movementSpeed = 3f;

    private Rigidbody _rigidbody;
    public float rotationSpeed = 90f;

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }


    public void FixedUpdate()
    {
        HandleMovement();
    }

    protected virtual void HandleMovement()
    {
        var wantedPosition = transform.position +
                             transform.forward * Input.GetAxis("Vertical") * movementSpeed * Time.deltaTime;
        _rigidbody.MovePosition(wantedPosition);

        var wantedRotation = transform.rotation *
                             Quaternion.Euler(Vector3.up *
                                              (rotationSpeed * Input.GetAxis("Horizontal") * Time.deltaTime));
        _rigidbody.MoveRotation(wantedRotation);
    }
}