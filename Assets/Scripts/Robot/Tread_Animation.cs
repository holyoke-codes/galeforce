﻿using UnityEngine;

public class Tread_Animation : MonoBehaviour
{
  public float Speed = 0.1f;
  Material treads;

  void Start()
  {
    Material[] materials = GetComponent<Renderer>().materials;
    foreach(Material material in materials)
    {
        if (material.name == "Treads (Instance)")
        {
            treads = material;
            break;
        }
    }
  }

  void Update()
  {
    float OffsetY = treads.mainTextureOffset.y + Speed;
    treads.mainTextureOffset = new Vector2(0.0f, OffsetY);
  }

}
