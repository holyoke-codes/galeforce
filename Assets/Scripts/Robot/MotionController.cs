﻿using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class MotionController : MonoBehaviour
{
    public float currentLocalXPosition;

    public float currentRotation;
    public float drivingSpeed = 0.01f;
    public List<Command> instructions;

    public bool isDriving;

    private Rigidbody _rigidbody;

    private bool _rotating;

    public float rotationSpeed = 20f;
    public float scalingFactor = 0.1f;
    public float targetLocalXPosition;
    private float _targetRotation;

    public PhotonView view;

    // Start is called before the first frame update
    private void Start()
    {
        currentRotation = transform.eulerAngles.y;
        currentLocalXPosition = transform.localPosition.x;
        _rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    private void Update()
    {
        currentRotation = transform.eulerAngles.y;
        currentLocalXPosition = transform.localPosition.x;
        if (_rotating)
        {
            var to = new Vector3(0, _targetRotation, 0);
            if (Vector3.Distance(transform.eulerAngles, to) > 0.01f)
            {
                transform.eulerAngles =
                    Vector3.MoveTowards(transform.rotation.eulerAngles, to, Time.deltaTime * rotationSpeed);
            }
            else
            {
                transform.eulerAngles = to;
                _rotating = false;
            }
        }

        if (!isDriving) return;
        if (currentLocalXPosition < targetLocalXPosition - 0.01f)
            _rigidbody.MovePosition(transform.position + transform.forward * drivingSpeed);
        else if (currentLocalXPosition > targetLocalXPosition + 0.01f)
            _rigidbody.MovePosition(transform.position + -transform.forward * drivingSpeed);
        else
            isDriving = false;
    }


    public void Turn(float degrees)
    {
        if (view.Owner == PhotonNetwork.LocalPlayer)
        {
            _rotating = true;
            _targetRotation = currentRotation + degrees;
        }
        else
        {
            view.TransferOwnership(PhotonNetwork.LocalPlayer);
            _rotating = true;
            _targetRotation = currentRotation + degrees;
        }
    }

    public void Drive(float distance)
    {
        if (view.Owner == PhotonNetwork.LocalPlayer)
        {
            isDriving = true;
            targetLocalXPosition = currentLocalXPosition + distance * scalingFactor;
        }
        else
        {
            view.TransferOwnership(PhotonNetwork.LocalPlayer);
            isDriving = true;
            targetLocalXPosition = currentLocalXPosition + distance * scalingFactor;
        }
    }
}

public readonly struct Command
{
    public Command(string task, float value)
    {
        TASK = task;
        VALUE = value;
    }

    public string TASK { get; }
    public float VALUE { get; }

    public override string ToString()
    {
        return $"({TASK}, {VALUE})";
    }
}