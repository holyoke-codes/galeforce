﻿using UnityEngine;

public class PickupDebris : MonoBehaviour
{
    private Debris _debris;

    private void Update()
    {
        if (_debris == null) return;
        if (Input.GetKeyDown(KeyCode.K))
            _debris.Clear();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Debris") _debris = collision.gameObject.GetComponent<Debris>();
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Debris") _debris = null;
    }
}