﻿using System;
using RSG;
using UnityEngine;
using SensorToolkit;
using System.Collections.Generic;
using System.Linq;
using Photon.Pun;


public class BlockRobotController : MonoBehaviour
{
    public Transform StartLocation;
    public GameObject Origin;
    public NetworkCargoHandler NetworkCargoHandler;

    // Cargo
    public GameObject Cargo;
    public List<GameObject> _cargoController = new List<GameObject>();

    // Driving
    public float RobotSpeed = 2.5f;
    private bool _isDriving;
    private float _distanceToTravel;
    private float _distanceTraveled;
    private int _directionToTravel; // 1 = forward, -1 = backward
    private bool _justOnce;

    // Rotation
    public float RobotRotationSpeed = 15f;
    public float RotationTolerance = 0.001f; // Get this close to 180 degrees
    private bool _isRotating;
    private float _degrees; // Requested turn in Euler degrees
    private Quaternion _startingRotation;
    private Quaternion _rotateTarget;
    private int _reps; // requested degrees mod ~ 180
    private int _count = 0; // Number of reps completed so far

    // Stop
    private bool _isStopping;

    // Sensor
    public RaySensor Sensor;
    public float TargetDistanceToCurb;
    public float P = 3f;
    private bool _isCurbFollowing;

    // Drone
    private bool _isFlyingUpOrDown;
    private bool _isStrafing;
    private bool _isLanding;
    private bool _isFlyingTo;
    private Coords _coords;
    Vector3 _target;

    private Rigidbody _rigidbody;
    private GameObject _cargo;
    private Promise<bool> _promise;
    private RobotInventory _robotInventory;
    public string MissionId;

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();

        if (transform.Find("Cargo"))
        {
            _cargo = transform.Find("Cargo").gameObject;
        }
        _robotInventory = GetComponent<RobotInventory>();
    }

    // robot's current position
    public Vector3 GetPosition()
    {
        return transform.position;
    }

    // Reset back to start point on green flag
    public IPromise<bool> Reset()
    {
        if (_promise != null && _promise.CurState == PromiseState.Pending)
        {
            _promise.Reject(new Exception());
        }
        _promise = new Promise<bool>();

        // Take ownership of the robot if needed
        if (!gameObject.GetComponent<PhotonView>().IsMine)
        {
            gameObject.GetComponent<PhotonView>().TransferOwnership(PhotonNetwork.LocalPlayer);

            // TODO: I think this code isn't used anymore. Remove it?
            // Take ownership of other mision assets - checking for these in MissionReset()
            //GameObject[] missionAssets = GameObject.FindGameObjectsWithTag("MissionAssets");
            //foreach (GameObject asset in missionAssets)
            //{
                //Debug.Log(asset);
                //if (asset.GetComponent<PickupZone>() && asset.GetComponent<PickupZone>().MissionId == MissionId)
                //{
                //    asset.GetComponent<PhotonView>().TransferOwnership(PhotonNetwork.LocalPlayer);
                //}
                //else if (asset.GetComponent<DeliveryZone>() && asset.GetComponent<DeliveryZone>().MissionId == MissionId)
                //{
                //    asset.GetComponent<PhotonView>().TransferOwnership(PhotonNetwork.LocalPlayer);
                //}
            //}
        }

        _isDriving = false;
        _isCurbFollowing = false;
        _isRotating = false;
        _isStrafing = false;
        _isFlyingUpOrDown = false;
        _isFlyingTo = false;
        _isLanding = false;

        ClearDeliveredCargo();

        transform.position = StartLocation.position;
        transform.rotation = StartLocation.rotation;

        _promise.Resolve(false);
        return _promise;
    }

    public IPromise<bool> Pickup()
    {
        _promise = new Promise<bool>();

        StartCoroutine(waiter(_promise));
        return _promise;
    }

    private IEnumerator<WaitForSeconds> waiter(RSG.Promise<bool> _promise)
    { 
        yield return new WaitForSeconds(1);

        bool canPickup = _robotInventory.PickupItem();
        Debug.Log(canPickup);
        Debug.Log(_cargo);
        if (_cargo && canPickup)
        {
            _cargo.SetActive(true);
            NetworkCargoHandler.isCarryingCargo = true;
        }
        _promise.Resolve(false);
    }

    public IPromise<bool> DropOff()
    {
        _promise = new Promise<bool>();

        bool canDropoff = _robotInventory.DropOffItem();

        // Instantiate a cargo object on the map when a delivery is made.
        GameObject activeMission = GameObject.FindGameObjectWithTag("ActiveMission");
        if (activeMission != null && canDropoff)
        {
            var missionType = activeMission.GetComponent<MissionController>().MissionType;
            var cargo = Resources.Load(missionType.ToString());
            
            if (cargo)
            {
                Vector3 pos = transform.position;
                Quaternion rot = transform.rotation;
                if (missionType.Equals(MissionTypes.Delivery))
                {
                    pos += Vector3.up * 1f;
                    // Rotation the shipping container
                    rot *= Quaternion.Euler(90, 90, 0);
                }
                if (missionType.Equals(MissionTypes.DroneDelivery))
                {
                    // drone delivers underneath
                    pos += Vector3.up * 0.5f;
                }
                else
                {
                    // other robots deliver 3 meters to the right of the robot
                    pos += transform.right * 3;
                }
                // let's try networking delivery cargo
                GameObject deliveryObj = null;
                // custom data 
                string activeMissionID = activeMission.GetComponent<MissionController>().missionID;
                // -1  for number of items
                // custom data - last one is for delivery true
                object[] data = new object[] { MissionId, PlayerInstance.Instance.ActiveTeam, true };
                Debug.Log($"Custom data passed to delivery object {data[0]} {data[1]} {data[2]}");

                deliveryObj = PhotonNetwork.Instantiate(cargo.name, pos, rot, 0, data);
                //GameObject delivery = Instantiate(cargo, pos, rot) as GameObject; 
                _cargoController.Add(deliveryObj);
            }
        }

        // If the robot has a cargo container displaying while carrying, hide it again.
        if (_cargo)
        {
            _cargo.SetActive(false);
            NetworkCargoHandler.isCarryingCargo = false;
        }

        _promise.Resolve(false);
        return _promise;
    }

    // direction is 1 if forward, -1 if reverse
    public IPromise<bool> Drive(float distance, int direction)
    {
        _promise = new Promise<bool>();

        _distanceTraveled = 0;
        _distanceToTravel = distance;
        _directionToTravel = direction;
        _isDriving = true;

        return _promise;
    }

    public IPromise<bool> StopDriving()
    {
        _promise = new Promise<bool>();
        _isStopping = true;
        return _promise;
    }

    public IPromise<bool> Turn(float degrees)
    {
        _promise = new Promise<bool>();
        // Use Euler angles and modulo to decide how many times to rotate.
        // If the angle is 180 degrees, Unity may decide to rotate in either direction.
        // We will turn a little more or less than 180 so it turns in the desired direction.
        _degrees = degrees;
        _startingRotation = transform.rotation;
        float startingDegree = transform.rotation.eulerAngles.y;
        float finalHeading = startingDegree + degrees;
        _rotateTarget = Quaternion.Euler(0f, finalHeading, 0f);
        _reps = Math.Abs((int)(degrees / (180 - RotationTolerance)));
        _isRotating = true;
        _count = 0;
        return _promise;
    }

    public IPromise<bool> FollowCurbFor(float distance)
    {
        _promise = new Promise<bool>();
        _distanceToTravel = distance;
        _distanceTraveled = 0;
        _directionToTravel = 1; // Drive forward
        _isCurbFollowing = true;
        _isDriving = true;
        return _promise;
    }

    public IPromise<bool> FollowCurb()
    {
        _promise = new Promise<bool>();
        _isCurbFollowing = true;
        _isDriving = true;
        _directionToTravel = 1;
        _distanceToTravel = 0;
        _justOnce = true;
        return _promise;
    }

    public List<string> SensorSees()
    {
        List<string> hits = new List<string>();
        if (Sensor)
        {
            List<RaycastHit> raycastHits = Sensor.DetectedObjectRayHits;
            hits = raycastHits.Select(hit => hit.collider.tag).ToList();
          /*  string tags = "Hits: ";
            foreach (string h in hits)
            {
                if (h != "Curb")
                   tags += h + " ";
            }
            if (tags != "Hits: ")
               Debug.Log(tags);*/
        }
        return hits;
    }

    public IPromise<bool> Altitude(float distance, int direction)
    {
        _promise = new Promise<bool>();

        _distanceTraveled = 0;
        _distanceToTravel = distance;
        _directionToTravel = direction;
        _isFlyingUpOrDown = true;

        return _promise;
    }

    public IPromise<bool> Strafe(float distance, int direction)
    {
        _promise = new Promise<bool>();

        _distanceTraveled = 0;
        _distanceToTravel = distance;
        _directionToTravel = direction;
        _isStrafing = true;

        return _promise;
    }

    public IPromise<bool> Land()
    {
        _promise = new Promise<bool>();

        _distanceTraveled = 0;
        _distanceToTravel = 0;
        _directionToTravel = -1;
        _isFlyingUpOrDown = true;
        _isLanding = true;

        return _promise;
    }

    public IPromise<bool> FlyTo(Coords coords)
    {
        _promise = new Promise<bool>();

        _target = Origin.transform.position + new Vector3(coords.x, coords.z, coords.y);
        _isFlyingTo = true;

        return _promise;
    }

    public void PickUp(string obj)
    {
        if (_cargo) _cargo.SetActive(true);
    }

    public void PutDown(string obj)
    {
        if (_cargo) _cargo.SetActive(false);
    }

    private void FixedUpdate()
    {
        if (_isStopping)
        {
            _isDriving = false;
            _isCurbFollowing = false;
            _isStopping = false;
            _isFlyingTo = false;
            _isFlyingUpOrDown = false;
            _isRotating = false;
            _isStrafing = false;
            if (_promise.CurState == PromiseState.Pending)
            {
                _promise.Resolve(false);
            }
        }
        else
        {
            if (Sensor)
            {
                List<RaycastHit> hits = Sensor.DetectedObjectRayHits;

                if (_isCurbFollowing)
                {
                    foreach (var hit in hits)
                    {
                        if (hit.collider.tag == "Curb")
                        {
                            // If we can see the curb, rotate to adjust steering and follow the curb.
                            // Note: this will drive straight if there isn't a visible curb. 

                            // Rotation vector on the Y axis
                            Vector3 rotationSpeed = new Vector3(0, 10, 0);
                            // Calculate difference from our target value and multiply by proporionate value
                            float angularVelocity = (TargetDistanceToCurb - hit.distance) * -P;
                            Quaternion deltaRotation = Quaternion.Euler(angularVelocity * rotationSpeed * Time.deltaTime);
                            _rigidbody.MoveRotation(_rigidbody.rotation * deltaRotation);
                        }
                    }
                }
            }

            if (_isRotating)
            {
                // Deal with turns of 180 or more by turning close to 180 degrees * requested turn amount % 180
                if (_count < _reps)
                {
                    // Which direction is the robot turning?
                    if (_degrees < 0)
                    {
                        // Move counter-clockwise for almost 180 degrees
                        Quaternion targetRotation = _startingRotation * Quaternion.Euler(0f, 180f + RotationTolerance, 0f);
                        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, Time.deltaTime * RobotRotationSpeed);
                        if (transform.rotation == targetRotation)
                        {
                            // Reset the starting rotation
                            _startingRotation = targetRotation;
                            _count += 1;
                        }
                    }
                    else
                    {
                        // Move clockwise for almost 180 degrees
                        Quaternion targetRotation = _startingRotation * Quaternion.Euler(0f, 180f - RotationTolerance, 0f);
                        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, Time.deltaTime * RobotRotationSpeed);
                        if (transform.rotation == targetRotation)
                        {
                            // Reset the starting rotation
                            _startingRotation = targetRotation;
                            _count += 1;
                        }
                    }
                }
                else
                {
                    // Any remaining angle to rotate is less than 180 degrees.
                    // We can just turn towards it.
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, _rotateTarget, Time.deltaTime * RobotRotationSpeed);
                }
                // Dot product of two equal quaternions is 1
                if (_count == _reps && Math.Abs(Quaternion.Dot(transform.rotation, _rotateTarget)) > 1 - RotationTolerance)
                {
                    _isRotating = false;
                    if (_promise.CurState == PromiseState.Pending)
                    {
                        _promise.Resolve(false);
                    }
                }
            }  
        }

        if (_isDriving)
        {
            MoveRobot(transform.forward);
        }

        if (_isFlyingUpOrDown)
        {
            MoveRobot(transform.up);      
        }

        if (_isStrafing)
        {
            MoveRobot(transform.right);
        }

        if (_isFlyingTo)
        {
            float step = RobotSpeed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, _target, step);

            // Determine which direction to rotate towards
            Vector3 targetDirection = _target - transform.position;

            // Rotate the forward vector towards the target direction by one step
            Vector3 newDirection = Vector3.RotateTowards(transform.forward, new Vector3(targetDirection.x, 0, targetDirection.z), Time.deltaTime * RobotRotationSpeed, 0.0f);
            transform.rotation = Quaternion.LookRotation(newDirection);

            if (Vector3.Distance(transform.position, _target) < 0.001f)
            {
                _isFlyingTo = false;
                _promise.Resolve(true);
            }
        }
    }

    private void MoveRobot(Vector3 direction)
    {
        if (_distanceToTravel > 0)
        {
            // A distance was specified
            if (Math.Abs(_distanceTraveled) < _distanceToTravel)
            {
                float step = RobotSpeed * Time.deltaTime;
                _rigidbody.MovePosition(transform.position + _directionToTravel * direction * step);
                _distanceTraveled += step;
            }
            else
            {
                _isFlyingUpOrDown = false;
                _isStrafing = false;
                _isDriving = false;
                _isCurbFollowing = false;
                if (_promise.CurState == PromiseState.Pending)
                {
                    _promise.Resolve(false);
                }
            }
        }
        else
        {
            // Just move
            var step = RobotSpeed * Time.deltaTime;
            _rigidbody.MovePosition(transform.position + _directionToTravel * direction * step);

            if (_justOnce)
            {
                _isFlyingUpOrDown = false;
                _isStrafing = false;
                _isDriving = false;
                _isCurbFollowing = false;
                if (_promise.CurState == PromiseState.Pending)
                {
                    _promise.Resolve(false);
                }
            }
        }
    }

    public void ClearDeliveredCargo()
    {
        // Reset any cargo that was delivered
        foreach (var t in _cargoController)
        {
            if (t)
                t.GetComponent<CargoToPickup>().DestroyCargo();
        }
        _cargoController.Clear();

        // Search for other delivery cargo owned by others 
        GameObject activeMission = GameObject.FindGameObjectWithTag("ActiveMission");
        if (activeMission != null)
        {
            var tag = activeMission.GetComponent<MissionController>().MissionType;
            if (tag != MissionTypes.Cellular && tag != MissionTypes.Debris
                && tag != MissionTypes.Map)
            {
                GameObject[] cargoObjs = GameObject.FindGameObjectsWithTag(tag.ToString());
                Debug.Log("Found " + cargoObjs.Length + " object(s) of tag " + tag + " delivery");
                foreach (GameObject cargo in cargoObjs)
                {
                    if (cargo.name.ToLower().StartsWith(tag.ToString().ToLower()) &&
                        cargo.GetComponent<CargoToPickup>().Team == PlayerInstance.Instance.ActiveTeam
                        && cargo.GetComponent<CargoToPickup>().MissionId == MissionId
                        && cargo.GetComponent<CargoToPickup>().delivery)
                    {
                        Debug.Log("Calling destroyCargo for someone else's cargo in ClearDeliveredCargo");
                        cargo.GetComponent<CargoToPickup>().DestroyCargo();
                    }
                }
            }
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (_isLanding)
        {
            _isLanding = false;
            _isFlyingUpOrDown = false;
            if (_promise.CurState == PromiseState.Pending)
            {
                _promise.Resolve(false);
            }
        }

        // Drive through other robots?
        if (collision.gameObject.tag.Contains("Robot"))
        {
            Physics.IgnoreCollision(collision.gameObject.GetComponent<Collider>(), GetComponent<Collider>());
        }
    }

}
