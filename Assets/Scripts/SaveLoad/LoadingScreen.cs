﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LoadingScreen : MonoBehaviour
{

    private float _nextActionTime = 0.0f;
    public float Period = 0.1f;

    private int _state = 0;

    public TMP_Text LoadingText;

    // Update is called once per frame
    void Update()
    {
        if (Time.time > _nextActionTime)
        {
            _nextActionTime += Period;
            switch (_state)
            {
                case 0:
                    LoadingText.text = "Loading";
                    break;
                case 1:
                    LoadingText.text = "Loading.";
                    break;
                case 2:
                    LoadingText.text = "Loading..";
                    break;
                case 3:
                    LoadingText.text = "Loading...";
                    break;
                default:
                    LoadingText.text = "Loading...";
                    break;
            }

            _state = _state < 3 ? _state += 1 : 0;
        }
    }
}
