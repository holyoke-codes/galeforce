﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData
{
    public string PlayerId;

    public GameData(string playerId)
    {
        PlayerId = playerId;
    }
}
