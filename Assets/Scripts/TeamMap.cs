﻿using UnityEngine;

public class TeamMap : MonoBehaviour
{
    public RenderTexture Base;
    public RenderTexture Team;
    public int FPS = 10;

    private Material _mat;

    private void Start()
    {
        _mat = GetComponent<MeshRenderer>().material;
        //InvokeRepeating("RenderCamera", 0, 1.0f / FPS);
    }

    private void RenderCamera()
    {
        RenderTexture tex = RenderTexture.GetTemporary(1080, 1080);
        //Graphics.CopyTexture(Team, tex);
        Graphics.CopyTexture(Base, tex);
        _mat.SetTexture("_MainTex", Base);
        RenderTexture.ReleaseTemporary(tex);
    }
}
