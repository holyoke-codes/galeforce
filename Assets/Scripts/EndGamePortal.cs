﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGamePortal : MonoBehaviour
{

    private BoxCollider _boxCollider;


    // Start is called before the first frame update
    void Start()
    {
        _boxCollider = GetComponent<BoxCollider>();
        TeacherDB.Instance.DbUpdated += NewDbUpdate;
        _boxCollider.enabled = false;
    }

    private void NewDbUpdate(object sender, DbUpdatedEventArgs args)
    {
        if (!_boxCollider.enabled && args.db.unlock_map)
        {
            _boxCollider.enabled = true;
        }
    }
}
