﻿using Photon.Pun;
using UnityEngine;

public class NetworkRobotSync : MonoBehaviour, IPunObservable
{
    public string MissionId;

    void Start()
    {
    }

    void IPunObservable.OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(MissionId);
            //Debug.Log("writing: " + MissionId);
        }
        else if (stream.IsReading)
        {
            MissionId = (string)stream.ReceiveNext();
            //Debug.Log("Reading: " + MissionId);
        }
    }
}
