using System.Collections;
using System.IO;
using System.Threading;
using Renci.SshNet;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;

public class AudioRecorder : MonoBehaviour
{
    private AudioClip _audioClip;

    private string _username = "unknown";
    private bool _isRecording = false;

    public bool Enabled = false;
    public int RecordingLength = 30;
    public static AudioRecorder Instance { get; private set; }

    private static string host = "104.238.220.132";
    private static int port = 44;
    private static string username = "galeforce_uploader";
    private static string password = "gfu712021";

    private void Awake()
    {
        if (Instance == null) { Instance = this; }
        else { Destroy(gameObject); }
        DontDestroyOnLoad(this.gameObject);
    }

    public void StartRecording(string username)
    {
        _username = username;
        if (Enabled && !_isRecording)
        {
            StartCoroutine(RecordClip());
            _isRecording = true;
        }
    }

    private IEnumerator RecordClip()
    {
        while (true)
        {
            Debug.Log("Starting Recording");
            _audioClip = Microphone.Start(null, false, RecordingLength, 44100);
            yield return new WaitForSeconds(RecordingLength);
            Debug.Log("Saving Recording");
            SaveRecording();
        }
    }

    private void SaveRecording()
    {
        System.DateTime localStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Local);
        int cur_time = (int)(System.DateTime.Now - localStart).TotalSeconds;
        string path = Application.persistentDataPath + "/Resources/" + $"{_username}_recording_{cur_time}.wav";
        SavWav.Save(path, _audioClip);

        Thread uploadThread = new System.Threading.Thread(delegate ()
        {
            using (var client = new SftpClient(host, port, username, password))
            {
                client.Connect();
                if (client.IsConnected)
                {
                    Debug.Log("Connected to SSH");

                    using (var fileStream = new FileStream(path, FileMode.Open))
                    {
                        client.BufferSize = 4 * 1024; // bypass Payload error large files
                        client.UploadFile(fileStream, Path.GetFileName(path));
                        Debug.Log("Uploaded file");
                        client.Disconnect();
                        client.Dispose();
                    }
                }
                else
                {
                    Debug.Log("Failed to Connect to SSH");
                }
            }
        });
        uploadThread.Start();
    }

    private void OnApplicationQuit()
    {
        if (Enabled)
        {
            Debug.Log("Saving Recording");
            SaveRecording();
        }
    }

}
