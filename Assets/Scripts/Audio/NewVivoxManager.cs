﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using RSG;
using UnityEngine;
using VivoxUnity;

public class NewVivoxManager : MonoBehaviour
{

    [Header("Vivox Config")] 
    public string TokenIssuer;
    public string TokenDomain;
    public string ServerUri;
    public string TokenKey;

    private Promise _promise;
    public static NewVivoxManager Instance { get; private set; }
    private Client _client;
    private ILoginSession _loginSession;
    private IChannelSession _channelSession;

    public string CurrentChannel = "";

    public IAudioDevices AudioInputDevices;

    private bool _leftText = false;
    private bool _leftAudio = false;
    private bool _leavingChannel = false;
    private string _displayName = "";

    public string VivoxId = "";
    private AccountId _localAccountId;

    public bool Muted;

    public Dictionary<string, VivoxUser> UsersInChannel =
        new Dictionary<string, VivoxUser>();

    public List<VivoxUser> Users = new List<VivoxUser>();

    private Stack<string> _channelsToJoin = new Stack<string>();

    private void Awake()
    {
        if (Instance == null) { Instance = this; }
        else { Destroy(gameObject); }
        DontDestroyOnLoad(this.gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        _client = new Client();
        _client.Uninitialize();
        _client.Initialize();
        AudioInputDevices = _client.AudioInputDevices;
        InvokeRepeating("ForceUpdateUsers", 1.0f, 1.0f);
        InvokeRepeating("AutoJoinLobbyOnLoss", 10.0f, 10.0f);
    }

    private void AutoJoinLobbyOnLoss()
    {
        if(CurrentChannel == "" && !_leavingChannel)
        {
            if(_leftAudio && _leftText)
            {
                JoinLobby();
            }
        }
    }

    private void ForceUpdateUsers()
    {
        //Debug.Log("Force Clearing User List");
        UsersInChannel.Clear();
        if (_channelSession == null) return;
        foreach (var channelSessionParticipant in _channelSession.Participants)
        {
            var username = channelSessionParticipant.Account.DisplayName;
            var vivoxId = channelSessionParticipant.ParticipantId;

            if (_localAccountId.Equals(channelSessionParticipant.Account))
            {
                VivoxId = vivoxId;
            }

            var user = new VivoxUser { DisplayName = username, VivoxId = vivoxId };

            UsersInChannel.Add(vivoxId, user);
        }
    }

    public void LoginUser(string displayName)
    {
        var id = Guid.NewGuid();

        var accountId = new AccountId(TokenIssuer, id.ToString(), TokenDomain, displayName);
        _displayName = displayName;
        _loginSession = _client.GetLoginSession(accountId);
        _loginSession.PropertyChanged += OnLoginSessionPropertyChanged;
        _loginSession.BeginLogin(new Uri(ServerUri), _loginSession.GetLoginToken(TokenKey, TimeSpan.FromHours(1)), ar =>
        {
            try
            {
                _loginSession.EndLogin(ar);
            }
            catch (Exception e)
            {
                Debug.LogError("Error: " + e.Message + "\nStack Trace:\n" + e.StackTrace);
                return;
            }

        });
    }

    private void OnLoginSessionPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
    {
        if ("State" == propertyChangedEventArgs.PropertyName)
        {
            switch (((ILoginSession) sender).State)
            {
                case LoginState.LoggingIn:
                    Debug.Log("VIVOX: Logging into Vivox");
                    break;

                case LoginState.LoggedIn:
                    Debug.Log("VIVOX: Logged into Vivox");
                    var login = (ILoginSession) sender;
                    _localAccountId = login.LoginSessionId;
                    JoinLobby();
                    break;

                case LoginState.LoggedOut:
                    Debug.Log("VIVOX: Logged out of Vivox");
                    break;
                default:
                    break;
            }
        }
    }

    public void ChangeChannel(string newChannel)
    {
        _channelsToJoin.Push(newChannel);
        if (!_leavingChannel)
        {
            LeaveChannels().Then(() =>
            {
                JoinChannel(_channelsToJoin.Pop());
            });
        }
    }

    private IPromise LeaveChannels()
    {
        _promise = new Promise();
        _leavingChannel = true;
        StartCoroutine("DisconnectAll");
        return _promise;
    }

    private IEnumerator DisconnectAll()
    {
        // Disconnect from channel
        _channelSession.Disconnect();
        yield return new WaitUntil(() => _leftAudio && _leftText);
        _loginSession.DeleteChannelSession(_channelSession.Channel);
        _channelSession.PropertyChanged -= OnLoginSessionPropertyChanged;
        _channelSession.Participants.AfterKeyAdded -= ParticipantAddedToChannel;
        _channelSession.Participants.BeforeKeyRemoved -= ParticipantRemovedFromChannel;
        _channelSession.Participants.AfterValueUpdated -= OnParticipantValueUpdated;
        UsersInChannel.Clear();
        _channelSession = null;
        _promise.Resolve();
    }

    private void JoinChannel(string channelName)
    {
        var channelId = new ChannelId(TokenIssuer, channelName, TokenDomain, ChannelType.NonPositional);
        _channelSession = _loginSession.GetChannelSession(channelId);

        // Subscribe to property changes for all channels.
        _channelSession.PropertyChanged += SourceOnChannelPropertyChanged;
        _channelSession.Participants.AfterKeyAdded += ParticipantAddedToChannel;
        _channelSession.Participants.BeforeKeyRemoved += ParticipantRemovedFromChannel;
        _channelSession.Participants.AfterValueUpdated += OnParticipantValueUpdated;

        // Connect to channel
        _channelSession.BeginConnect(true, true, true, _channelSession.GetConnectToken(TokenKey, TimeSpan.FromHours(1)), ar =>
        {
            try
            {
                _channelSession.EndConnect(ar);
            }
            catch (Exception e)
            {
                Debug.LogError("Error: " + e.Message + "\nStack Trace:\n" + e.StackTrace);
                return;
            }
        });
    }

    private void ParticipantAddedToChannel(object sender, KeyEventArg<string> keyEventArg)
    {
        ValidateArgs(new object[] { sender, keyEventArg });

        var source = (VivoxUnity.IReadOnlyDictionary<string, IParticipant>)sender;
        var participant = source[keyEventArg.Key];
        var username = participant.Account.DisplayName;
        var vivoxId = participant.ParticipantId;

        if (_localAccountId.Equals(participant.Account))
        {
            VivoxId = vivoxId;
        }

        var user = new VivoxUser { DisplayName = username, VivoxId = vivoxId };

        UsersInChannel.Add(vivoxId, user);
    }

    private void ParticipantRemovedFromChannel(object sender, KeyEventArg<string> keyEventArg)
    {
        ValidateArgs(new object[] { sender, keyEventArg });

        var source = (VivoxUnity.IReadOnlyDictionary<string, IParticipant>)sender;
        var participant = source[keyEventArg.Key];
        var username = participant.Account.DisplayName;
        var vivoxId = participant.ParticipantId;

        var user = new VivoxUser {DisplayName = username, VivoxId = vivoxId};

        UsersInChannel.Remove(vivoxId);
    }

    private void OnParticipantValueUpdated(object sender, ValueEventArg<string, IParticipant> valueEventArg)
    {
        ValidateArgs(new object[] {sender, valueEventArg}); //see code from earlier in post

        var source = (VivoxUnity.IReadOnlyDictionary<string, IParticipant>) sender;
        var participant = source[valueEventArg.Key];

        if (!UsersInChannel.ContainsKey(participant.ParticipantId)) return;
        var user = UsersInChannel[participant.ParticipantId];
        user.IsTalking = participant.SpeechDetected;
        UsersInChannel[participant.ParticipantId] = user;

    }

    private static void ValidateArgs(object[] objs)
    {
        foreach (var obj in objs)
        {
            if (obj == null)
                throw new ArgumentNullException(obj.GetType().ToString(), "Specify a non-null/non-empty argument.");
        }
    }

    private void JoinLobby()
    {
        JoinChannel("TrainingComplex");
    }

    private void SourceOnChannelPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
    {
        var channelSession = (IChannelSession)sender;

        if (propertyChangedEventArgs.PropertyName == "AudioState")
        {
            switch (channelSession.AudioState)
            {
                case ConnectionState.Connecting:
                    Debug.Log("Audio connecting in " + channelSession.Key.Name);
                    break;

                case ConnectionState.Connected:
                    Debug.Log("Audio connected in " + channelSession.Key.Name);
                    CurrentChannel = channelSession.Key.Name;
                    _leftAudio = false;
                    _leavingChannel = false;
                    _channelsToJoin.Clear();
                    TeacherDB.Instance.UpdatePlayerLocation(_displayName, channelSession.Key.Name);
                    break;

                case ConnectionState.Disconnecting:
                    Debug.Log("Audio disconnecting in " + channelSession.Key.Name);
                    break;

                case ConnectionState.Disconnected:
                    Debug.Log("Audio disconnected in " + channelSession.Key.Name);
                    CurrentChannel = _channelsToJoin.Peek();
                    _leftAudio = true;
                    break;
            }
        }

        if (propertyChangedEventArgs.PropertyName == "TextState")
        {
            switch (channelSession.TextState)
            {
                case ConnectionState.Connecting:
                    Debug.Log("Text connecting in " + channelSession.Key.Name);
                    break;

                case ConnectionState.Connected:
                    Debug.Log("Text connected in " + channelSession.Key.Name);
                    _leftText = false;
                    break;

                case ConnectionState.Disconnecting:
                    Debug.Log("Text disconnecting in " + channelSession.Key.Name);
                    break;

                case ConnectionState.Disconnected:
                    Debug.Log("Text disconnected in " + channelSession.Key.Name);
                    _leftText = true;
                    break;
            }
        }
    }

    void OnApplicationQuit()
    {
        _client.Uninitialize();
    }

    private void Update()
    {
        Users.Clear();
        foreach (var user in UsersInChannel.Values)
        {
            Users.Add(user);
        }
    }
}

[Serializable]
public class VivoxUser
{
    public string DisplayName;
    public string VivoxId;
    public bool IsTalking;
}
