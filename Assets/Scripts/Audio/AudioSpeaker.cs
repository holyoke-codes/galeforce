﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AudioSpeaker : MonoBehaviour
{

    public Image AudioStatusImage;

    public string VoiceID;

    public TMP_Text NameText;

}
