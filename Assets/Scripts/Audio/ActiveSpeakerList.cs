﻿using System.Collections.Generic;
using System.Linq;
using Photon.Pun;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ActiveSpeakerList : MonoBehaviour
{

    public GameObject AudioPlayerName;

    public GameObject CurrentSpeaker;

    Dictionary<string, GameObject> _playerNames =
        new Dictionary<string, GameObject>();

    private PlayFabLogin _playFabLogin;

    void Start()
    {
        _playFabLogin = FindObjectOfType<PlayFabLogin>();
    }

    private void OnEnable()
    {
        if (!NewVivoxManager.Instance) return;

        if (!GetComponent<RectTransform>()) return;

        GetComponent<RectTransform>().sizeDelta = new Vector2(185, NewVivoxManager.Instance.UsersInChannel.Count * 50);

        var index = 0;
        // NetworkPlayer[] players = FindObjectsOfType<NetworkPlayer>();    
        
        foreach (var vivoxUser in NewVivoxManager.Instance.UsersInChannel)
        {
           // this won't work
            // need to check if vivoxUser vivoxUser.Value.DisplayName is a researcher in playfab
            // possibly need to add role to player network style in network manager and iterate over network players
           //   if (!_playFabLogin.IsResearcher)
            {
                var nameTag = Instantiate(AudioPlayerName, transform);
                nameTag.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, index * -50);
                nameTag.GetComponent<TMP_Text>().text = vivoxUser.Value.DisplayName;
                _playerNames[vivoxUser.Value.VivoxId] = nameTag;
                index++;
            }
        }
    }

    private void OnDisable()
    {
        foreach (var playerName in _playerNames)
        {
            Destroy(playerName.Value);
        }
        _playerNames.Clear();
    }

    public void Update()
    {
        foreach (var player in PhotonNetwork.PlayerList)
        {
            if (!player.CustomProperties.ContainsKey("VivoxId") ||
                !player.CustomProperties.ContainsKey("Muted")) continue;

            var vivoxId = (string)player.CustomProperties["VivoxId"];
            var isMuted = (bool)player.CustomProperties["Muted"];

            if (!_playerNames.ContainsKey(vivoxId)) continue;

            var playerText = _playerNames[vivoxId];
            playerText.GetComponentInChildren<Image>().color = isMuted ? Color.red : Color.black;
           
        }

        if (CurrentSpeaker == null) return;
        var userWasTalking = false;

        foreach (var vivoxUser in NewVivoxManager.Instance.UsersInChannel.Values.Where(vivoxUser => vivoxUser.IsTalking))
        {
            userWasTalking = true;
            CurrentSpeaker.GetComponentInChildren<TMP_Text>().text = vivoxUser.DisplayName;
        }

        CurrentSpeaker.SetActive(userWasTalking);
    }
}
