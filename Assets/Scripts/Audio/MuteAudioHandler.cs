﻿using ExitGames.Client.Photon;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class MuteAudioHandler : MonoBehaviour
{
    private NewVivoxManager _vivoxManager;
    private PlayFabLogin _playFabLogin;

    private bool _overrideMute;
    private bool _callback;
    private string _lastChannel;
    private bool _didSwitch = false;
    public bool ForcedMute;

    public Sprite MuteIcon;
    public Sprite UnmuteIcon;

    public Image MuteButton;

    void Start()
    {
        _vivoxManager = NewVivoxManager.Instance;
        _playFabLogin = FindObjectOfType<PlayFabLogin>();
        TeacherDB.Instance.DbUpdated += NewDbUpdate;
    }

    public void NewDbUpdate(object sender, DbUpdatedEventArgs args)
    {
        _overrideMute = args.db.mute;
        // _callback = args.db.callback;
    }

    void Update()
    {

        if (_callback && !_didSwitch && _vivoxManager.CurrentChannel != "TeacherCallback")
        {
            Debug.Log("Enter teacher callback");
            _lastChannel = _vivoxManager.CurrentChannel;
            _vivoxManager.ChangeChannel("TeacherCallback");
            _didSwitch = true;
        }

        if (_callback && _vivoxManager.CurrentChannel == "TeacherCallback")
        {
            _didSwitch = false;
        }

        if (!_callback && !_didSwitch && _vivoxManager.CurrentChannel == "TeacherCallback")
        {
            Debug.Log("Exit teacher callback");
            _vivoxManager.ChangeChannel(_lastChannel);
            _didSwitch = true;
        }

        if (!_callback && _vivoxManager.CurrentChannel != "TeacherCallback")
        {
            _didSwitch = false;
        }

        if (Input.GetKeyDown(KeyCode.M))
        {
            ToggleMute();
        }

        if (_playFabLogin.IsTeacher)
        {
            _vivoxManager.AudioInputDevices.Muted = _vivoxManager.Muted;
        }
        else
        {
            ForcedMute = _overrideMute;
            _vivoxManager.AudioInputDevices.Muted = _overrideMute ? _overrideMute : _vivoxManager.Muted;
        }

        MuteButton.sprite = _vivoxManager.Muted ? UnmuteIcon : MuteIcon;
    }

    public void ToggleMute()
    {
        _vivoxManager.Muted = !_vivoxManager.Muted;
        Hashtable mutedHash = new Hashtable() { { "Muted", _vivoxManager.Muted}, {"VivoxId", _vivoxManager.VivoxId } };
        PhotonNetwork.LocalPlayer.SetCustomProperties(mutedHash);
    }
}
