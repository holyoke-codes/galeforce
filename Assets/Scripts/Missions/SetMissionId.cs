﻿using UnityEngine;
using Photon.Pun;

public class SetMissionId : MonoBehaviour, IPunInstantiateMagicCallback
{

    public string MissionId;
    public string Team;
    public float Scale;

    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        object[] instantiationData = info.photonView.InstantiationData;
        MissionId = (string)instantiationData[0];
        Team = (string)instantiationData[1];
        Scale = (float)instantiationData[2];
        int teamMask = LayerMask.NameToLayer(Team);
        gameObject.layer = teamMask;
        var children = GetComponentsInChildren<Transform>(includeInactive: true);
        foreach (var child in children)
        {
            child.gameObject.layer = teamMask;
        }

        if (gameObject.name.Contains("Tree"))
        {
            transform.localScale = new Vector3(Scale, Scale, Scale);
        }
    }
}
