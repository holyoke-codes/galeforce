﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Photon.Pun;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using UnityEngine.SceneManagement;

public class PickupZone : MonoBehaviourPun, IPunInstantiateMagicCallback
{
    public enum ZoneTypes
    {
        Delivery,
        dog,
        DroneDelivery,
        Medical,
        person
    }

    public int _numberOfItems = 0;
    public int TotalNumberOfItems;
    public ZoneTypes ZoneType;
    public string MissionId;
    public string Team;
    private RobotInventory _robotInventory;
    private Queue<GameObject> _cargoObjects = new Queue<GameObject>();
    private bool _resetting = true;
    private PhotonView _pv;
    private Renderer _renderer;


    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        _pv = GetComponent<PhotonView>();
        _renderer = GetComponent<Renderer>();

        object[] instantiationData = info.photonView.InstantiationData;
        if (instantiationData != null && instantiationData.Length > 1)
        {
            MissionId = (string)instantiationData[0];
            Team = (string)instantiationData[1];
            TotalNumberOfItems = (int)instantiationData[2];
            // the ZoneTypes enum is 2 off the missiontypes enum
            ZoneType = (ZoneTypes)((int)instantiationData[3] - 2);
            gameObject.layer = LayerMask.NameToLayer(Team);
            _pv.RPC("RPCSetEnabled", RpcTarget.All, true);
            PhotonNetwork.SendAllOutgoingCommands();

        }
        else
            Debug.Log("Blank instantiation data? " + instantiationData);

        Debug.Log($"OnPhotonInstantiate set missionId {MissionId} for {Team} {gameObject.name} with {TotalNumberOfItems} {ZoneType}");
    }

    private void Update()
    {
        // Check if all items have been picked up and disable zone
        if (!_resetting && _numberOfItems <= 0)
        {

            if (_renderer.enabled)
            {
                Debug.Log("all cargo picked up");
                Debug.Log("Calling RPC set enabled false All for pickup zone from Update because all cargo picked up");
                _pv.RPC("RPCSetEnabled", RpcTarget.All, false);
                PhotonNetwork.SendAllOutgoingCommands();
            }
            if (_robotInventory != null)
            {
                _robotInventory.PickupZone = null;
            }
        }
    }

    [PunRPC]
    public void RPCSetEnabled(bool state)
    {
        GameObject activeMission = GameObject.FindGameObjectWithTag("ActiveMission");
        string activeMissionID = "-1";
        if (activeMission != null)
            activeMissionID = activeMission.GetComponent<MissionController>().missionID;
        //Debug.Log($"In RPC SetEnabled {Team} {MissionId} match to {PlayerInstance.Instance.ActiveTeam} {activeMissionID}?");
        if (Team == PlayerInstance.Instance.ActiveTeam && MissionId == activeMissionID)
           // && gameObject.GetComponent<PhotonView>().IsMine)
        {
            Debug.Log("RPC SetEnabled to: " + state);
            // Enabling renderer because if we use setActive, we can't find by tag.
            _renderer.enabled = state;
        }
        else
        {
            // doesn't match missionId and team, so make sure it's off
            _renderer.enabled = false;
        }
    }

    //[PunRPC]
    //public void RPCSetInactive()
    //{
    //    Debug.Log("RPC SetInactive zone");
    //    gameObject.SetActive(false);
    //    _renderer.enabled = false;
    //}

    public void Pickup()
    {
        //Debug.Log("Calling RPC Pickup All");
        _pv.RPC("RPCPickup", RpcTarget.All);
        PhotonNetwork.SendAllOutgoingCommands();
    }

    // RPC pickup(missionid, teamid) sent to all players
    // pickup if team and mission match     
    [PunRPC]
    public void RPCPickup()
    {
        GameObject activeMission = GameObject.FindGameObjectWithTag("ActiveMission");
        string activeMissionID = "-1";
        if (activeMission != null)
            activeMissionID = activeMission.GetComponent<MissionController>().missionID;
        // Debug.Log($"In RPC Pickup {Team} {MissionId} match to {PlayerInstance.Instance.ActiveTeam} {activeMissionID}?");
        if (Team == PlayerInstance.Instance.ActiveTeam && MissionId == activeMissionID)
        {
            //Debug.Log($"RPC Pickup, matched team and missionID with cargo {_cargoObjects.Count}");
            if (_cargoObjects.Count > 0)
            {
                GameObject cargo = _cargoObjects.Dequeue();
                if (cargo.GetComponent<PhotonView>().IsMine)
                    cargo.GetComponent<CargoToPickup>().DestroyCargo();
                _numberOfItems--;
            }
        }
    }


    // Called when Mission is closed or reset
    public void Clear()
    {
        Debug.Log("Destroying cargo " + _cargoObjects.Count);
        foreach (GameObject cargo in _cargoObjects)
        {
            if (cargo)
                cargo.GetComponent<CargoToPickup>().DestroyCargo();
        }

        _cargoObjects.Clear();
        _numberOfItems = 0;
    }

    [PunRPC]
    public void OwnerDestroy()
    {
        // send to all and only owner destroys
        if (gameObject && _pv.IsMine)
        {
            PhotonNetwork.Destroy(gameObject);
            PhotonNetwork.SendAllOutgoingCommands();
            Debug.Log("Owner destroyed pickup zone " + gameObject.name);
        }
    }


    public void DestroyZone()
    {
        Debug.Log("Calling RPC setInactive all for zone in destroyzone");
        //_pv.RPC("RPCSetInactive", RpcTarget.All);
        PhotonNetwork.SendAllOutgoingCommands();

        if (gameObject && !_pv)
            Debug.Log("Missing PhotonView " + gameObject.name);
        if (gameObject && _pv)
        {
            // Debug.Log("Calling RPC owner destroy for zone");
            _pv.RPC("OwnerDestroy", RpcTarget.All);
            PhotonNetwork.SendAllOutgoingCommands();
        }

    }

    [PunRPC]
    public void RPCReset()
    {
        _resetting = true;
    }

    public void Reset()
    {
        // let other players know you are reseting so they don't trigger their update()
        gameObject.GetComponent<PhotonView>().RPC("RPCReset", RpcTarget.All);

        Debug.Log("Resetting zones and instantiating cargo " + _cargoObjects.Count);
        _pv.RPC("RPCSetEnabled", RpcTarget.All, true);
        PhotonNetwork.SendAllOutgoingCommands();


        Clear();  // delete any existing cargo and clear cargoObjects

        bool alreadyLoaded = false;
        string tag = ZoneType.ToString();
        if (tag == "Medical")
            tag = "person";
        GameObject[] cargoObjs = GameObject.FindGameObjectsWithTag(tag);
        Debug.Log("Found in pickupzone reset " + cargoObjs.Length + " object(s) of tag " + tag);
        foreach (GameObject cargo in cargoObjs)
        {
            if (cargo.name.ToLower().StartsWith(tag.ToLower()) &&
                cargo.GetComponent<CargoToPickup>().Team == PlayerInstance.Instance.ActiveTeam &&
                cargo.GetComponent<CargoToPickup>().MissionId == MissionId)
            {
                cargo.SetActive(true);
                alreadyLoaded = true;
                _cargoObjects.Enqueue(cargo);
                _numberOfItems++;
                Debug.Log("Cargo exists in the game generated by another player");
            }
        }
        // If they don't exist at all 
        if (!alreadyLoaded)
        {
            Debug.Log("Cargo does not exist yet -- so instantiate");
            int offsetCount = 0;
            while (_numberOfItems < TotalNumberOfItems)
            {
                // ZoneTypes Delivery, dog, DroneDelivery, person
                var cargo = Resources.Load(tag);
                if (cargo)
                {
                    GameObject pickupObject;
                    // Instantiate objects to be picked up in the pickup zone.
                    // custom data - the third argument delivery = false since this is a pickup obj
                    object[] data = new object[] { MissionId, PlayerInstance.Instance.ActiveTeam, false };
                    Debug.Log("Custom data passed to pickup cargo " + cargo.name + " " +  data[0] + " " + data[1]);
                    if (ZoneType == ZoneTypes.Delivery)
                    {
                        pickupObject = PhotonNetwork.Instantiate(cargo.name, transform.position + transform.up, transform.rotation * Quaternion.Euler(90, 0, 0), 0, data);                          
                    }
                    else if (ZoneType == ZoneTypes.DroneDelivery)
                    {
                        // stack up multiple objects
                        Vector3 offset = new Vector3(1.5f, 1 * offsetCount * 0.56f, 0);
                        offsetCount++;
                        Vector3 pos = transform.position + offset;
                        // Let's get rid of - transform.up 
                        pickupObject = PhotonNetwork.Instantiate(cargo.name, pos, transform.rotation, 0, data);
                    }
                    else // dronedelivery, dog, medical, person, etc.
                    {
                        pickupObject = PhotonNetwork.Instantiate(cargo.name, transform.position, transform.rotation, 0, data);
                    }
                    pickupObject.GetComponent<CargoToPickup>().MissionId = MissionId;
                    pickupObject.GetComponent<CargoToPickup>().Team = PlayerInstance.Instance.ActiveTeam;
                    _cargoObjects.Enqueue(pickupObject);
                }
                _numberOfItems++;
            }
        }
        
        _resetting = false;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "ActiveRobot")
        {
            _robotInventory = other.gameObject.GetComponent<RobotInventory>();
            _robotInventory.PickupZone = this;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "ActiveRobot" && other.gameObject.GetComponent<NetworkRobot>().MissionId == MissionId)
        {

            if (SceneManager.GetActiveScene().name == "TrainingComplex")
            {
                if (other.gameObject.GetComponent<NetworkRobot>().Team == PlayerInstance.Instance.ActiveTeam)
                {
                    _robotInventory = other.gameObject.GetComponent<RobotInventory>();
                    _robotInventory.PickupZone = this;
                }
            }
            else
            {
                _robotInventory = other.gameObject.GetComponent<RobotInventory>();
                _robotInventory.PickupZone = this;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "ActiveRobot")
        {
            _robotInventory = other.gameObject.GetComponent<RobotInventory>();
            _robotInventory.PickupZone = null;
        }
    }

}
