﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeliveryZone : MonoBehaviour
{

    public int ItemsInZone = 0;
    public int ItemGoal = 4;
    public string MissionId;

    private RobotInventory _robotInventory;
    private MissionController _missionController;
    private MissionAssets _missionAssets;

    private int _baseGoal = 0;

    public enum ItemTypes
    {
        Medicine
    }

    public ItemTypes ItemToLookFor = ItemTypes.Medicine;

    private void Start()
    {
        _baseGoal = ItemGoal;
        _missionAssets = GameObject.FindGameObjectWithTag("MissionAssets").GetComponent<MissionAssets>();
    }

    public void Reset()
    {
        gameObject.SetActive(true);
        ItemsInZone = 0;
        ItemGoal = _baseGoal;
    }

    // Update is called once per frame
    void Update()
    {
        if (_missionController != null)
        {
            if (ItemGoal == ItemsInZone)
            {
                Debug.Log("All items arrived!");
                //_missionController.FinishMission(PlayerInstance.Instance.ActiveTeam);
                if (!_missionController.IsFinished)
                {
                    //_missionController.FinishMission(PlayerInstance.Instance.ActiveTeam);
                    TeacherDB.Instance.SaveMissionCompleted(PlayerInstance.Instance.ActiveTeam, MissionId);
                    _missionController.IsFinished = true;
                }

            }
        }
            
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "ActiveRobot" && other.gameObject.GetComponent<NetworkRobot>().MissionId == MissionId)
        {
            if (SceneManager.GetActiveScene().name == "TrainingComplex")
            {
                // Check if teams match in the Training Complex
                if (other.gameObject.GetComponent<NetworkRobot>().Team == PlayerInstance.Instance.ActiveTeam)
                {
                    _robotInventory = other.gameObject.GetComponent<RobotInventory>();
                    _robotInventory.DeliveryZone = this;
                }
            }
            else
            {
                // Don't compare teams in the ControlRoom
                _robotInventory = other.gameObject.GetComponent<RobotInventory>();
                _robotInventory.DeliveryZone = this;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "ActiveRobot")
        {
            _robotInventory = other.gameObject.GetComponent<RobotInventory>();
            _robotInventory.DeliveryZone = null;
        }
    }

    public void SetMissionController(MissionController missionController)
    {
        _missionController = missionController;
    }
}
