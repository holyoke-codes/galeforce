﻿using UnityEngine;
using Photon.Pun;

public class CargoToPickup : MonoBehaviourPun, IPunInstantiateMagicCallback
{
    public string MissionId;
    public string Team;
    public bool isActive = true;
    public bool isDestroyed = false;
    public bool delivery = false;
    private PhotonView _pv;


    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        _pv = GetComponent<PhotonView>();

        object[] instantiationData = info.photonView.InstantiationData;
        if (instantiationData != null && instantiationData.Length > 1)
        {
            MissionId = (string)instantiationData[0];
            Team = (string)instantiationData[1];
            delivery = (bool)instantiationData[2];
            int teamMask = LayerMask.NameToLayer(Team);
            gameObject.layer = teamMask;
            var children = GetComponentsInChildren<Transform>(includeInactive: true);
            foreach (var child in children)
            {
                child.gameObject.layer = teamMask;
            }
        }
        else
            Debug.Log("Blank instantiation data? " + instantiationData);

        Debug.Log($"OnPhotonInstantiate set missionId {MissionId} for {Team} {gameObject.name}");
    }

    [PunRPC]
    public void RPCSetActive(bool state)
    {
        GameObject activeMission = GameObject.FindGameObjectWithTag("ActiveMission");
        string activeMissionID = "-1";
        if (activeMission != null)
            activeMissionID = activeMission.GetComponent<MissionController>().missionID;
       // Debug.Log($"In RPC Set Active Cargo {Team} {MissionId} match to {PlayerInstance.Instance.ActiveTeam} {activeMissionID}?");
        if (Team == PlayerInstance.Instance.ActiveTeam && MissionId == activeMissionID)
        { 
            Debug.Log("RPC SetActive cargo, matched team and missionID to setactive " + state);
            gameObject.SetActive(state);
        } 
        else
            gameObject.SetActive(false);
    }

    public void DestroyCargo()
    {
        //Debug.Log("Calling RPC setActive false All for cargo");
        _pv.RPC("RPCSetActive", RpcTarget.All, false);

        isActive = false;
        isDestroyed = true;
      
        if (gameObject && !_pv)
            Debug.Log("Missing PhotonView " + gameObject.name);
        if (gameObject && _pv)
        {
            if (_pv.IsMine)
            {
                PhotonNetwork.Destroy(gameObject);
                Debug.Log("Destroyed cargo " + gameObject.name);
            }
            else
            {
                Debug.Log("Calling RPC owner destroy");
                _pv.RPC("OwnerDestroy", RpcTarget.All);
                PhotonNetwork.SendAllOutgoingCommands();
                // This was giving errors
                //                Debug.Log("transfer ownership");
                //              cargo.GetComponent<PhotonView>().TransferOwnership(PhotonNetwork.LocalPlayer);
            }

        }
        // if it still exists, destroy it locally 
       /* if (gameObject)
        {
            Debug.Log("Local destroy cargo if all else fails " + gameObject.name);
            Destroy(gameObject);
        }*/
    }              

    [PunRPC]
    public void OwnerDestroy()
    {
        // only the owner will be able to destroy it
        if (gameObject && _pv.IsMine)
        {
            PhotonNetwork.Destroy(gameObject);
            PhotonNetwork.SendAllOutgoingCommands();
            Debug.Log("Owner destroyed cargo " + gameObject.name);
        }
       // else
        //    Debug.Log("Couldn't destroy cargo");
    }

    // let's try stacking them instead
    /*void OnCollisionEnter(Collision collision)
    {
        // don't collide with multiple pick up objects
        if (collision.gameObject.GetComponent<CargoToPickup>())
        {
            Physics.IgnoreCollision(collision.gameObject.GetComponent<Collider>(), GetComponent<Collider>());
        }
    }*/


}


