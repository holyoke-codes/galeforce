﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MissionList : MonoBehaviour
{
    // MOVE TO NETWORK
    public Dictionary<string, List<MissionController>> _availableMissions;
    private Dictionary<string, List<MissionController>> _completedMissions;
    public int TotalScore;

    public string DefaultStage;
    private string _currentStage;
    private string _databaseStage;
    public MissionsStage[] MissionStages;
    private PlayFabLogin _playFabLogin;
    private string _activeTeam;

    private bool _missionsLoaded;
    private GameObject[] _rings;
    private GameObject _rain;
    private GameObject _waterBlocker;
    private GameObject[] _townpeople;
    public List<GameObject> DebrisList = new List<GameObject>();
    public Dictionary<string, Dictionary<string, GameObject>> RobotList = new Dictionary<string, Dictionary<string, GameObject>>();

    private GameObject _whiteboardScavengerHunt;
    private GameObject _whiteboardPreparation;
    private GameObject _whiteboardResponse1;
    private GameObject _whiteboardResponse2;
    private GameObject _whiteboardResponse3;
    private GameObject _whiteboardEnding;

    private void Start()
    {
        _rings = GameObject.FindGameObjectsWithTag("ring");
        _rain = GameObject.Find("Rain");
        _waterBlocker = GameObject.Find("Dont drive through the water");
        _townpeople = GameObject.FindGameObjectsWithTag("townpeople");
        _whiteboardScavengerHunt = GameObject.Find("whiteboard-Drone Scavenger Hunt");
        _whiteboardPreparation = GameObject.Find("whiteboard-Preparation");
        _whiteboardResponse1 = GameObject.Find("whiteboard-Response1");
        _whiteboardResponse2 = GameObject.Find("whiteboard-Response2");
        _whiteboardResponse3 = GameObject.Find("whiteboard-Response3");
        _whiteboardEnding = GameObject.Find("whiteboard-Ending");

        // Get the mission robots, add them to the list of robots
        // and deactivate them until a mission is started.
        GameObject[] robots = GameObject.FindGameObjectsWithTag("Robot");
        NetworkManager networkManager = FindObjectOfType<NetworkManager>();
        foreach (GameObject robot in robots)
        {
            NetworkRobot nr = robot.GetComponent<NetworkRobot>();
            // Load the robots into a dictionary of dictionaries
            // GO robot = RobotList[team][missionid]
            if (SceneManager.GetActiveScene().name == "ControlRoom")
            {
                // Players in the Control Room are in separate Photon rooms
                // so we can use the same robot for multiple teams.
                foreach (string team in networkManager.TeamProps.Keys)
                {
                    if (!RobotList.ContainsKey(team))
                        RobotList.Add(team, new Dictionary<string, GameObject>());
                    if (!RobotList[team].ContainsKey(nr.MissionId))
                        RobotList[team].Add(nr.MissionId, robot);
                }
            }
            else
            {
                // Training complex robots have a different robot for each team
                // since they are all in the same Photon room.
                if (!RobotList.ContainsKey(nr.Team))
                    RobotList.Add(nr.Team, new Dictionary<string, GameObject>());
                if (!RobotList[nr.Team].ContainsKey(nr.MissionId))
                    RobotList[nr.Team].Add(nr.MissionId, robot);
            }
            robot.SetActive(false);
        }
        _playFabLogin = GetComponent<PlayFabLogin>();
        _availableMissions = new Dictionary<string, List<MissionController>>();
        _completedMissions = new Dictionary<string, List<MissionController>>();
    }

    public void LoadMissionList()
    {
        TeacherDB.Instance.DbUpdated += NewDbUpdate;

        foreach (PlayFabLogin.PlayfabGroup group in _playFabLogin.PlayfabGroups)
        {
            _availableMissions[group.Name] = new List<MissionController>();
            _completedMissions[group.Name] = new List<MissionController>();
        }

        _missionsLoaded = true;
    }

    public void LoadStage(string stageName)
    {
        if (SceneManager.GetActiveScene().name == "ControlRoom")
        {
            // Show appropriate whiteboard content
            _whiteboardScavengerHunt.SetActive(false);
            _whiteboardPreparation.SetActive(false);
            _whiteboardResponse1.SetActive(false);
            _whiteboardResponse2.SetActive(false);
            _whiteboardResponse3.SetActive(false);
            _whiteboardEnding.SetActive(false);
            switch (stageName)
            {
                case "Preparation":
                    _whiteboardPreparation.SetActive(true);
                    break;
                case "Response1":
                    _whiteboardResponse1.SetActive(true);
                    break;
                case "Response2":
                    _whiteboardResponse2.SetActive(true);
                    break;
                case "Response3":
                    _whiteboardResponse3.SetActive(true);
                    break;
                case "ScavengerHunt":
                    _whiteboardScavengerHunt.SetActive(true);
                    break;
                case "Ending":
                    _whiteboardEnding.SetActive(true);
                    break;
                default:
                    break;
            }
        }

        if (MissionStages.Where(s => s.Name == stageName).ToArray().Length == 0)
        {
            Debug.LogError("No stage found with the name " + stageName );
            _currentStage = stageName;
            return;
        }

        // Show rings for drone flying stage
        foreach (GameObject ring in _rings)
        {

            if (stageName == "Remote Drones")
            {
                ring.SetActive(true);
            }
            else
            {
                ring.SetActive(false);
            }
        }

        // Show townpeople at the end  stage
        foreach (GameObject p in _townpeople)
        {

            if (stageName == "Ending")
            {
                p.SetActive(true);
            }
            else
            {
                p.SetActive(false);
            }
        }

        // Switch to destroyed buildings if hurricane response stages
        if (stageName.Contains("Response"))
        {
            Debug.Log("Switching to destroyed buildings and trees in Response stages!");
            // Get game objects with script ToggleDestruction
            ToggleDestruction[] buildingsAndTrees = (ToggleDestruction[])FindObjectsOfType<ToggleDestruction>();
            
            foreach (ToggleDestruction b in buildingsAndTrees)
            {
                b.Destroyed = true;
            }
            _rain.SetActive(true);
            if (stageName == "Response1" || stageName == "Response2")
                _waterBlocker.SetActive(true);
            else
                _waterBlocker.SetActive(false);
        }

        // Toggle off destruction if switching back to Preparation during testing
        // or in ending stage
        else
        {
            Debug.Log("Switching to undestroyed buildings and trees in Prep stage");
            // Get game objects with script ToggleDestruction
            ToggleDestruction[] buildingsAndTrees = (ToggleDestruction[])FindObjectsOfType<ToggleDestruction>();

            foreach (ToggleDestruction b in buildingsAndTrees)
            {
                b.Destroyed = false;
            }
            if (_rain)
                _rain.SetActive(false);
            if (_waterBlocker)
                _waterBlocker.SetActive(false);
        }

        _availableMissions[_activeTeam] = new List<MissionController>();
        foreach (MissionController mc in MissionStages.Where(s => s.Name == stageName).ToArray()[0].Missions)
        {
            MissionController mCon = Instantiate(mc);
            if (stageName == _databaseStage && !_completedMissions[_activeTeam].Contains(mc))
                _availableMissions[_activeTeam].Add(mCon);
        }

        _currentStage = stageName;
    }

    public void LoadMissions()
    {
        if (PlayerInstance.Instance.ActiveTeam == "")
            PlayerInstance.Instance.ActiveTeam = "team4"; // for testing
        foreach (MissionController m in _availableMissions[PlayerInstance.Instance.ActiveTeam])
        {
            m.ShowMissionIcon();
        }
    }

    public void HideMissions()
    {
        foreach (MissionController m in _availableMissions[PlayerInstance.Instance.ActiveTeam]) m.HideMissionIcon();
    }

    private void Update()
    {
        if (!_missionsLoaded)
        {
            LoadMissionList();
        }

        // Check if stage has advanced
        if (_databaseStage != _currentStage)
        {
            Debug.Log("Stage update to " + _databaseStage);
            LoadStage(_databaseStage);
        }

        // Display only available missions
        _activeTeam = PlayerInstance.Instance.ActiveTeam;
        if (!String.IsNullOrEmpty(_activeTeam) && _availableMissions != null && _availableMissions.ContainsKey(_activeTeam))
        {
            // First hide all the mission icons
            foreach (PlayFabLogin.PlayfabGroup team in _playFabLogin.PlayfabGroups)
            {
                foreach (MissionController mc in _availableMissions[team.Name])
                {
                    mc.HideMissionIcon();
                }
            }
            // Show mission icons for the new stage for the active team
            foreach (MissionController mc in _availableMissions[_activeTeam])
            {
                foreach (MissionsStage stage in MissionStages)
                {
                    if (stage.Name == _databaseStage && _availableMissions[_activeTeam].Contains(mc) && !_completedMissions[_activeTeam].Contains(mc))
                    {                        
                        mc.DisplayMissionIcon();
                    }
                }
            }
            // Hide any completed icons
            foreach (MissionController mc in _completedMissions[_activeTeam])
            {
                mc.HideMissionIcon();
            }
        }
    }

    public void NewDbUpdate(object sender, DbUpdatedEventArgs args)
    {
        // TODO: Get stages for correct team

        var matchingTeamList = args.db.teams.Where(t => t.team_name == PlayerInstance.Instance.ActiveTeam).ToArray();

        if (matchingTeamList.Length > 0)
        {
            var currentTeamInfo = matchingTeamList[0];
            _databaseStage = SceneManager.GetActiveScene().name == "TrainingComplex" ? currentTeamInfo.training_stage : currentTeamInfo.control_stage;
        }

        foreach (var team in args.db.teams)
        {
            foreach (var missionId in team.completed_missions)
            {
                MissionController mc = _availableMissions[team.team_name].Find(m => m.missionID == missionId);
                if (mc)
                {
                    Debug.Log("Being told to finish mission " + mc.missionID);
                    mc.FinishMission(team.team_name);
                }
            }   
        }
        // Generate debris
        if (!String.IsNullOrEmpty(_activeTeam))
        {
            foreach (MissionController mc in _availableMissions[_activeTeam])
            {
                if (mc.MissionType == MissionTypes.Debris && !mc.DebrisCreated)
                {
                    mc.GenerateDebris();
                    mc.DebrisCreated = true;
                }
            }
        }
    }

    public List<MissionController> GetAvailableMissions()
    {
        return _availableMissions[PlayerInstance.Instance.ActiveTeam];
    }

    public void SetCompletedMissions(string team, string[] missions)
    {
        MissionsStage missionStage = MissionStages.DefaultIfEmpty(null).FirstOrDefault(s => s.Name == _currentStage);
        if (missionStage != null)
        {
            foreach (string id in missions)
            {
                {
                    MissionController mc = missionStage.Missions.Cast<MissionController>().DefaultIfEmpty(null).FirstOrDefault(m => m.missionID == id);
                    if (mc && !_completedMissions[team].Contains(mc))
                    {
                        mc.FinishMission(team);
                        _completedMissions[team].Add(mc);
                        _availableMissions[team].Remove(mc);
                    }
                }

            }
        }
    }

    public List<MissionController> GetCompletedMissions()
    {
        if (_completedMissions.ContainsKey(PlayerInstance.Instance.ActiveTeam))
            return _completedMissions[PlayerInstance.Instance.ActiveTeam];
        return new List<MissionController>();
    }

    public void CompleteMission(string team, MissionController mission)
    {
        _availableMissions[team].Remove(mission);
        _completedMissions[team].Add(mission);
    }
}

[Serializable]
public class MissionsStage
{
    public string Name;
    public MissionController[] Missions;
}
