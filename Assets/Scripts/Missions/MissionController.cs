﻿using System;
using System.Collections.Generic;
using System.Linq;
using MyBox;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;
using Photon.Pun;
using Random = UnityEngine.Random;
using SensorToolkit;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using UnityEngine.SceneManagement;


public enum MissionTypes
{
    Cellular,
    Debris,
    Delivery,
    dog,
    DroneDelivery,
    Medical,
    person,
    Map
}

public static class MissionTypesExtensions
{
    public static string ToFriendlyString(this MissionTypes me)
    {
        switch (me)
        {
            case MissionTypes.DroneDelivery:
                return "Drone Delivery";
            default:
                return me.ToString();
        }
    }
}

public enum ControlTypes
{
    Autonomous,
    RemoteControlled
}

public static class ControlTypesExtensions
{
    public static string ToFriendlyString(this ControlTypes me)
    {
        switch (me)
        {
            case ControlTypes.RemoteControlled:
                return "Remote Controlled";
            default:
                return me.ToString();
        }
    }
}


public class MissionController : MonoBehaviour
{

    public enum Robots
    {
        DeliBot,
        EMMA,
        RoboVan,
        Aseo,
        DeliDrone,
        Athena
    }

    [Header("Mission Setup:")]
    public string missionID;
    public string missionName = "Mission Name";
    public string missionDescription = "Description";
    public string missionSuccessTitle = "Thank You";
    public string missionSuccessMsg = "Success Message";
    public string missionFrom = "From";
    public Sprite speakerBubble;
    public MissionTypes MissionType = MissionTypes.Debris;
    public Robots RobotToUse = Robots.DeliBot;
    public ControlTypes ControlStyle = ControlTypes.Autonomous;
    public int points = 0;

    public ComputerController _computerController;

    public Dictionary<string,List<Debris>> _debrisControllers = new Dictionary<string, List<Debris>>();
    private readonly List<MobileCellTowerScript> _cellTowerController = new List<MobileCellTowerScript>();

    [Header("Mission Options:")]
    public Transform startPoint;
    public Transform endPoint;

    [ConditionalField(nameof(MissionType), true, MissionTypes.Debris, MissionTypes.Map, MissionTypes.Cellular)]
    public int ItemsToDeliver;

    [ConditionalField(nameof(MissionType), true, MissionTypes.Debris, MissionTypes.Map, MissionTypes.Cellular)]
    public PickupZones pickupZones;

    [ConditionalField(nameof(MissionType), false, MissionTypes.Debris)]
    public DebrisLocations debrisLocations;

    [ConditionalField(nameof(MissionType), false, MissionTypes.Debris)]
    public DebrisTypes debrisTypes;

    [ConditionalField(nameof(MissionType), false, MissionTypes.Cellular)]
    public MobileCellTowerLocations mobileCellTowerLocations;

    [ConditionalField(nameof(MissionType), false, MissionTypes.Map)]
    public BuildingLocations buildingLocations; // List of transforms

    [ConditionalField(nameof(finishAtDump))]
    public Transform dumpLocation;

    [ConditionalField(nameof(MissionType), false, MissionTypes.Debris)]
    public bool finishAtDump;

    [Header("Mission Sequencing:")]
    public Sprite missionSprite;
    public Sprite missionStartIcon;
    public Sprite missionMiddleIcon;
    public Sprite missionEndIcon;

    [Header("Mission Icon Position:")]
    public int positionX;
    public int positionY;

    private Transform _startIcon;
    private MissionAssets _missionAssets;
    private JSTranslator _jsTranslator;
    private MissionDetailPane _missionDetailPaneScript;
    private Button _missionIcon;
    private bool _missionStarted;
    private GameObject _activeRobot;
    private List<GameObject> zones = new List<GameObject>();
    private GameObject _myNetworkPlayer;
    private List<FlyOverZoneScript> _buildingController = new List<FlyOverZoneScript>();
    private NetworkManager _networkManager;

    private bool _shown = false;
    public bool DebrisCreated = false;
    private Dictionary<string, bool> _loaded = new Dictionary<string, bool>();
    public bool IsFinished = false;


    public void Start()
    {
        _missionAssets = GameObject.FindGameObjectWithTag("MissionAssets").GetComponent<MissionAssets>();
        _missionIcon = gameObject.transform.GetChild(0).GetChild(0).GetComponent<Button>();

        var missionIdText = _missionIcon.GetComponentInChildren<TextMeshProUGUI>();
        missionIdText.text = missionID;
        var _image = _missionIcon.transform.Find("MissionCategoryImage").GetComponent<Image>();
        _image.sprite = missionSprite;
        _computerController = GameObject.Find("GAMEMASTER").GetComponent<ComputerController>();
        _missionDetailPaneScript = _missionAssets.MissionDetailPane.GetComponent<MissionDetailPane>();

        _missionAssets.MissionCompleteCanvas.SetActive(false);
        _myNetworkPlayer = GameObject.FindWithTag("myNetworkPlayer");
        _networkManager = GameObject.FindObjectOfType<NetworkManager>();
    }

    public void LoadMission()
    {
        Debug.Log("loading mission");
        
        // Put the missionID into the ? current mission button
        TextMeshProUGUI _missionNumText = _missionAssets.MissionNumText.GetComponent<TextMeshProUGUI>();
        _missionNumText.text = missionID;

        _missionAssets.MissionDetailPane.SetActive(true);
        // hack: add team to mission name
        _missionDetailPaneScript.UpdateMissionCard(missionID,
             missionName + " " + PlayerInstance.Instance.ActiveTeam, missionFrom, missionDescription, speakerBubble);
        _missionDetailPaneScript.UpdateMissionDetails(ControlStyle.ToFriendlyString(), RobotToUse.ToString(), MissionType.ToFriendlyString());
        _missionDetailPaneScript.missionSelectButton.onClick.RemoveAllListeners();
        _missionDetailPaneScript.missionSelectButton.onClick.AddListener(() => { StartMission(); });
        // Add icons showing start, middle, and end points of mission
        // Clear any previous mission icons that have been placed.
        foreach (var t in _missionDetailPaneScript.missionIconController)
            Destroy(t.gameObject);

        _missionDetailPaneScript.missionIconController.Clear();

        // Add the start icon
        _startIcon = Instantiate(_missionDetailPaneScript.startIcon, startPoint.position + new Vector3(0, 50, 0), Quaternion.identity);
        _startIcon.GetChild(1).GetComponent<SpriteRenderer>().sprite = missionStartIcon;
        _startIcon.gameObject.SetActive(true);
        _missionDetailPaneScript.missionIconController.Add(_startIcon);

    
        // Add the middle icon if it exists for delivery type of missions
        if (MissionType != MissionTypes.Debris && MissionType != MissionTypes.Map  
            && missionMiddleIcon != null)
        {
            var midIcon = Instantiate(_missionDetailPaneScript.middleIcon, pickupZones.Value[0].Location.position + new Vector3(0, 50, 0),
                Quaternion.identity);
            midIcon.GetChild(1).GetComponent<SpriteRenderer>().sprite = missionMiddleIcon;
            _missionDetailPaneScript.missionIconController.Add(midIcon);
        }

        if (MissionType == MissionTypes.Debris)
        {
            var locs = debrisLocations.Value;
            foreach (var t in locs)
            {
                if (t != null)
                {
                    var middleIcon = Instantiate(_missionDetailPaneScript.middleIcon, t.position + new Vector3(0, 50, 0), Quaternion.identity);
                    middleIcon.GetChild(1).GetComponent<SpriteRenderer>().sprite = missionMiddleIcon;
                    _missionDetailPaneScript.missionIconController.Add(middleIcon);
                }
            }
        }

        if (MissionType != MissionTypes.Map && MissionType != MissionTypes.Debris)
        {
            // Add the end icon
            Transform endIcon = Instantiate(_missionDetailPaneScript.endIcon, endPoint.position + new Vector3(0, 50, 0),
                Quaternion.identity);
            endIcon.GetChild(1).GetComponent<SpriteRenderer>().sprite = missionEndIcon;
            _missionDetailPaneScript.missionIconController.Add(endIcon);
        }

        // Hide the mission selection icons
        _computerController.MissionListRef.HideMissions();
        _missionAssets.ChooseAMissionImage.SetActive(false);
    }

    public void StartMission()
    {

        // Connect to Mission Channel

        var currentChannel = NewVivoxManager.Instance.CurrentChannel;
        var missionChannelToJoin = "";

        if (currentChannel.Split('_').Length > 1)
        {
            missionChannelToJoin = currentChannel.Split('_')[0] + "_" + currentChannel.Split('_')[1] + "_" + missionID;
        }
        else
        {
            missionChannelToJoin = currentChannel + "_" + missionID;
        }

        NewVivoxManager.Instance.ChangeChannel(missionChannelToJoin);

        // Set this MissionController to the ActiveMission
        gameObject.tag = "ActiveMission";

        _activeRobot = _computerController.MissionListRef.RobotList[PlayerInstance.Instance.ActiveTeam][missionID];

        // Set the robot to the start position for this mission if it's not currently active
        if (!_activeRobot.activeSelf)
        {
            // Take ownership of the robot if needed to fix falling robot problem if not MasterClient
            // Do we need to take ownership of other mision assets too?
            if (!_activeRobot.GetComponent<PhotonView>().IsMine)
            {
                _activeRobot.GetComponent<PhotonView>().TransferOwnership(PhotonNetwork.LocalPlayer);
            }
            _activeRobot.transform.position = startPoint.position;
            _activeRobot.transform.rotation = startPoint.rotation;
            Debug.Log("robot set to startPoint " + startPoint.position);

            // Check to see if the mission is already active in the Photon Room
            if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsKey(PlayerInstance.Instance.ActiveTeam))
            {
                _networkManager.TeamProps[PlayerInstance.Instance.ActiveTeam] = PhotonNetwork.CurrentRoom.CustomProperties[PlayerInstance.Instance.ActiveTeam];
            }
            Hashtable teamHash;
            if (_networkManager.TeamProps.ContainsKey(PlayerInstance.Instance.ActiveTeam))
            {
                teamHash = (Hashtable)_networkManager.TeamProps[PlayerInstance.Instance.ActiveTeam];
            }
            else
            {
                teamHash = new Hashtable();
            }
            bool active = false;
            if (teamHash.ContainsKey(missionID))
                active = (bool)teamHash[missionID];

            if (!active)
            {
                // It's not active yet, set the active state through Photon.
                // Robot is set to active state in NetworkManager when this
                // event is received.

                if (teamHash.ContainsKey(missionID))
                    teamHash[missionID] = true;
                else
                    teamHash.Add(missionID, true);

                Hashtable setValue = new Hashtable();
                setValue.Add(PlayerInstance.Instance.ActiveTeam, teamHash);
                PhotonNetwork.CurrentRoom.SetCustomProperties(setValue);
            }
        }

        switch (ControlStyle)
        {
            case ControlTypes.RemoteControlled:
                // Open remote control UI
                _missionAssets.TeleOpCanvas.SetActive(true);
                _missionAssets.MonitorCanvas.SetActive(false);

                // Change to for all remote drones, disable
                if (_activeRobot.GetComponent<PhotonView>().IsMine)
                {
                    // If it's a drone, disable for now until they hit the steering wheel
                    if (_activeRobot.GetComponent<DroneRCController>())
                    {
                        _activeRobot.GetComponent<DroneRCController>().enabled = false;
                        _activeRobot.GetComponent<HoverScript>().enabled = false;
                    }
                }
                break;
            case ControlTypes.Autonomous:
                // Open the programming UI
                _computerController.OpenProgramming();
                _missionAssets.ProgrammingCanvas.SetActive(true);
                _missionAssets.MonitorCanvas.SetActive(false);
                // If it's an autonous mission, store the mission id for database
                _activeRobot.GetComponent<BlockRobotController>().MissionId = missionID;
                _activeRobot.GetComponent<BlockRobotController>().NetworkCargoHandler = _activeRobot.GetComponent<NetworkCargoHandler>();

                // Set the grid origin point based on the scene
                if (MissionType.Equals(MissionTypes.DroneDelivery)
                    || MissionType.Equals(MissionTypes.Map))
                {
                    if (SceneManager.GetActiveScene().name == "ControlRoom")
                    {
                        _activeRobot.GetComponent<BlockRobotController>().Origin = GameObject.FindGameObjectWithTag("ControlRoomOrigin");
                    }
                    else
                    {
                        _activeRobot.GetComponent<BlockRobotController>().Origin = GameObject.FindGameObjectWithTag("TrainingOrigin");
                    }
                }

                _jsTranslator = _missionAssets.ProgrammingView.transform.Find("CodingWindow").GetComponent<JSTranslator>();

                // set the team name from PlayFab (BH)
                _jsTranslator.TeamName = PlayerInstance.Instance.ActiveTeam;
                 // set in missionlist
                 // if (_jsTranslator.TeamName == "")
                 //   _jsTranslator.TeamName = "TeamZero";  // for testing
                _jsTranslator.HandleFirebaseUpdate("team", _jsTranslator.TeamName);
                Debug.Log("Team name set in jsTranslator to " + _jsTranslator.TeamName);

                // New robot, tell the coding environment
                _jsTranslator.ActiveRobot = _activeRobot.GetComponent<BlockRobotController>();
                _jsTranslator.HandleFirebaseUpdate("robot", _activeRobot.name);

                // New  mission, tell the coding environment
                _jsTranslator.HandleFirebaseUpdate("mission", missionID);

                // Also send mission description (BH)
                _jsTranslator.HandleFirebaseUpdate("missiondescription", missionDescription);

                // If it's a drone, disable remote controls and enable hovering
                if (_activeRobot.GetComponent<DroneRCController>())
                {
                    _activeRobot.GetComponent<DroneRCController>().enabled = false;
                    _activeRobot.GetComponent<HoverScript>().enabled = true;
                }
                _activeRobot.GetComponent<BlockRobotController>().StartLocation = startPoint;
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        // Cameras to follow the new active robot
        CinemachineVirtualCamera frontCam = _missionAssets.FrontCamera.GetComponent<CinemachineVirtualCamera>();
        frontCam.Follow = _activeRobot.transform;

        CinemachineVirtualCamera overheadCam = _missionAssets.OverheadCamera.GetComponent<CinemachineVirtualCamera>();
        overheadCam.Follow = _activeRobot.transform;
        overheadCam.LookAt = _activeRobot.transform;

        CinemachineVirtualCamera rearOverheadCam = _missionAssets.RearOverheadCamera.GetComponent<CinemachineVirtualCamera>();
        rearOverheadCam.Follow = _activeRobot.transform;
        rearOverheadCam.LookAt = _activeRobot.transform;

        CinemachineVirtualCamera leftFrontCam = _missionAssets.LeftFrontCamera.GetComponent<CinemachineVirtualCamera>();
        leftFrontCam.Follow = _activeRobot.transform;
        CinemachineVirtualCamera leftOverheadCam = _missionAssets.LeftOverheadCamera.GetComponent<CinemachineVirtualCamera>();
        leftOverheadCam.Follow = _activeRobot.transform;
        leftOverheadCam.LookAt = _activeRobot.transform;
        CinemachineVirtualCamera leftRearOverheadCam = _missionAssets.LeftRearOverheadCamera.GetComponent<CinemachineVirtualCamera>();
        leftRearOverheadCam.Follow = _activeRobot.transform;
        leftRearOverheadCam.LookAt = _activeRobot.transform;

        // Mapping mission
        if (MissionType == MissionTypes.Map)
        {
            _activeRobot.transform.Find("Sensor").GetComponent<RaySensor>().enabled = true;
            // Instantiate the locations
            Transform[] locs = buildingLocations.Value;
            foreach (Transform t in locs)
            {
                GameObject zoneObj = Instantiate(Resources.Load("FlyOver Zone") as GameObject, t.position, Quaternion.identity) as GameObject;
                zones.Add(zoneObj);
               _buildingController.Add(zoneObj.GetComponent<FlyOverZoneScript>());
            }

        }

        // Mobile Cell Tower mission
        if (MissionType == MissionTypes.Cellular)
        {
            // Instantiate the locations
            Transform[] locs = mobileCellTowerLocations.Value;
            foreach (Transform t in locs)
            {
                GameObject zoneObj = Instantiate(Resources.Load("Hover Zone") as GameObject, t.position, Quaternion.identity) as GameObject;
                zones.Add(zoneObj);
                _cellTowerController.Add(zoneObj.GetComponent<MobileCellTowerScript>());
            }
        }

        // If its a delivery location setup zones
        if (MissionType != MissionTypes.Debris && MissionType != MissionTypes.Map && MissionType != MissionTypes.Cellular)
        {
            // DeliBot and DeliDrone carry cargo objects. Start with those hidden.
            if (MissionType == MissionTypes.Delivery || MissionType == MissionTypes.DroneDelivery)
            {
                _activeRobot.transform.Find("Cargo").gameObject.SetActive(false);
            }

            GameObject zoneObj = null;

            if (zones.Count() == 0)
            {
                Debug.Log("No existing zones, so instantiate");
                // We haven't started this mission before,
                // so create all the mission objects  
                // PickupZone is networked, others are local.

                // Delivery Zone

                zoneObj = Instantiate(Resources.Load("Delivery Zone") as GameObject, endPoint.transform.position, endPoint.transform.rotation);
                zoneObj.GetComponent<DeliveryZone>().MissionId = missionID;

                if (zoneObj)
                {
                    zones.Add(zoneObj);
                    DeliveryZone deliveryZone = zoneObj.GetComponent<DeliveryZone>();
                    deliveryZone.ItemGoal = ItemsToDeliver;
                    deliveryZone.SetMissionController(this);
                    switch (MissionType)
                    {
                        case MissionTypes.Medical:
                            deliveryZone.ItemToLookFor = DeliveryZone.ItemTypes.Medicine;
                            break;
                        default:
                            deliveryZone.ItemToLookFor = DeliveryZone.ItemTypes.Medicine;
                            break;
                    }
                }

                // Pickup Zones - now networked
                GameObject[] puZoneObjs = GameObject.FindGameObjectsWithTag("pickupZone");
                Debug.Log("Found " + puZoneObjs.Length + " pick up zones");
                bool alreadyLoaded = false;
                foreach (GameObject zone in puZoneObjs)
                {
                    if (zone.GetComponent<PickupZone>().Team == PlayerInstance.Instance.ActiveTeam &&
                        zone.GetComponent<PickupZone>().MissionId == missionID)
                    {
                        zone.SetActive(true);
                        zone.GetComponent<Renderer>().enabled = true;

                        zones.Add(zone);
                        alreadyLoaded = true;
                        Debug.Log("Pick up zone exists in the game generated by another player");
                    }
                }
                // If they don't exist at all 
                if (!alreadyLoaded)
                {
                    Debug.Log("Pick up zone does not exist yet -- so instantiate");

                    PickupZoneRef[] zoneLocs = pickupZones.Value;
                    foreach (PickupZoneRef pickupRef in zoneLocs)
                    {
                        GameObject pickupObj = null;
                        object[] data = new object[] { missionID, PlayerInstance.Instance.ActiveTeam,
                               pickupRef.QuantityAtLocation, (int) MissionType };
                    
                        Debug.Log($"Custom data passed in mission {data[0]} {data[1]} {data[2]} {data[3]}");
                        pickupObj = PhotonNetwork.Instantiate(Resources.Load("Pickup Zone").name, pickupRef.Location.position, pickupRef.Location.rotation, 0, data);

                        if (pickupObj)
                        {
                            zones.Add(pickupObj);
                            PickupZone pickupZone = pickupObj.GetComponent<PickupZone>();
                            // for some reason assigning these after doesn;t work for remote player. 
                            // has to be passed in as custom data in photon instantiate
                            pickupZone.TotalNumberOfItems = pickupRef.QuantityAtLocation;
                            pickupZone.MissionId = missionID;

                            switch (MissionType)
                            {
                                case MissionTypes.Delivery:
                                    pickupZone.ZoneType = PickupZone.ZoneTypes.Delivery;
                                    break;
                                case MissionTypes.DroneDelivery:
                                    pickupZone.ZoneType = PickupZone.ZoneTypes.DroneDelivery;
                                    break;
                                case MissionTypes.dog:
                                    pickupZone.ZoneType = PickupZone.ZoneTypes.dog;
                                    break;
                                case MissionTypes.person:
                                    pickupZone.ZoneType = PickupZone.ZoneTypes.person;
                                    break;
                                default: // default Medical to person for now although could be drone delivery too
                                    pickupZone.ZoneType = PickupZone.ZoneTypes.person;
                                    break;
                            }
                            pickupZone.Reset();
                        }
                    }
                }
            }
            else
            {
                // I've already started this mission, set all the zones and cargo to Active
                // this is probably not being used anymore because we're deleting zones in close mission
                foreach (var zone in zones)
                {
                    zone.SetActive(true);
                    zone.GetComponent<Renderer>().enabled = true;
                    PickupZone pickZone = zone.GetComponent<PickupZone>();
                    if (pickZone)
                    {
                        pickZone.Reset(); // do we really want to reset/destroy cargo?
                    }

                }
            }
        }

            if (_startIcon && _startIcon.gameObject)
                _startIcon.gameObject.SetActive(false);

            _computerController.MissionListRef.HideMissions();

            _missionStarted = true;

            _missionDetailPaneScript.UpdateSuccessPane(missionSuccessTitle, missionFrom, missionSuccessMsg, speakerBubble);

            // Label the network player with the current mission id
            if (!_myNetworkPlayer)
            {
                _myNetworkPlayer = GameObject.FindWithTag("myNetworkPlayer");
            }
            _myNetworkPlayer.GetComponent<NetworkPlayerMissionIdHandler>().MissionId = missionID;
            // Add team info just in case
           _myNetworkPlayer.GetComponent<NetworkPlayerMissionIdHandler>().Team = PlayerInstance.Instance.ActiveTeam;

    }


    // Update is called once per frame
    private void Update()
    {
        if (!_missionStarted) return;

        if (MissionType == MissionTypes.Cellular)
        {
            int finished = _cellTowerController.Sum(t => t.IsCleared() ? 1 : 0);

            if (finished != _cellTowerController.Count) return;
            if (!IsFinished)
            {
                TeacherDB.Instance.SaveMissionCompleted(PlayerInstance.Instance.ActiveTeam, missionID);
                IsFinished = true;
            }
        }

        if (MissionType == MissionTypes.Debris &&
            _debrisControllers.ContainsKey(PlayerInstance.Instance.ActiveTeam) &&
            _debrisControllers[PlayerInstance.Instance.ActiveTeam].Count > 0)
        {
            int finishedDebris = _debrisControllers[PlayerInstance.Instance.ActiveTeam].Sum(t => t.IsCleared() ? 1 : 0);
            if (finishedDebris != _debrisControllers[PlayerInstance.Instance.ActiveTeam].Count) return;
            //  Debug.Log("Mission is Done");

            if (!IsFinished)
            {
                TeacherDB.Instance.SaveMissionCompleted(PlayerInstance.Instance.ActiveTeam, missionID);
                IsFinished = true;
            }
        }

        if (MissionType == MissionTypes.Map)
        {
            // Trying list of building locations instead
            int finished = _buildingController.Sum(t => t.IsCleared() ? 1 : 0);
            // Debug.Log("Counting finished zones " + finished + " / " + _buildingController.Count);
            if (finished == _buildingController.Count)
            {
                if (!IsFinished)
                {
                    TeacherDB.Instance.SaveMissionCompleted(PlayerInstance.Instance.ActiveTeam, missionID);
                    IsFinished = true;
                }
            }
        }
    }

    public void FinishMission(string team)
    {
        var completedMissions = _computerController.MissionListRef.GetCompletedMissions();
        if (!completedMissions.Contains(this))
        {
            Debug.Log($"Mission {missionID} Finished for {team}!");

            if (gameObject.tag == "ActiveMission" && team == PlayerInstance.Instance.ActiveTeam)
            {
                _missionAssets.MissionCompleteCanvas.SetActive(true);

                // Store sucess in Firebase if it's a coding mission
                if (_jsTranslator)
                {
                    _jsTranslator.HandleFirebaseUpdate("success", "true");
                    _jsTranslator.HandleFirebaseUpdate("unsubscribe", _jsTranslator.TeamName);
                }    

                // Clear cargo
                if (_activeRobot && _activeRobot.GetComponent<BlockRobotController>()
                    && MissionType != MissionTypes.Cellular && MissionType != MissionTypes.Map)
                    _activeRobot.GetComponent<BlockRobotController>().ClearDeliveredCargo();
            }
            // Add it to the list of completed missions even if it isn't currently active
            _computerController.MissionListRef.CompleteMission(team, this);
        }
    }

    public void ResetMission()
    {
        // Reset pickup and delivery zones
        foreach (GameObject zone in zones)
        {
            if (zone == null) return;
            DeliveryZone delZone = zone.GetComponent<DeliveryZone>();
            PickupZone pickZone = zone.GetComponent<PickupZone>();
            FlyOverZoneScript flyOverZone = zone.GetComponent<FlyOverZoneScript>();
            if (delZone)
            {
                delZone.Reset();
            }
            else if (pickZone)
            {
                pickZone.Reset();
            }
            else if (flyOverZone)
                flyOverZone.Clear();
        }

        // If it is a delivery type mission, reset and cargo and inventory
        if (_activeRobot &&
            (MissionType == MissionTypes.Delivery ||
            MissionType == MissionTypes.DroneDelivery ||
            MissionType == MissionTypes.person ||
            MissionType == MissionTypes.dog))
        {
            _activeRobot.GetComponent<BlockRobotController>().ClearDeliveredCargo();
            _activeRobot.GetComponent<RobotInventory>().Reset();
            _activeRobot.transform.Find("Cargo").gameObject.SetActive(false);
        }
    }

    public void CloseMission()
    {
        Debug.Log("Close Mission!");

        if(_jsTranslator)
        {
            _jsTranslator.HandleFirebaseUpdate("unsubscribe", _jsTranslator.TeamName);
        }
  
        // make mission not active
        if (gameObject.tag == "ActiveMission")
            gameObject.tag = "Untagged";

        if (_activeRobot && _activeRobot.GetComponent<BlockRobotController>())
        {
            _activeRobot.GetComponent<BlockRobotController>().ClearDeliveredCargo();
        }

        _myNetworkPlayer.GetComponent<NetworkPlayerMissionIdHandler>().MissionId = "";

        // If no other players are in this mission, de-activate the robot and zones
        bool active = false;
        NetworkPlayer[] players = FindObjectsOfType<NetworkPlayer>();
        foreach (NetworkPlayer player in players)
        {
            NetworkPlayerMissionIdHandler np = player.GetComponent<NetworkPlayerMissionIdHandler>();
            if (np.MissionId == missionID && np.Team == PlayerInstance.Instance.ActiveTeam)
            {
                Debug.Log("Found another player in mission " + np.name);
                active = true;
            }
        }
        if (!active)
        {
            Debug.Log("Found no other players in mission");
            Hashtable teamHash;
            if (_networkManager.TeamProps.ContainsKey(PlayerInstance.Instance.ActiveTeam))
            {
                teamHash = (Hashtable)_networkManager.TeamProps[PlayerInstance.Instance.ActiveTeam];
                teamHash[missionID] = false;
            }
            else
            {
                teamHash = new Hashtable();
                teamHash.Add(missionID, false);
            }

            Hashtable setValue = new Hashtable();
            setValue.Add(PlayerInstance.Instance.ActiveTeam, teamHash);
            Debug.Log($"Setting hash for ${PlayerInstance.Instance.ActiveTeam} ${teamHash[missionID]}");

            PhotonNetwork.CurrentRoom.SetCustomProperties(setValue);

            // For networked zones
            foreach (GameObject zone in zones)
            {
                if (zone && zone.GetComponent<PickupZone>())
                {
                    zone.GetComponent<PickupZone>().Clear(); // Clear will destroy cargo
                   // in case there is leftover cargo on the map, target and destroy
                    string tag = zone.GetComponent<PickupZone>().ZoneType.ToString();
                    GameObject[] cargoObjs = GameObject.FindGameObjectsWithTag(tag);
                    Debug.Log("Found " + cargoObjs.Length + " object(s) of tag " + tag);
                    foreach (GameObject cargo in cargoObjs)
                    {
                        if (cargo.name.ToLower().StartsWith(tag.ToLower()) &&
                            cargo.GetComponent<CargoToPickup>().Team == PlayerInstance.Instance.ActiveTeam
                            && cargo.GetComponent<CargoToPickup>().MissionId == missionID)
                        {
                            cargo.GetComponent<CargoToPickup>().DestroyCargo();

                        }
                    }
                    // networked destroy zone 
                    zone.SetActive(false);
                    zone.GetComponent<PickupZone>().DestroyZone();
                }
            }
        }
        // Also destroy any local zones and make pickup networked zones invisible for me
        foreach (GameObject zone in zones)
        {
            
            if (zone)
            {
                if (zone.GetComponent<PickupZone>())  // make it invisible for me
                    zone.GetComponent<Renderer>().enabled = false;
                else  // other type of local zone
                {
                    zone.SetActive(false);
                    Destroy(zone);
                }
            }
        }
        // clear all zones list on close mission
        zones.Clear();

        var sections = NewVivoxManager.Instance.CurrentChannel.Split('_');
        if (sections.Length > 1)
           NewVivoxManager.Instance.ChangeChannel(sections[0] + "_" + sections[1]);

        foreach (GameObject icon in GameObject.FindGameObjectsWithTag("Hexagon"))
        {
            Destroy(icon);
        }
        _missionDetailPaneScript.missionIconController.Clear();

    }

    public Button GetMissionIcon()
    {
        return _missionIcon;
    }

    public void ShowMissionIcon()
    {
        // Place icon on the map
        if (_missionIcon)
        {
            _missionIcon.gameObject.SetActive(true);
            _missionIcon.gameObject.transform.SetParent(_missionAssets.MapView.transform);
            _missionIcon.gameObject.name = missionID;
            _missionIcon.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);
            _missionIcon.GetComponent<RectTransform>().anchoredPosition = new Vector3(positionX, positionY);

            // Add click listener to the icon
            _missionIcon.onClick.RemoveAllListeners();
            _missionIcon.onClick.AddListener(LoadMission);

            // Clear any previous mission icons that have been placed.
            foreach (var t in _missionDetailPaneScript.missionIconController)
            {
                Destroy(t.gameObject);
            }
            _missionDetailPaneScript.missionIconController.Clear();

            Image[] images = _missionIcon.GetComponentsInChildren<Image>();
            foreach (Image image in images)
            {
                image.enabled = true;
            }
            _missionIcon.GetComponentInChildren<TMP_Text>().enabled = true;
            _shown = true;
        }
    }

    public void HideMissionIcon()
    {
        if (_missionAssets && _missionAssets.MapView)
        {
            foreach (Transform t in _missionAssets.MapView.transform)
            {
                if (t.name == missionID)
                {
                    Image[] images = t.GetComponentsInChildren<Image>();
                    foreach (Image image in images)
                    {
                        image.enabled = false;
                    }
                    t.GetComponentInChildren<TMP_Text>().enabled = false;
                }
            }
        }
        _shown = false;
    }

    public void DisplayMissionIcon()
    {
        if (!_shown) ShowMissionIcon();

        if (_missionAssets && _missionAssets.MapView)
        {
            foreach (Transform t in _missionAssets.MapView.transform)
            {
                if (t && t.name == missionID)
                {
                    t.gameObject.SetActive(true);
                }
            }
        }
    }

    public void TransferDebrisOwnership()
    {
        foreach (Debris debris in _debrisControllers[PlayerInstance.Instance.ActiveTeam])
        {
            // Only transfer debris ownership for debris associated with this mission.
            if (debris.GetComponent<SetMissionId>().MissionId == missionID)
            {
                PhotonView pv = debris.gameObject.GetComponent<PhotonView>();
                if (!pv.IsMine)
                {
                    pv.TransferOwnership(PhotonNetwork.LocalPlayer);
                }
            }
        }
    }

    public void GenerateDebris()
    {
        Transform[] locs = debrisLocations.Value;
        GameObject[] types = debrisTypes.Value;

        if (!_networkManager)
            _networkManager = FindObjectOfType<NetworkManager>();

        // Check if debris has already been created by another player.
        String team = PlayerInstance.Instance.ActiveTeam;
        if (!_loaded.ContainsKey(team))
        {
            GameObject[] debrisObjects = GameObject.FindGameObjectsWithTag("Debris");
            foreach (GameObject debris in debrisObjects)
            {
                if (debris.GetComponent<SetMissionId>().Team == team && debris.GetComponent<SetMissionId>().MissionId == missionID)
                {
                    if (!_debrisControllers.ContainsKey(team))
                        _debrisControllers.Add(team, new List<Debris>());
                    _debrisControllers[team].Add(debris.GetComponent<Debris>());
                    _computerController.MissionListRef.DebrisList.Add(debris);
                }
            }
            if (_debrisControllers.ContainsKey(PlayerInstance.Instance.ActiveTeam) && _debrisControllers[PlayerInstance.Instance.ActiveTeam].Count() > 0)
            {
                _loaded.Add(team, true);
            }
        }

        // Create the dictionary key for team if it doesn't exist yet
        if (!_loaded.ContainsKey(team) && !_debrisControllers.ContainsKey(team))
        {
            _debrisControllers.Add(team, new List<Debris>());

            // Photon instantiate the debris
            foreach (Transform t in locs)
            {
                if (t != null)
                {
                    float rndScale = Random.Range(0.07f, 0.10f);
                    object[] data = new object[] { missionID, team, rndScale };
                    int r = Random.Range(0, types.Length);
                    GameObject debris = PhotonNetwork.InstantiateRoomObject(types[r].name, t.position, Quaternion.identity, 0, data);
                    // Photon will return null if debris has already been created
                    if (debris)
                    {
                        _debrisControllers[team].Add(debris.GetComponent<Debris>());
                        _computerController.MissionListRef.DebrisList.Add(debris);
                    }
                }
            }
        }
    }

    [Serializable]
    public class DebrisLocations : CollectionWrapper<Transform>
    {
    }

    [Serializable]
    public class BuildingLocations : CollectionWrapper<Transform>
    {
    }

    [Serializable]
    public class DebrisTypes : CollectionWrapper<GameObject>
    {
    }

    [Serializable]
    public class MobileCellTowerLocations : CollectionWrapper<Transform>
    {
    }

    [Serializable]
    public class PickupZones : CollectionWrapper<PickupZoneRef>
    {
    }

    [Serializable]
    public struct PickupZoneRef
    {
        public Transform Location;
        public ZoneTypes ZoneType;
        public int QuantityAtLocation;
    }

    public enum ZoneTypes
    {
        dog,
        Delivery,
        DroneDelivery,
        Medicine,
        person
    }

}
