﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionAssets : MonoBehaviour
{
    public GameObject ChooseAMissionImage;
    public GameObject MissionDetailPane;
    public GameObject ButtonBarCanvas;
    public GameObject TeleOpCanvas;
    public GameObject ProgrammingCanvas;
    public GameObject ProgrammingView;
    public GameObject MonitorCanvas;
    public GameObject FrontCamera; // Virtual Cameras for right pane
    public GameObject OverheadCamera;
    public GameObject RearOverheadCamera;
    public GameObject LeftFrontCamera; // Virtual Cameras for left pane
    public GameObject LeftOverheadCamera;
    public GameObject LeftRearOverheadCamera;
    public GameObject MapView;
    public GameObject MissionNumText;

    public Sprite CellIcon;
    public Sprite DebrisIcon;
    public Sprite DeliveryIcon;
    public Sprite DogIcon;
    public Sprite DroneDeliveryIcon;
    public Sprite MapIcon;
    public Sprite MedicalIcon;
    public Sprite ShuttleIcon;
    public Sprite DeliBotIcon;
    public Sprite EMMAIcon;
    public Sprite RoboVanIcon;
    public Sprite AseoIcon;
    public Sprite DeliDroneIcon;
    public Sprite AthenaIcon;

    public GameObject MissionCompleteCanvas;
    public GameObject speakerBubble;
    public GameObject missionSuccessTitle;
    public GameObject missionFrom;
    public GameObject missionSuccessMsg;
}
