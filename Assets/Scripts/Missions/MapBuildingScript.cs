﻿using System.Collections.Generic;
using UnityEngine;
using SensorToolkit;

public class MapBuildingScript : MonoBehaviour
{
    private RaySensor _sensor;
    
    // Start is called before the first frame update
    void Start()
    {
        _sensor = transform.Find("Sensor").GetComponent<RaySensor>();
    }

    // Update is called once per frame
    void Update()
    {
        List<RaycastHit> hits = _sensor.DetectedObjectRayHits;
        foreach (var hit in hits)
        {
            if (hit.collider.tag == "Building")
            {
                hit.transform.GetComponent<BuildingScript>().Clear();
            }
        }
    }
}
