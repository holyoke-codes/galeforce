﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotInventory : MonoBehaviour
{

    public DeliveryZone DeliveryZone;
    public PickupZone PickupZone;

    public int InventorySize = 1;

    public List<GameObject> Inventory = new List<GameObject>();

    public bool PickupItem()
    {
        if (PickupZone == null || Inventory.Count >= InventorySize || PickupZone.TotalNumberOfItems <= 0) return false;

        // Can pickup item
        var copy = new GameObject
        {
            tag = PickupZone.ZoneType.ToString(),
            name = PickupZone.ZoneType.ToString()
        };
        copy.SetActive(false);
        Inventory.Add(copy);
        PickupZone.Pickup();
        return true;
    }

    public void Reset()
    {
        foreach (GameObject g in Inventory)
            Destroy(g);
        Inventory = new List<GameObject>();
    }

    public bool DropOffItem()
    {
        if ( Inventory.Count <= 0) return false;
        // destroy that copy object - probably should be called in close mission too
        Destroy(Inventory[0]);
        Inventory.RemoveAt(0);

        if (DeliveryZone != null)
        {
            // Can drop off
            DeliveryZone.ItemsInZone++;
        }

        return true;
    }
}
