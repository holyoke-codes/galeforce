﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyOverZoneScript : MonoBehaviour
{
    private bool _cleared;
    // How long the drone needs to stay in the location
    // Might not need timer
    private float timerCountDown = 2.0f;
    private bool isDroneInLocation = false;
    private Renderer _rend = new Renderer();
    private GameObject _activeMission;
    private string _missionId;

    public void Start()
    {
        _activeMission = GameObject.FindGameObjectWithTag("ActiveMission");
        if (_activeMission)
            _missionId = _activeMission.GetComponent<MissionController>().missionID;
  
       _rend = gameObject.GetComponent<MeshRenderer>();
       _rend.enabled = false;
        _cleared = false;
    }
    public bool IsCleared()
    {
        return _cleared;
    }

    public void Clear()
    {
        _cleared = false;
        _rend.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (timerCountDown < 0)
        {
            timerCountDown = 0;
            _cleared = true;
            _rend.enabled = true;
            //Debug.Log("Found fly over zone!");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "ActiveRobot")
        {
            //Debug.Log("touching drone");
            isDroneInLocation = true;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (isDroneInLocation)
        {
            // Countdown the timer when the drone is in the hover zone
            timerCountDown -= Time.deltaTime;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "ActiveRobot")
        {
            //Debug.Log("Exiting hover zone");
            // Reset the timer
            isDroneInLocation = false;
            timerCountDown = 2.0f;
        }
    }
}
