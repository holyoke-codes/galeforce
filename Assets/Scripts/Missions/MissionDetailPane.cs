﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MissionDetailPane : MonoBehaviour
{
    // Mission information 
    private TextMeshProUGUI _bot;
    private TextMeshProUGUI _class;
    private TextMeshProUGUI _description;
    private TextMeshProUGUI _from;
    private Image _image;
    private Image _speechBubble;
    private MissionAssets _missionAssets;

    // Mission success
    private TextMeshProUGUI _missionSuccessTitle;
    private TextMeshProUGUI _missionSuccessMsg;
    public TextMeshProUGUI _missionFrom;
    public Image _missionSpeakerBubble;

    private TextMeshProUGUI _title;
    private TextMeshProUGUI _type;
    public Transform endIcon;
    public Transform middleIcon;
    public GameObject missionBot;
    public Image missionCategoryIcon;
    public GameObject missionClass;
    public GameObject missionDescription;
    public GameObject missionSpeakerBubble;
    public List<Transform> missionIconController = new List<Transform>();
    public Button missionSelectButton;
    public Image robotIcon;
    private Image _boticon;

    [Header("Mission Information:")] public GameObject missionTitle;

    public GameObject missionType;

    [Header("Generic Mission Configuration:")]
    public Transform startIcon;

    // Start is called before the first frame update
    private void Awake()
    {
        _missionAssets = GameObject.FindGameObjectWithTag("MissionAssets").GetComponent<MissionAssets>();
        _title = missionTitle.GetComponent<TextMeshProUGUI>();
        _from = transform.Find("MissionFrom").GetComponent<TextMeshProUGUI>();
        _description = missionDescription.GetComponent<TextMeshProUGUI>();
        _speechBubble = missionSpeakerBubble.GetComponent<Image>();
        _image = missionCategoryIcon.GetComponent<Image>();
        _class = missionClass.GetComponent<TextMeshProUGUI>();
        _bot = missionBot.GetComponent<TextMeshProUGUI>();
        _type = missionType.GetComponent<TextMeshProUGUI>();
        _boticon = robotIcon.GetComponent<Image>();

        _missionSuccessTitle = _missionAssets.MissionCompleteCanvas.transform.Find("ThanksStatement").gameObject.GetComponent<TextMeshProUGUI>();
        _missionFrom = _missionAssets.MissionCompleteCanvas.transform.Find("MissionFrom").gameObject.GetComponent<TextMeshProUGUI>();
        _missionSuccessMsg = _missionAssets.MissionCompleteCanvas.transform.Find("SuccessStatement").gameObject.GetComponent<TextMeshProUGUI>();
        _missionSpeakerBubble = _missionAssets.MissionCompleteCanvas.transform.Find("SpeakerBubble").gameObject.GetComponent<Image>();
    }

    public void UpdateMissionCard(string missionID, string title, string from, string description, Sprite speechBubble)
    {
        _title.text = title;
        _from.text = from;
        _description.text = description;
        _speechBubble.sprite = speechBubble;

        // Get users in room
        Debug.Log($"Fetch Users for mission: {missionID}");
    }

    public void UpdateMissionCategoryIcon(Sprite sprite)
    {
        _image.sprite = sprite;
    }

    public void UpdateRobotIcon(Sprite sprite)
    {
        _boticon.sprite = sprite;
    }

    public void UpdateMissionDetails(string mClass, string mBot, string mType)
    {
        _class.text = mClass;

        switch (mClass)
        {
            case "Cellular":
                UpdateMissionCategoryIcon(_missionAssets.CellIcon);
                break;
            case "Debris":
                UpdateMissionCategoryIcon(_missionAssets.DebrisIcon);
                break;
            case "Delivery":
                UpdateMissionCategoryIcon(_missionAssets.DeliveryIcon);
                break;
            case "Dog":
                UpdateMissionCategoryIcon(_missionAssets.DogIcon);
                break;
            case "DroneDelivery":
                UpdateMissionCategoryIcon(_missionAssets.DroneDeliveryIcon);
                break;
            case "Map":
                UpdateMissionCategoryIcon(_missionAssets.MapIcon);
                break;
            case "Medical":
                UpdateMissionCategoryIcon(_missionAssets.MedicalIcon);
                break;
            case "Shuttle":
                UpdateMissionCategoryIcon(_missionAssets.ShuttleIcon);
                break;
            default:
                break;
        }

        _bot.text = mBot;
        _type.text = mType;
       // new robot icons
       switch(mBot)
        {
            case "Aseo":
                UpdateRobotIcon(_missionAssets.AseoIcon);
                break;
            case "DeliBot":
                UpdateRobotIcon(_missionAssets.DeliBotIcon);
                break;
            case "EMMA":
                UpdateRobotIcon(_missionAssets.EMMAIcon);
                break;
            case "RoboVan":
                UpdateRobotIcon(_missionAssets.RoboVanIcon);
                break;
            case "DeliDrone":
                UpdateRobotIcon(_missionAssets.DeliDroneIcon);
                break;
            case "Athena":
                UpdateRobotIcon(_missionAssets.AthenaIcon);
                break;

        }
    }

    public void UpdateSuccessPane(string _title, string _from, string _msg, Sprite _sprite)
    {
        _missionSuccessTitle.text = _title;
        _missionFrom.text = _from;
        _missionSuccessMsg.text = _msg;
        _missionSpeakerBubble.sprite = _sprite;
    }
}