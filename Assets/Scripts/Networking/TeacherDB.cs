﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Serialization;

public class TeacherDB : MonoBehaviour
{

    public DB PublicDb = new DB();

    public string SessionCode = "RT5QC";

    public static TeacherDB Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null) { Instance = this; }
        else { Destroy(gameObject); }


        SessionCode = PlayerPrefs.GetString("sessionCode");

    }

    void Start()
    {
        InvokeRepeating("GetDbUpdate", 1.0f, 1.0f);
    }

    void GetDbUpdate()
    {
        StartCoroutine(SendRequest());
    }

    IEnumerator SendRequest()
    {
        using (var webRequest = UnityWebRequest.Get($"https://us-central1-savedblocks.cloudfunctions.net/galeforce/session?session_code={SessionCode}"))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();
            PublicDb = JsonUtility.FromJson<DB>(webRequest.downloadHandler.text);

            var args = new DbUpdatedEventArgs
            {
                db = PublicDb
            };
            OnDBUpdated(args);
        }
    }

    public void UpdatePlayerLocation(string name, string location)
    {
        StartCoroutine(UpdateLocationRequest(name, location));
    }

    IEnumerator UpdateLocationRequest(string name, string location)
    {
        using (var webRequest = UnityWebRequest.Post($"https://us-central1-savedblocks.cloudfunctions.net/galeforce/playerlocation?session_code={SessionCode}&player_name={name}&location={location}", ""))
        {
            yield return webRequest.SendWebRequest();
        }
    }

    public void SaveMissionCompleted(string team, string missionId)
    {
        StartCoroutine(UpdateMissionsRequest(team, missionId));
    }

    IEnumerator UpdateMissionsRequest(string team, string missionId)
    {
        using (var webRequest = UnityWebRequest.Post($"https://us-central1-savedblocks.cloudfunctions.net/galeforce/completemission?session_code={SessionCode}&team_name={team}&mission_id={missionId}", ""))
        {
            yield return webRequest.SendWebRequest();
        }
    }

    protected virtual void OnDBUpdated(DbUpdatedEventArgs e)
    {
        DbUpdated?.Invoke(this, e);
    }

    public event EventHandler<DbUpdatedEventArgs> DbUpdated;
}

[Serializable]
public class DB
{
    public string screen_url = "";
    public bool mute = false;
    public bool auto_play = false;
    public bool unlock_map = false;
    public TeamMissions[] teams = new TeamMissions[0];
}

[Serializable]
public class TeamMissions
{
    public string team_name = "";
    public string control_stage = "";
    public string training_stage = "";
    public string[] completed_missions = new string[0];
}

public class DbUpdatedEventArgs : EventArgs
{
    public DB db { get; set; }
}
