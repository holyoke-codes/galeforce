﻿using Photon.Pun;
using UnityEngine;

public class NetworkPlayerMissionIdHandler : MonoBehaviour, IPunObservable
{
    public string MissionId;
    public string Team;

    public PhotonView view;

    void IPunObservable.OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(MissionId);
            stream.SendNext(PlayerInstance.Instance.ActiveTeam);
        }
        else if (stream.IsReading)
        {
            MissionId = (string)stream.ReceiveNext();
            Team = (string)stream.ReceiveNext();
        }
    }
}