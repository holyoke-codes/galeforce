﻿using UnityEngine;
using Vuplex.WebView;

public class LargeDisplayController : MonoBehaviour
{

    private WebViewPrefab _webViewPrefab;

    private DB _localCopy;

    void Start()
    {
        _webViewPrefab = GetComponentInChildren<WebViewPrefab>();
        _localCopy = new DB();
        TeacherDB.Instance.DbUpdated += NewDbUpdate;
    }

    public void NewDbUpdate(object sender, DbUpdatedEventArgs args)
    {
        if (args.db.screen_url != _localCopy.screen_url)
        {
            _webViewPrefab.WebView.LoadUrl(args.db.screen_url);
            _webViewPrefab.WebView.LoadProgressChanged += PageLoadProgress;
            _localCopy.screen_url = args.db.screen_url;
        }
    }

    public void PageLoadProgress(object sender, ProgressChangedEventArgs args)
    {
        if (_localCopy.screen_url.Contains("youtube.com") || _localCopy.screen_url.Contains("https://galeforce.netlify.app/video"))
        {
            if ((args.Progress >= 1f))
            {
                Debug.Log("Loaded YouTube!");
                _webViewPrefab.WebView.Click(new Vector2(0.5f, 0.5f));
            }
        }
    }
}
