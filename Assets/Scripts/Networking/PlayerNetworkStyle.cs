﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

public class PlayerNetworkStyle : MonoBehaviourPunCallbacks
{
    public string PlayerDisplayName;
    public string PlayerAvatar;
    public string PlayerHair;
    public string[] PlayerColors;

    private NetworkPlayer _networkPlayer;
    private Animator _animator;

    private Renderer _renderer;
    private List<Material> _materials;
    private GameObject _playerModel;
    private PhotonView _pv;

    public List<AvatarReference> Avatars;

    private void Awake()
    {
        _pv = GetComponent<PhotonView>();
        _animator = GetComponent<Animator>();
        _networkPlayer = GetComponent<NetworkPlayer>();
    }

    public void Update()
    {
        // Handle custom properties for players who are already in the game
        // Maybe there's a better way to do this, but it is working. 
        foreach (Player player in PhotonNetwork.PlayerList)
        {
            if (player == _pv.Owner)
            {
                HandleCustomProperties(player.CustomProperties);
            }
        }
    }

    public void SetProperties(string displayName, string playerAvatar, string playerHair, string[] playerColors)
    {
        Hashtable styleHash = new Hashtable() { { "PlayerDisplayName", displayName }, { "PlayerAvatar", playerAvatar }, { "PlayerHair", playerHair }, { "PlayerColors", playerColors } };
        PhotonNetwork.LocalPlayer.SetCustomProperties(styleHash);
    }

    public override void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps)
    {
        base.OnPlayerPropertiesUpdate(targetPlayer, changedProps);
        // Ignore the customer properties unless they belong to this player
        if (!_pv.Owner.Equals(targetPlayer)) return;

        HandleCustomProperties(changedProps);
    }

    private void HandleCustomProperties(Hashtable props)
    {
        if (props.ContainsKey("PlayerDisplayName"))
        {
            PlayerDisplayName = (string)props["PlayerDisplayName"];
        }

        if (props.ContainsKey("PlayerAvatar"))
        {
            PlayerAvatar = (string)props["PlayerAvatar"];
        }

        if (props.ContainsKey("PlayerHair"))
        {
            PlayerHair = (string)props["PlayerHair"];
        }

        if (props.ContainsKey("PlayerColors"))
        {
            PlayerColors = (string[])props["PlayerColors"];
        }

        if (PlayerDisplayName != null && PlayerAvatar != null && PlayerHair != null && PlayerColors != null)
        {
            // Only run when all data has arrived
            UpdatePlayerInfo();
        }
    }

    public void UpdatePlayerInfo()
    {
        if (_playerModel == null)
        {
            if (PlayerAvatar == "") return;

            _playerModel = Instantiate(Resources.Load("PlayerModels/" + PlayerAvatar + "_Model"), transform) as GameObject;

            Avatar modelAvatar = Avatars.Find(a => a.CharName == PlayerAvatar).AvatarToUse;
            _animator.avatar = modelAvatar;
            _animator.Rebind();

            _networkPlayer.Body = _playerModel;
        }

        GameObject hair;
        Color hairColor;
        Color skinColor;
        Color shirtColor;
        Color pantsColor;

        ColorUtility.TryParseHtmlString(PlayerColors[0], out hairColor);
        ColorUtility.TryParseHtmlString(PlayerColors[1], out skinColor);
        ColorUtility.TryParseHtmlString(PlayerColors[2], out shirtColor);
        ColorUtility.TryParseHtmlString(PlayerColors[3], out pantsColor);

        foreach (Transform child in _playerModel.transform)
        {
            if (child.tag == "hair")
            {
                if (child.name == PlayerHair)
                {
                    hair = child.gameObject;
                    hair.GetComponent<Renderer>().material.color = hairColor;
                    hair.SetActive(true);
                }
                else
                {
                    child.gameObject.SetActive(false);
                }
            }
        }

        // Get the renderer for the Body, find the material with the specified Name, set the color of that material
        // update that material in the list, and change the materials on the Body.
        _renderer = _playerModel.transform.Find("Body").GetComponent<Renderer>();
        _materials = _renderer.materials.ToList();
        UpdateAvatarColor("Skin", skinColor);
        UpdateAvatarColor("OverShirt", shirtColor);
        UpdateAvatarColor("Pants", pantsColor);
        _renderer.materials = _materials.ToArray<Material>();
    }

    private void UpdateAvatarColor(string objName, Color color)
    {
        // The material name has some extra text in it
        Material objMat = _materials.Find(x => x.name.Replace(" (Instance)", "") == (objName));
        if (objMat)
        {
            int i = _materials.IndexOf(objMat);
            objMat.color = color;
            _materials[i] = objMat;
        }
    }
}

[Serializable]
public class AvatarReference
{
    public string CharName;
    public Avatar AvatarToUse;
}