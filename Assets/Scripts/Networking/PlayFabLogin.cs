﻿using PlayFab;
using PlayFab.ClientModels;
using PlayFab.GroupsModels;
using UnityEngine;
using EntityKey = PlayFab.ClientModels.EntityKey;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.SceneManagement;

public class PlayFabLogin : MonoBehaviour
{
    public JSTranslator JSTranslator;
    public Transform FPPlayer;
    public AvatarConfiguration avatarConfig;
    public PlayerLoginController PlayerLoginControllerRef;
    public LoginPageTranslator LoginPageTranslator;

    private string _playFabId;
    private EntityKey _entityKey;

    private GameData _data;

    private string _playerName = "";
    private string[] _colors;
    private string _avatar;
    private string _hair;

    public bool ResetPlayerInfo = false;
    public string ActiveTeam;
    public string GroupId;
    public bool IsTeacher = false;
    public bool IsResearcher = false;

    public List<PlayfabGroup> PlayfabGroups = new List<PlayfabGroup>();

    private void Start()
    {
        if(PlayerInstance.Instance)
        {
            FPPlayer = PlayerInstance.Instance.transform;

            if (PlayerInstance.Instance.LastScene == PlayerInstance.Instance.CurrentScene)
            {
                LoadSaveData();
            }
        }
        else
        {
            LoadSaveData();
        }

        // Four Playfab groups to store mission success
        PlayfabGroup team1 = new PlayfabGroup("D17D42188E70FA72", "team1");
        PlayfabGroups.Add(team1);

        PlayfabGroup team2 = new PlayfabGroup("7EF33D15663CB87E", "team2");
        PlayfabGroups.Add(team2);

        PlayfabGroup team3 = new PlayfabGroup("5A91978D684C0B01", "team3");
        PlayfabGroups.Add(team3);

        PlayfabGroup team4 = new PlayfabGroup("6C482753BC938B6B", "team4");
        PlayfabGroups.Add(team4);

        //PlayfabGroup team0 = new PlayfabGroup("5B681F9EEE47539D", "TeamZero");
    }

    private void LoadSaveData()
    {
        _data = SaveSystem.LoadGameData();

        if (_data == null || _data.PlayerId == null)
        {
            _data = new GameData(System.Guid.NewGuid().ToString());
            SaveSystem.SaveGameData(_data);
        }
        else
        {
            if (ResetPlayerInfo)
            {
                _data.PlayerId = System.Guid.NewGuid().ToString();
                SaveSystem.SaveGameData(_data);
            }
        }
        var request = new LoginWithCustomIDRequest { CustomId = _data.PlayerId, CreateAccount = true };
        PlayFabClientAPI.LoginWithCustomID(request, OnLoginSuccess, OnLoginFailure);
    }

    private void OnLoginSuccess(LoginResult result)
    {
        Debug.Log("Logged in with PlayFab!");

        _playFabId = result.PlayFabId;
        _entityKey = result.EntityToken.Entity;

        var request = new GetAccountInfoRequest { PlayFabId = _playFabId };

        PlayFabClientAPI.GetAccountInfo(request, OnGetAccountInfoSuccess, OnGetAccountInfoFailure);
    }

    private void OnLoginFailure(PlayFabError error)
    {
        Debug.LogError("Failed to login with PlayFab" + error.GenerateErrorReport());
    }

    private void OnGetAccountInfoSuccess(GetAccountInfoResult result)
    {

        _playerName = result.AccountInfo.TitleInfo.DisplayName;
        if (PlayerLoginControllerRef)
        {
            PlayerLoginControllerRef.DisplayName = _playerName;
            Debug.Log(_playerName);
            LoginPageTranslator.EmitWebViewMessage();
        }

        // Send Player Name to JSTranslator
        if (JSTranslator)
        {
            JSTranslator.HandleFirebaseUpdate("player", _playerName);
            JSTranslator.PlayerName = _playerName;
        }

        var request = new GetUserDataRequest() { PlayFabId = _playFabId };
        PlayFabClientAPI.GetUserData(request, OnGetUserDataSuccess, OnGetUserDataFailure);
    }

    private void OnGetAccountInfoFailure(PlayFabError error)
    {
        Debug.LogError("Failed to get player username" + error.GenerateErrorReport());
    }

    private void OnGetUserDataSuccess(GetUserDataResult result)
    {
        UserDataRecord avatarRecord;
        UserDataRecord hairRecord;
        UserDataRecord hairColorRecord;
        UserDataRecord skinColorRecord;
        UserDataRecord shirtRecord;
        UserDataRecord pantsRecord;
        UserDataRecord teacherRecord;
        UserDataRecord researcherRecord;


        result.Data.TryGetValue("AvatarModel", out avatarRecord);
        result.Data.TryGetValue("HairModel", out hairRecord);
        result.Data.TryGetValue("HairColor", out hairColorRecord);
        result.Data.TryGetValue("SkinColor", out skinColorRecord);
        result.Data.TryGetValue("ShirtColor", out shirtRecord);
        result.Data.TryGetValue("PantsColor", out pantsRecord);
        result.Data.TryGetValue("Teacher", out teacherRecord);
        result.Data.TryGetValue("Researcher", out researcherRecord);

        _avatar = avatarRecord.Value;
        _hair = hairRecord.Value;

        if (teacherRecord != null)
        {
            IsTeacher = bool.Parse(teacherRecord.Value);
        }
        if (researcherRecord != null)
        {
            IsResearcher = bool.Parse(teacherRecord.Value);
        }
        var hairColor = hairColorRecord == null ? "FFFFFF" : hairColorRecord.Value;
        var skinColor = skinColorRecord == null ? "FFFFFF" : skinColorRecord.Value;
        var shirtColor = shirtRecord == null ? "#FFFFFF" : shirtRecord.Value;
        var pantsColor = pantsRecord == null ? "#FFFFFF" : pantsRecord.Value;

        if (PlayerLoginControllerRef)
        {
            PlayerLoginControllerRef.PlayerInfo[2] = "#" + hairColor;
            PlayerLoginControllerRef.PlayerInfo[3] = "#" + skinColor;
            PlayerLoginControllerRef.PlayerInfo[4] = "#" + shirtColor;
            PlayerLoginControllerRef.PlayerInfo[5] = "#" + pantsColor;
        }

        _colors = new string[] { hairColor, skinColor, shirtColor, pantsColor };

        Color color_hairColor, color_skinColor, color_shirtColor, color_pantsColor;
        ColorUtility.TryParseHtmlString(hairColorRecord.Value, out color_hairColor);
        ColorUtility.TryParseHtmlString(skinColorRecord.Value, out color_skinColor);
        ColorUtility.TryParseHtmlString(shirtRecord.Value, out color_shirtColor);
        ColorUtility.TryParseHtmlString(pantsRecord.Value, out color_pantsColor);

        GameObject player = null;
        if (SceneManager.GetActiveScene().name == "LoginScene")
        {
            if (!FPPlayer)
                FPPlayer = PlayerInstance.Instance.transform;
            foreach (Transform child in FPPlayer)
            {
                if (child.name.Contains("Clone"))
                {
                    Destroy(child.gameObject);
                }
            }
            List<Object> avatars = Resources.LoadAll("Players").ToList<Object>();
            int avatar_index = 0;
            foreach (GameObject avatar in avatars)
            {
                if (avatar.name == _avatar)
                {
                    avatarConfig.I = avatar_index;
                    break;
                }
                avatar_index++;
            }
            player = Instantiate(Resources.Load("Players/" + _avatar), FPPlayer) as GameObject;
            avatarConfig.Avatar = player;
        }
        else
        {
            if (PlayerInstance.Instance.gameObject.transform.childCount <= 1)
            {
                if (!FPPlayer)
                    FPPlayer = PlayerInstance.Instance.transform;
                player = Instantiate(Resources.Load("Players/" + _avatar), FPPlayer) as GameObject;
            }
            else
            {
                player = PlayerInstance.Instance.gameObject.transform.GetChild(1).gameObject;
            }
        }

        // If we are in the Login scene, also update the avatar config options
        if (avatarConfig)
        {
            avatarConfig.HairColor = color_hairColor;
            avatarConfig.SkinColor = color_skinColor;
            avatarConfig.ShirtColor = color_shirtColor;
            avatarConfig.PantsColor = color_pantsColor;

            int index = 0;
            foreach (Transform child in player.transform)
            {
                if (child.tag == "hair")
                {
                    if (child.name == _hair)
                    {
                        avatarConfig.HairIndex = index;
                        child.GetComponent<Renderer>().material.color = color_hairColor;
                        child.gameObject.SetActive(true);
                    }
                    else
                    {
                        child.gameObject.SetActive(false);
                    }
                    index++;
                }
            }
            avatarConfig.HandleUpdate();
        }
        else
        {
            if (player != null)
            {
                foreach (Transform child in player.transform)
                {
                    if (child.tag == "hair")
                    {
                        if (child.name == _hair)
                        {
                            child.GetComponent<Renderer>().material.color = color_hairColor;
                            child.gameObject.SetActive(true);
                        }
                        else
                        {
                            child.gameObject.SetActive(false);
                        }
                    }
                }
            }
        }

        // Get the renderer for the Body, find the material with the specified Name, set the color of that material
        // update that material in the list, and change the materials on the Body
        Renderer renderer = player.transform.Find("Body").GetComponent<Renderer>();
        List<Material> materials = renderer.materials.ToList();
        // The material name has some extra text in it
        Material skinMat = materials.Find(x => x.name.Replace(" (Instance)", "") == ("Skin"));
        if (skinMat)
        {
            int i = materials.IndexOf(skinMat);
            skinMat.color = color_skinColor;
            materials[i] = skinMat;
        }

        Material shirtMat = materials.Find(x => x.name.Replace(" (Instance)", "") == ("OverShirt"));
        if (shirtMat)
        {
            int i = materials.IndexOf(shirtMat);
            shirtMat.color = color_shirtColor;
            materials[i] = shirtMat;
        }

        Material pantsMat = materials.Find(x => x.name.Replace(" (Instance)", "") == ("Pants"));
        if (pantsMat)
        {
            int i = materials.IndexOf(pantsMat);
            pantsMat.color = color_pantsColor;
            materials[i] = pantsMat;
        }

        renderer.materials = materials.ToArray<Material>();

        Animator anim = player.GetComponent<Animator>();
        KeyboardMovement keyboardMovement = FPPlayer.GetComponent<KeyboardMovement>();
        if (keyboardMovement)
            keyboardMovement.CharacterAnim = anim;

        var request = new ListMembershipRequest() { Entity = new PlayFab.GroupsModels.EntityKey() { Id = _entityKey.Id, Type = _entityKey.Type } };
        PlayFabGroupsAPI.ListMembership(request, OnListMembershipSuccess, OnListMembershipFailure);

        // Join the TrainingComplex team in Photon and load the missions
        string teamName = "TrainingComplex";
        ActiveTeam = teamName;

        NetworkManager networkManager = NetworkManager.Instance;
        if (networkManager)
        {
            Debug.Log("Logging into Photon");
            networkManager.LogIntoPhoton(_playerName, _playFabId, teamName, _avatar, _hair, _colors);
        }
    }

    private void OnGetUserDataFailure(PlayFabError error)
    {
        Debug.LogError("Failed to get Account Info: " + error.GenerateErrorReport());
    }

    private void OnListMembershipSuccess(ListMembershipResponse result)
    {
        foreach (var group in result.Groups)
        {
            Debug.Log(group);
        }
    }

    private void OnListMembershipFailure(PlayFabError error)
    {
        Debug.LogError("Failed to get memberships" + error.GenerateErrorReport());
    }

    public void JoinPlayFabGroups()
    {

        // Join 4 PlayFab Groups, used for syncing mission success for teams
        foreach (PlayfabGroup group in PlayfabGroups)
        {
            JoinTeam(group);
        }
    }

    private void JoinTeam(PlayfabGroup group)
    {
        PlayFabCloudScriptAPI.ExecuteEntityCloudScript(new PlayFab.CloudScriptModels.ExecuteEntityCloudScriptRequest
        {
            FunctionName = "addMember",
            FunctionParameter = new { GroupId = group.Id },
            GeneratePlayStreamEvent = true
        }, resulter =>
        {
            if ((bool)resulter.FunctionResult == true)
            {
                //Add into group successfully
                Debug.Log("Joined " + group.Name);
            }
            else
            {
                //Can't join this group, maybe blocked by your logic or an error occurred in AddMembers call.
                Debug.Log("Failed to join group " + group.Name);
            }
        }, error => { Debug.LogError(error.GenerateErrorReport()); });
    }

    public string GetPlayfabGroupId(string team)
    {
        foreach (PlayfabGroup group in PlayfabGroups)
        {
            if (group.Name == team)
            {
                return group.Id;
            }
        }
        return null;
    }

    public struct PlayfabGroup
    {
        public string Id;
        public string Name;
        public PlayfabGroup(string id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}
