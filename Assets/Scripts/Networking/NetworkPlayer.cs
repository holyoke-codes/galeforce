﻿using Photon.Pun;
using UnityEngine;

public class NetworkPlayer : MonoBehaviour
{
    public Animator Anim;
    public GameObject Body;
    private PhotonView _pv;

    // Start is called before the first frame update
    private void Start()
    {
        _pv = GetComponent<PhotonView>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (Body != null && _pv.IsMine)
        {
            Body.SetActive(false);
        }

        if (!_pv.IsMine) return;
        var player = PlayerInstance.Instance.transform;

        Anim.SetInteger("Emote", player.GetComponent<KeyboardMovement>().CharacterAnim.GetInteger("Emote"));
        Anim.SetFloat("InputH", player.GetComponent<KeyboardMovement>().CharacterAnim.GetFloat("InputH"));
        Anim.SetFloat("InputV", player.GetComponent<KeyboardMovement>().CharacterAnim.GetFloat("InputV"));

        transform.position = player.position;
        transform.rotation = player.rotation;
    }

    private void OnDestroy()
    {
        if (_pv.IsMine)
            PhotonNetwork.Destroy(this.gameObject);
    }
}