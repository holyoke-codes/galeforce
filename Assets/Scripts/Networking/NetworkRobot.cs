﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class NetworkRobot : MonoBehaviour
{

    public GameObject Body;
    private PhotonView _pv;
    private GameObject _cargo;
    private NetworkCargoHandler _networkCargoHandler;
    private NetworkRobotSync _networkRobotSync;
    public string MissionId;
    public string Team;

    // Start is called before the first frame update
    void Start()
    {
        _pv = GetComponent<PhotonView>();

        Transform cargoTransform = transform.Find("Cargo");
        if (cargoTransform)
        {
            _cargo = cargoTransform.gameObject;
            _cargo.SetActive(false);
        }

        // If it's my robot, set the mission id from the mission controller
        _networkRobotSync = GetComponent<NetworkRobotSync>();
        if (_pv.IsMine)
        {
            var activeMission = GameObject.FindGameObjectWithTag("ActiveMission");
            if (activeMission)
            {
                _networkRobotSync.MissionId = activeMission.GetComponent<MissionController>().missionID;
            }
        }

        _networkCargoHandler = GetComponent<NetworkCargoHandler>();

    }

    // Update is called once per frame
    void Update()
    {
        MissionId = _networkRobotSync.MissionId;

        if (!_pv.IsMine)
        {
            if (_cargo && _networkCargoHandler)
                _cargo.SetActive(_networkCargoHandler.isCarryingCargo);
            return;
        }
    }
}
