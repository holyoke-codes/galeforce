﻿using System;
using UnityEngine;
using Vuplex.WebView;
using Random = UnityEngine.Random;

public class LoginPageTranslator : MonoBehaviour
{

    private CanvasWebViewPrefab _webViewPrefab;

    public PlayerLoginController PlayerLoginControllerRef;
    public AvatarConfiguration AvatarConfiguration;
    public GameObject LoadingPanel;
    private GameObject _avatar;
    private GameObject _avatarPanel;

    // Start is called before the first frame update
    void Start()
    {
        _webViewPrefab = GetComponent<CanvasWebViewPrefab>();
        _webViewPrefab.Initialized += (sender, e) =>
        {
            _webViewPrefab.WebView.MessageEmitted += WebViewMessageEmitted;
            _webViewPrefab.WebView.LoadProgressChanged += (send, ev) =>
            {
                if (ev.Progress >= 1.0f)
                {
                    Debug.Log("DONE LOADING!");
                    LoadingPanel.SetActive(false);
                }
            };
        };
        _avatar = GameObject.Find("AvatarCustomizerPlayerPosition");
        _avatarPanel = GameObject.Find("AvatarPanel");
        Debug.Log(PlayerPrefs.GetString("sessionCode"));
    }

    public void EmitWebViewMessage()
    {
        var currentVals = new LoginInfo();
        currentVals.DisplayName = PlayerLoginControllerRef.DisplayName;
        currentVals.AvatarModel = PlayerLoginControllerRef.PlayerInfo[0];
        currentVals.HairModel = PlayerLoginControllerRef.PlayerInfo[1];
        currentVals.HairColor = PlayerLoginControllerRef.PlayerInfo[2];
        currentVals.SkinColor = PlayerLoginControllerRef.PlayerInfo[3];
        currentVals.ShirtColor = PlayerLoginControllerRef.PlayerInfo[4];
        currentVals.PantsColor = PlayerLoginControllerRef.PlayerInfo[5];
        currentVals.SessionCode = PlayerPrefs.GetString("sessionCode");
        _webViewPrefab.WebView.PostMessage(JsonUtility.ToJson(currentVals));
    }

    private void WebViewMessageEmitted(object sender, EventArgs<string> eventArgs)
    {
        // Plain JSON String
        var jsonString = eventArgs.Value;

        if (jsonString.Contains("REQUEST_FOR_INFO"))
        {
            EmitWebViewMessage();
        } 
        else if (!jsonString.Contains("ACTIVE_ELEMENT_TYPE_CHANGED"))
        {
            JsonEvent jsonEvent = JsonUtility.FromJson<JsonEvent>(jsonString);
            switch (jsonEvent.action)
            {
                case "avatar":
                    {
                        if (jsonEvent.value == "next")
                        {
                            AvatarConfiguration.HandleNextAvatar();
                        }
                        else
                        {
                            AvatarConfiguration.HandlePrevAvatar();
                        }
                    }
                    break;

                case "hair":
                    if (jsonEvent.value == "next")
                    {
                        AvatarConfiguration.HandleNextHair();
                    }
                    else
                    {
                        AvatarConfiguration.HandlePrevHair();
                    }
                    break;

                case "hairColor":
                    Color color;
                    ColorUtility.TryParseHtmlString(jsonEvent.value, out color);
                    AvatarConfiguration.HandleColorChange(jsonEvent.action, color);
                    break;

                case "skinColor":
                    ColorUtility.TryParseHtmlString(jsonEvent.value, out color);
                    AvatarConfiguration.HandleColorChange(jsonEvent.action, color);
                    break;

                case "pantsColor":
                    ColorUtility.TryParseHtmlString(jsonEvent.value, out color);
                    AvatarConfiguration.HandleColorChange(jsonEvent.action, color);
                    break;

                case "shirtColor":
                    ColorUtility.TryParseHtmlString(jsonEvent.value, out color);
                    AvatarConfiguration.HandleColorChange(jsonEvent.action, color);
                    break;

                case "play":
                    var connectionInfo = jsonEvent.value.Split(',');
                    var playerName = connectionInfo[0];
                    var sessionCode = connectionInfo[1];
                    
                    PlayerPrefs.SetString("sessionCode", sessionCode);
                    
                    PlayerLoginControllerRef.DisplayName = playerName ?? "Player";
                    AudioRecorder.Instance.StartRecording(playerName ?? "Unknown" + Random.Range(0, 1000));
                    PlayerLoginControllerRef.PlayerInfo[0] = AvatarConfiguration.Avatar.name.Replace("(Clone)", "") ?? "PlayerModel_1";
                    if (AvatarConfiguration.Hair)
                        PlayerLoginControllerRef.PlayerInfo[1] = AvatarConfiguration.Hair.name;
                    PlayerLoginControllerRef.PlayerInfo[2] = "#" + ColorUtility.ToHtmlStringRGB(AvatarConfiguration.HairColor) ?? "#FFFFFF";
                    PlayerLoginControllerRef.PlayerInfo[3] = "#" + ColorUtility.ToHtmlStringRGB(AvatarConfiguration.SkinColor) ?? "#FFFFFF";
                    PlayerLoginControllerRef.PlayerInfo[4] = "#" + ColorUtility.ToHtmlStringRGB(AvatarConfiguration.ShirtColor) ?? "#FFFFFF";
                    PlayerLoginControllerRef.PlayerInfo[5] = "#" + ColorUtility.ToHtmlStringRGB(AvatarConfiguration.PantsColor) ?? "#FFFFFF";

                    LoadingPanel.SetActive(true);
                    PlayerLoginControllerRef.SavePlayInfo();
                    break;

                case "hideAvatar":
                    _avatarPanel.SetActive(false);
                    break;

                case "showAvatar":
                    _avatarPanel.SetActive(true);
                    break;

                case "quit":
                    Application.Quit();
                    break;
            }
        }
    }
}

[Serializable]
public class LoginInfo
{
    public string DisplayName;
    public string AvatarModel;
    public string HairModel;
    public string HairColor;
    public string SkinColor;
    public string ShirtColor;
    public string PantsColor;
    public string SessionCode;
}

public class JsonEvent
{
    public string action;
    public string value;
}
