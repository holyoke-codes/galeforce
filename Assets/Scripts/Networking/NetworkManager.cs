﻿using System;
using System.Collections;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.SceneManagement;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class NetworkManager : MonoBehaviourPunCallbacks, IConnectionCallbacks
{
    public GameObject Player;
    private string _displayName;
    private string _playFabId;
    private Color _hairColor;
    private Color _skinColor;
    private Color _shirtColor;
    private Color _pantsColor;
    private string[] _playerColors;
    private string _playerAvatar;
    private string _playerHair;

    public string RoomName = "test room";
    public string CurrentRoom = "TrainingComplex";
    public string LastRoom = "TrainingComplex";
    public string GoToRoom;
    private string _targetRoom;
    public string DestinationScene;

    private Action _leftRoomAction;

    private Vector3 _pos;

    private bool _firstLoad = true;

    private LoadBalancingClient _loadBalancingClient;
    private AppSettings _appSettings;


    public static NetworkManager Instance { get; private set; }

    public MissionList MissionList;

    public Hashtable TeamProps;

    public void Awake()
    {
        if (Instance == null) { Instance = this; }
        else { Destroy(gameObject); }
        DontDestroyOnLoad(this.gameObject);
        _loadBalancingClient = new LoadBalancingClient();
        _appSettings = PhotonNetwork.PhotonServerSettings.AppSettings;
        TeamProps = new Hashtable();
        TeamProps.Add("team1", new Hashtable());
        TeamProps.Add("team2", new Hashtable());
        TeamProps.Add("team3", new Hashtable());
        TeamProps.Add("team4", new Hashtable());
    }

    public void LogIntoPhoton(string displayName, string playFabId, string teamName, string avatar, string hair, string[] colors)
    {
        // TODO: This is being called when switching scenes when it should not, could be the source of some issues
        _displayName = displayName;
        _playFabId = playFabId;
        RoomName = teamName;
        _playerAvatar = avatar;
        _playerHair = hair;
        _playerColors = colors;

        ColorUtility.TryParseHtmlString(colors[0], out _hairColor);
        ColorUtility.TryParseHtmlString(colors[1], out _skinColor);
        ColorUtility.TryParseHtmlString(colors[2], out _shirtColor);
        ColorUtility.TryParseHtmlString(colors[3], out _pantsColor);

        Debug.Log("Starting Connection");
        //PlayerPrefs.DeleteAll();

        if (_firstLoad)
            ConnectToPhoton();
    }

    public void ConnectToPhoton()
    {
        if (!PhotonNetwork.IsConnected)
        {
            PhotonNetwork.GameVersion = "0.1";
            PhotonNetwork.AutomaticallySyncScene = false;
            PhotonNetwork.ConnectUsingSettings();
            Debug.Log("Connecting to network...");
            NewVivoxManager.Instance.LoginUser(_displayName);
        }
    }

    public void AttemptJoinRoom(string RoomName)
    {
        Debug.Log(PhotonNetwork.CurrentRoom);
        if (PhotonNetwork.CurrentRoom != null)
        {
            PhotonNetwork.LeaveRoom();
            _targetRoom = RoomName;
            _leftRoomAction += JoinCallback;
        }
        else
        {
            JoinRoom(RoomName);
        }
    }

    private void JoinCallback()
    {
        Debug.Log("OnLeftRoom");

        if (!string.IsNullOrEmpty(Instance.DestinationScene) && Instance.DestinationScene != SceneManager.GetActiveScene().name)
            SceneManager.LoadScene(Instance.DestinationScene);
            
        JoinRoom(_targetRoom);
        _leftRoomAction -= JoinCallback;
    }

    public void JoinRoom(string RoomName)
    {
        if (!PhotonNetwork.IsConnected) return;
        PhotonNetwork.LocalPlayer.NickName = _displayName;
        var roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = 40;
        var typedLobby = new TypedLobby(RoomName, LobbyType.Default);
        PhotonNetwork.JoinOrCreateRoom(RoomName, roomOptions, typedLobby);
        Debug.Log("Connecting to room " + RoomName + "...");
    }

    public override void OnConnected()
    {
        base.OnConnected();
        Debug.Log("Connected to network");
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        base.OnDisconnected(cause);
        Debug.LogError("Disconnected.");
    }

    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();
        if (_firstLoad)
        {
            JoinRoom(RoomName);
            _firstLoad = false;
        }
        else
        {
            _leftRoomAction();
        }
    }

    public override void OnJoinedRoom()
    {
        LastRoom = CurrentRoom;
        CurrentRoom = PhotonNetwork.CurrentRoom.Name;

        // -7.8 1.49 -0.28

        Debug.Log(PhotonNetwork.IsMasterClient ? $"Joined {CurrentRoom} room and you are the master client" : $"Joined {CurrentRoom} room");

        Quaternion rot = PlayerInstance.Instance.transform.rotation;

        // Enable walking on the map
        if (CurrentRoom == "ControlRoomTeamMap")
        {
            _pos = new Vector3(-41f,0.6f, -110.33f);
            Player.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        }
        else
        {

            switch (LastRoom)
            {
                case "ControlRoomTeam1":
                    _pos = new Vector3(-6.77f, 1.5f, -15.75f);
                    break;
                case "ControlRoomTeam2":
                    _pos = new Vector3(-16.31f, 1.5f, -13.65f);
                    break;
                case "ControlRoomTeam3":
                    _pos = new Vector3(-25.97f, 1.5f, -15.75f);
                    break;
                case "ControlRoomTeam4":
                    _pos = new Vector3(-35.37f, 1.5f, -13.65f);
                    break;
                default:
                    if (SceneManager.GetActiveScene().name == "ControlRoom")
                    {
                        _pos = new Vector3(-7f, 1.5f, 0.04f);
                    }
                    else
                    {
                        _pos = new Vector3(-3.15f, 1.5f, 0.04f);
                    }
                    rot = Quaternion.Euler(new Vector3(0, 90, 0));
                    break;
            }
            Player.transform.localScale = new Vector3(1f, 1f, 1f);
            Player.transform.rotation = rot;
        }

        GameObject networkPlayer = PhotonNetwork.Instantiate("NetworkPlayer", _pos, PlayerInstance.Instance.transform.rotation);
        // Enable walking on the map
        if (CurrentRoom == "ControlRoomTeamMap")
            networkPlayer.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        else
            networkPlayer.transform.localScale = new Vector3(1f, 1f, 1f);

        PlayerNetworkStyle playerStyle = networkPlayer.GetComponent<PlayerNetworkStyle>();
        playerStyle.SetProperties(_displayName, _playerAvatar, _playerHair, _playerColors);

        StartCoroutine(LoadingScreenDelay());
    }

    IEnumerator LoadingScreenDelay()
    {
        yield return new WaitForSeconds(3);
        PlayerInstance.Instance.gameObject.GetComponent<GravityScript>().enabled = true;
        GameObject.FindGameObjectWithTag("LoadingCanvas").SetActive(false);
    }

    public void UserLoggedIn()
    {
        Debug.Log("User Logged In");
    }

    public override void OnRoomPropertiesUpdate(ExitGames.Client.Photon.Hashtable propertiesThatChanged)
    {
        base.OnRoomPropertiesUpdate(propertiesThatChanged);
        Debug.Log("Photon OnRoomPropertiesUpdate.");
        Debug.Log(PhotonNetwork.CurrentRoom.CustomProperties);

        // GAMEMASTER changes in different scenes
        var robotList = GameObject.Find("GAMEMASTER").GetComponent<MissionList>().RobotList;

        // Update robot active state
        foreach (string team in PhotonNetwork.CurrentRoom.CustomProperties.Keys)
        {
            if (TeamProps.ContainsKey(team))
            {
                TeamProps[team] = PhotonNetwork.CurrentRoom.CustomProperties[team];
            }
        }

        foreach (var team in TeamProps)
        {
            foreach (var mission in (Hashtable)team.Value)
            {
                string mId = (string)mission.Key;
                string activeTeam = (string)team.Key;
                // Make sure the robot still exists before changing state
                if (robotList.ContainsKey(activeTeam) &&
                    robotList[activeTeam].ContainsKey(mId) &&
                    robotList[activeTeam][mId])
                {
                    Debug.Log("Setting robot to active in " + mId);
                    bool isActive = (bool)mission.Value;
                    robotList[activeTeam][mId].SetActive(isActive);
                    if (isActive)
                        robotList[activeTeam][mId].gameObject.tag = "ActiveRobot";
                }
            }
        }
    }

    public void RecoverFromUnexpectedDisconnect(LoadBalancingClient loadBalancingClient, AppSettings appSettings)
    {
        this._loadBalancingClient = loadBalancingClient;
        this._appSettings = appSettings;
        this._loadBalancingClient.AddCallbackTarget(this);
    }

    ~NetworkManager()
    {
        this._loadBalancingClient.RemoveCallbackTarget(this);
    }

    void IConnectionCallbacks.OnDisconnected(DisconnectCause cause)
    {
        if (this.CanRecoverFromDisconnect(cause))
        {
            this.Recover();
        }
    }

    private bool CanRecoverFromDisconnect(DisconnectCause cause)
    {
        switch (cause)
        {
            // the list here may be non exhaustive and is subject to review
            case DisconnectCause.Exception:
            case DisconnectCause.ServerTimeout:
            case DisconnectCause.ClientTimeout:
            case DisconnectCause.DisconnectByServerLogic:
            case DisconnectCause.DisconnectByServerReasonUnknown:
                return true;
        }
        return false;
    }

    private void Recover()
    {
        if (!_loadBalancingClient.ReconnectAndRejoin())
        {
            Debug.LogError("ReconnectAndRejoin failed, trying Reconnect");
            if (!_loadBalancingClient.ReconnectToMaster())
            {
                Debug.LogError("Reconnect failed, trying ConnectUsingSettings");
                if (!_loadBalancingClient.ConnectUsingSettings(_appSettings))
                {
                    Debug.LogError("ConnectUsingSettings failed");
                }
            }
        }
    }
}
