﻿using System.Collections.Generic;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerLoginController : MonoBehaviour
{

    public string DisplayName = "Player";
    //                                          Avatar,          Hair, hair color, skin color, shirt color, pants color
    public string[] PlayerInfo = new string[] { "PlayerModel_1", "Afro", "#000000", "#000000", "#FFFFFF", "#FF00FF" };

    private GameData _data;
    private string _playFabId;
    public PlayFabLogin _playFabLogin;

    public void SavePlayInfo()
    {
        SetPlayerName();
        SetPlayerData();
    }

    private void Start()
    {
        _data = SaveSystem.LoadGameData();

        if (_data == null || _data.PlayerId == null)
        {
            _data = new GameData(System.Guid.NewGuid().ToString());
            SaveSystem.SaveGameData(_data);
        }

        var request = new LoginWithCustomIDRequest { CustomId = _data.PlayerId, CreateAccount = true };
        PlayFabClientAPI.LoginWithCustomID(request, OnLoginSuccess, OnLoginFailure);
    }

    private void OnLoginSuccess(LoginResult result)
    {
        // We logged in!
        Debug.Log("Successfully logged in with PlayFab");

        _playFabId = result.PlayFabId;

        _playFabLogin.JoinPlayFabGroups();

        var request = new GetAccountInfoRequest { PlayFabId = _playFabId };

        PlayFabClientAPI.GetAccountInfo(request, OnGetAccountInfoSuccess, OnGetAccountInfoFailure);
    }

    private void JoinTeam(PlayFabLogin.PlayfabGroup group)
    {
        PlayFabCloudScriptAPI.ExecuteEntityCloudScript(new PlayFab.CloudScriptModels.ExecuteEntityCloudScriptRequest
        {
            FunctionName = "addMember",
            FunctionParameter = new { GroupId = group.Id },
            GeneratePlayStreamEvent = true
        }, resulter =>
        {
            if ((bool)resulter.FunctionResult == true)
            {
                //Add into group successfully
                Debug.Log("Joined " + group.Name);
            }
            else
            {
                //Can't join this group, maybe blocked by your logic or an error occurred in AddMembers call.
                Debug.Log("Failed to join group " + group.Name);
            }
        }, error => { Debug.LogError(error.GenerateErrorReport()); });
    }

    private void OnLoginFailure(PlayFabError error)
    {
        Debug.LogError("Failed to login with PlayFab: " + error.GenerateErrorReport());
    }

    private void OnGetAccountInfoSuccess(GetAccountInfoResult result)
    {
        DisplayName = result.AccountInfo.TitleInfo.DisplayName;

        // Get other user info
        var request = new GetUserDataRequest() { PlayFabId = _playFabId };
        PlayFabClientAPI.GetUserData(request, OnGetUserDataSuccess, OnGetUserDataFailure);
    }

    private void OnGetAccountInfoFailure(PlayFabError error)
    {
        Debug.LogError("Failed to get Account Info: " + error.GenerateErrorReport());
    }

    private void OnGetUserDataSuccess(GetUserDataResult result)
    {
        UserDataRecord avatarRecord;
        UserDataRecord hairRecord;
        UserDataRecord hairColorRecord;
        UserDataRecord skinColorRecord;
        UserDataRecord shirtRecord;
        UserDataRecord pantsRecord;

        result.Data.TryGetValue("AvatarRecord", out avatarRecord);
        result.Data.TryGetValue("hairRecord", out hairRecord);
        result.Data.TryGetValue("hairColorRecord", out hairColorRecord);
        result.Data.TryGetValue("skinColorRecord", out skinColorRecord);
        result.Data.TryGetValue("ShirtColor", out shirtRecord);
        result.Data.TryGetValue("PantsColor", out pantsRecord);

        PlayerInfo[0] = avatarRecord == null ? "PlayerModel1" : avatarRecord.Value;
        PlayerInfo[1] = hairRecord == null ? "Afro" : hairRecord.Value;
        PlayerInfo[2] = hairColorRecord == null ? "#FFFFFF" : hairColorRecord.Value;
        PlayerInfo[3] = skinColorRecord == null ? "#FFFFFF" : skinColorRecord.Value;
        PlayerInfo[4] = shirtRecord == null ? "#FFFFFF" : shirtRecord.Value;
        PlayerInfo[5] = pantsRecord == null ? "#FFFFFF" : pantsRecord.Value;
    }

    private void OnGetUserDataFailure(PlayFabError error)
    {
        Debug.LogError("Failed to get Account Info: " + error.GenerateErrorReport());
    }

    // SET PLAYER NAME
    public void SetPlayerName()
    {
        var request = new UpdateUserTitleDisplayNameRequest() {DisplayName = DisplayName};

        PlayFabClientAPI.UpdateUserTitleDisplayName(request, OnUpdateUserTitleDisplayNameSuccess, OnUpdateUserTitleDisplayNameFailure);
    }

    private void OnUpdateUserTitleDisplayNameSuccess(UpdateUserTitleDisplayNameResult result)
    {
        Debug.Log("Display Name set to: " + result.DisplayName);
    }

    private void OnUpdateUserTitleDisplayNameFailure(PlayFabError error)
    {
        Debug.LogError("Failed to update Display Name: " + error.GenerateErrorReport());
    }

    // SET PLAYER DATA
    public void SetPlayerData()
    {
        var playerData = new Dictionary<string, string>();

        playerData.Add("AvatarModel", PlayerInfo[0]);
        playerData.Add("HairModel", PlayerInfo[1]);
        playerData.Add("HairColor", PlayerInfo[2]);
        playerData.Add("SkinColor", PlayerInfo[3]);
        playerData.Add("ShirtColor", PlayerInfo[4]);
        playerData.Add("PantsColor", PlayerInfo[5]);
        var request = new UpdateUserDataRequest() {Data = playerData};

        PlayFabClientAPI.UpdateUserData(request, OnUpdateUserDataSuccess, OnUpdateUserDataFailure);
    }

    private void OnUpdateUserDataSuccess(UpdateUserDataResult result)
    {
        Debug.Log("Player data updated");

        // Switch Scenes
        SceneManager.LoadScene("TrainingComplex", LoadSceneMode.Single);
    }

    private void OnUpdateUserDataFailure(PlayFabError error)
    {
        Debug.LogError("Failed to update Player Data: " + error.GenerateErrorReport());
    }
}
