﻿using Photon.Pun;
using UnityEngine;

public class NetworkCargoHandler : MonoBehaviour, IPunObservable
{
    public bool isCarryingCargo;

    public PhotonView view;

    void IPunObservable.OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(isCarryingCargo);
        }
        else if (stream.IsReading)
        {
            isCarryingCargo = (bool)stream.ReceiveNext();
        }
    }
}