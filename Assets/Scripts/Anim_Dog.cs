﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Anim_Dog : MonoBehaviour
{
    public Animator anim;

    public float duration = 10;

    private bool w = true;
    private int rand;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void Update()
    {
      if(w == true)
      {
        w = false;
        StartCoroutine("Timer");
      }
    }

    IEnumerator Timer()
    {
      yield return new WaitForSeconds(duration);
      rand = Random.Range(0, 4);
      anim.SetInteger("State", rand);
      w = true;
    }
}
