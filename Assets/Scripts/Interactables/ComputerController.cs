using System.Collections;
using Photon.Pun;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using VivoxUnity;

public class ComputerController : MonoBehaviour
{
    [Header("Interface Components:")]
    public GameObject ComputerUi;
    public GameObject TeleopUi;
    public GameObject ButtonBarCanvas;
    public GameObject ChooseAMission;
    public GameObject MissionDetailsPane;
    private Text _driveButtonText;
    public GameObject StartDrivingButton;
    public GameObject StopDrivingButton;

    [Header("Game Controller")] 
    public MissionList MissionListRef;

    [Header("Player Components:")]
    public GameObject NavigationUiCanvas;

    [Header("Programming Components:")]
    public GameObject ProgrammingUi;
    public GameObject ProgrammingView;

    private GameObject _activeRobot;
    private GameObject _activeMission;
    private bool _isDriving = false;


    private void Start()
    {
        ComputerUi.SetActive(false);
    }

    private void Update() {
        _activeMission = GameObject.FindGameObjectWithTag("ActiveMission");

        if (_activeMission)
        {
            // always look for a new active robot - hopefully won't slow down things
            _activeRobot = FindActiveRobot();

            // Disable the DriveRobot button if someone else is driving it
            if (_activeRobot && _activeRobot.GetComponent<RobotNetworkHandler>())
            {
                if (_activeRobot.GetComponent<RobotNetworkHandler>().isDriving &&
                    !_activeRobot.GetComponent<PhotonView>().IsMine)
                    StartDrivingButton.GetComponent<Button>().interactable = false;
                else
                    StartDrivingButton.GetComponent<Button>().interactable = true;
            }
        }
    }

    public void OpenMonitor()
    {
        NavigationUiCanvas.SetActive(false);
        ComputerUi.SetActive(true);
        ButtonBarCanvas.SetActive(true);
        PlayerInstance.Instance.GetComponent<KeyboardMovement>().enabled = false;
        ProgrammingUi.SetActive(false);
        ProgrammingView.SetActive(false);
        PlayerInstance.Instance.GetComponentInChildren<MouseMovement>().UnlockMouse();
        PlayerInstance.Instance.GetComponentInChildren<MouseMovement>().AtWorkstation = true;
        ChooseAMission.SetActive(true);
        MissionDetailsPane.SetActive(false);

        // stop driving robot if you hit the Missions button (which calls openMonitor()) to go join another mission 
        if (_isDriving &&_activeRobot)
        {
            StopDriving(); 
        }

        PlayerInstance.Instance.GetComponentInChildren<ClickController>().enabled = false;

        GameObject activeMission = GameObject.FindGameObjectWithTag("ActiveMission");
        if (activeMission != null)
        {
            activeMission.GetComponent<MissionController>().CloseMission();
        }

        MissionListRef.LoadMissions();
    }

    public void MissionInfo()
    {
        MissionDetailsPane.SetActive(true);
        GameObject activeMission = GameObject.FindGameObjectWithTag("ActiveMission");
        if (activeMission != null)
        {
            activeMission.GetComponent<MissionController>().ResetMission();
            activeMission.GetComponent<MissionController>().CloseMission();
            activeMission.GetComponent<MissionController>().LoadMission();
        }
    }

    public void ExitMonitor()
    {
        ComputerUi.SetActive(false);
        ButtonBarCanvas.SetActive(false);
        PlayerInstance.Instance.GetComponent<KeyboardMovement>().enabled = true;
        PlayerInstance.Instance.GetComponentInChildren<MouseMovement>().LockMouse();
        PlayerInstance.Instance.GetComponentInChildren<MouseMovement>().AtWorkstation = false;

        if(_isDriving && _activeRobot) {
            StopDriving();
        }

        foreach (GameObject icon in GameObject.FindGameObjectsWithTag("Hexagon"))
        {
            Destroy(icon);
        }
        MissionDetailsPane.GetComponent<MissionDetailPane>().missionIconController.Clear();

        TeleopUi.SetActive(false);
        ProgrammingUi.SetActive(false);
        ProgrammingView.SetActive(false);
        NavigationUiCanvas.SetActive(true);

        PlayerInstance.Instance.GetComponentInChildren<ClickController>().enabled = true;
        // we were losing the team in the control rooms
        if (SceneManager.GetActiveScene().name != "ControlRoom")
            PlayerInstance.Instance.ActiveTeam = "";
        NewVivoxManager.Instance.ChangeChannel(NewVivoxManager.Instance.CurrentChannel.Split('_')[0]);
    }

    public void OpenProgramming()
    {
        ProgrammingUi.SetActive(true);
        ProgrammingView.SetActive(true);
    }

    public void CloseProgramming()
    {
        ProgrammingUi.SetActive(false);
        ProgrammingView.SetActive(false);

        if (_activeRobot && _activeRobot.GetComponent<BlockRobotController>())
        {
            _activeRobot.GetComponent<BlockRobotController>().ClearDeliveredCargo();
        }
     
        var activeMission = GameObject.FindGameObjectWithTag("ActiveMission");
        if (activeMission != null)
        {
            activeMission.GetComponent<MissionController>().CloseMission();
        }
    }

    public GameObject FindActiveRobot()
    {
        if (!_activeMission) return null;

        // Find the robot that matches the current mission and team
        string missionID = _activeMission.GetComponent<MissionController>().missionID;
        GameObject[] robots = GameObject.FindGameObjectsWithTag("ActiveRobot");
        foreach (GameObject robot in robots)
        {
            NetworkRobot r = robot.GetComponent<NetworkRobot>();
            if (SceneManager.GetActiveScene().name == "ControlRoom")
            {

                if (r && r.MissionId == missionID)
                    return robot;
            }
            else
            {
                if (r && r.Team == PlayerInstance.Instance.ActiveTeam && r.MissionId == missionID)
                    return robot;
            }

        }
        return null;
    }
      
    

    public void DriveRobot()
    {
        _activeRobot = FindActiveRobot();
        if (_activeRobot)
        {
            _isDriving = true;
            Debug.Log("In DriveRobot: activeRobot mission is " + _activeRobot.GetComponent<NetworkRobot>().MissionId);
            NetworkRobot r = _activeRobot.GetComponent<NetworkRobot>();
            // check that the mission id and team matches
            if (_activeRobot && r && r.MissionId == _activeMission.GetComponent<MissionController>().missionID)
            {
                RobotNetworkHandler networkHandle = _activeRobot.GetComponent<RobotNetworkHandler>();

                if (networkHandle.isDriving)
                {
                    //StartCoroutine(DriveButtonWait());
                }
                else
                {
                    if (!_activeRobot.GetComponent<PhotonView>().IsMine)
                        _activeRobot.GetComponent<PhotonView>().TransferOwnership(PhotonNetwork.LocalPlayer);

                    networkHandle.isDriving = true;
                    if (_activeRobot.GetComponent<RobotRCController>()) // Aseo
                        _activeRobot.GetComponent<RobotRCController>().enabled = true;
                    if (_activeRobot.GetComponent<DroneRCController>()) // drone
                        _activeRobot.GetComponent<DroneRCController>().enabled = true;

                    StopDrivingButton.SetActive(true);
                    StartDrivingButton.SetActive(false);

                    GameObject activeMission = GameObject.FindGameObjectWithTag("ActiveMission");
                    activeMission.GetComponent<MissionController>().TransferDebrisOwnership();
                }
            }
        }

    }

    public void StopDriving()
    {
        _activeRobot = FindActiveRobot();
        _isDriving = false;
        
        if (_activeRobot &&  _activeRobot.GetComponent<NetworkRobot>())
        {
            _activeRobot.GetComponent<RobotNetworkHandler>().isDriving = false;
            if (_activeRobot.GetComponent<RobotRCController>()) // Aseo
                _activeRobot.GetComponent<RobotRCController>().enabled = false;
            if (_activeRobot.GetComponent<DroneRCController>()) // drone
                _activeRobot.GetComponent<DroneRCController>().enabled = false;
            StopDrivingButton.SetActive(false);
            StartDrivingButton.SetActive(true);
        }
    }

    // Not currently using this, text button replaced with icon
    private IEnumerator DriveButtonWait()
    {
        _driveButtonText.text = "In Use";
        yield return new WaitForSeconds(2);
        _driveButtonText.text = "Drive Robot";
    }

}
