﻿using UnityEngine;
using UnityEngine.SceneManagement;


public class SceneSwitcher : MonoBehaviour
{

    public string DestinationScene = "ControlRoom";
    public string DestinationRoom = "ControlRoom";
    public bool ToMap = false;

    public void GoToNewRoom()
    {
        PlayerInstance.Instance.gameObject.GetComponent<GravityScript>().enabled = false;
        PlayerInstance.Instance.PreviousRoom = NewVivoxManager.Instance.CurrentChannel;
        PlayerInstance.Instance.GoingToMap = ToMap;
        NetworkManager.Instance.GoToRoom = DestinationRoom;
        int i = DestinationRoom.IndexOf("Team");
        if (i > -1)
        {  // switching to ControlRoomTeam# 
            string team = DestinationRoom.Substring(i).ToLower();
            PlayerInstance.Instance.ActiveTeam = team;
            Debug.Log("Scene switch: Joined team " + team);
        }

        NetworkManager.Instance.DestinationScene = DestinationScene;
        NetworkManager.Instance.AttemptJoinRoom(DestinationRoom);

        NewVivoxManager.Instance.ChangeChannel(DestinationRoom);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            GoToNewRoom();
        }
    }
}
