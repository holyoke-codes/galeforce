﻿using UnityEngine;

public class ClickController : MonoBehaviour
{

    public float ClickDistance = 10f;
    private ComputerController _computerController;

    private void Start()
    {
        GameObject gamemaster = GameObject.FindGameObjectWithTag("GameMaster");
        _computerController = gamemaster.GetComponent<ComputerController>();
    }

    // Update is called once per frame
    private void Update()
    {
        Vector3 pos = Vector3.zero;

        if (Input.GetMouseButtonDown(0))
        {
            pos = Input.mousePosition;
        }
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
                pos = touch.position;
        }

        if (pos != Vector3.zero)
        {
            var ray = GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out var hit, ClickDistance))
            {
                if (hit.transform.parent)
                {
                    Transform parent = hit.transform.parent;
                    // in training complex
                    if (parent.name.StartsWith("Station"))
                    {
                        var teamName = parent.parent.GetComponent<ControlTable>().TeamName;
                        PlayerInstance.Instance.ActiveTeam = teamName;
                        NewVivoxManager.Instance.ChangeChannel(NewVivoxManager.Instance.CurrentChannel + "_" + teamName);
                        GameObject gamemaster = GameObject.FindGameObjectWithTag("GameMaster");
                        _computerController = gamemaster.GetComponent<ComputerController>();
                        _computerController.OpenMonitor();
                    }
                    //in control room - already have a team set from SceneSwitcher
                    // but need to get new gamemaster object and its computer controller
                    if (parent.name.StartsWith("ControlRoomStation"))
                    {
                        GameObject gamemaster = GameObject.FindGameObjectWithTag("GameMaster");
                        _computerController = gamemaster.GetComponent<ComputerController>();
                        _computerController.OpenMonitor();
                    }
                    if (parent.name.StartsWith("WebView"))
                    {
                        GetComponent<MouseMovement>().UnlockMouse();
                    }
                }
            }
        }
        
    }
}