﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MobileCellTowerScript : MonoBehaviour
{
    private bool _cleared;
    // How long the drone needs to stay in the mobile cell tower location
    private float timerCountDown = 2.0f;
    private bool isDroneInLocation = false;
    private GameObject _activeMission;
    private string _missionId;

    public void Start()
    {
        _activeMission = GameObject.FindGameObjectWithTag("ActiveMission");
        if (_activeMission)
          _missionId = _activeMission.GetComponent<MissionController>().missionID;
    }

    public bool IsCleared()
    {
        return _cleared;
    }

    public void Clear()
    {
        _cleared = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (timerCountDown < 0)
        {
            timerCountDown = 0;
            _cleared = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "ActiveRobot" &&
            other.gameObject.GetComponent<NetworkRobot>().MissionId == _missionId)
        {
            if (SceneManager.GetActiveScene().name == "TrainingComplex")
            {
                // Check if teams match in the Training Complex
                if (other.gameObject.GetComponent<NetworkRobot>().Team == PlayerInstance.Instance.ActiveTeam)
                {
                    Debug.Log("touching drone");
                    isDroneInLocation = true;
                }
            }
            else
            {
                Debug.Log("touching drone");
                isDroneInLocation = true;
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (isDroneInLocation)
        {
            // Countdown the timer when the drone is in the hover zone
            timerCountDown -= Time.deltaTime;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "ActiveRobot")
        {
            Debug.Log("Exiting hover zone");
            // Reset the timer
            isDroneInLocation = false;
            _cleared = false;
            timerCountDown = 2.0f;
        }
    }
}
