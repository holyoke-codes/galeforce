﻿using UnityEngine;

public class Debris : MonoBehaviour
{
    private bool _cleared;
    // How long the debris needs to stay off the road
    private float _timer = 5.0f;
    private bool _touchingRoad = true;
    private Material _mat;

    public bool IsCleared()
    {
        return _cleared;
    }

    public void Clear()
    {
        _cleared = true;
    }

    private void Start()
    {
        // Some debris has a child model
        Renderer renderer = GetComponent<Renderer>() ? GetComponent<Renderer>() : gameObject.GetComponentInChildren<Renderer>();
        _mat = renderer.material;
    }

    private void Update()
    {
        if (!_touchingRoad)
            _timer -= Time.deltaTime;

        if (_timer < 0)
            _cleared = true;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Road")
        {
            _touchingRoad = true;
            if (_mat)
                _mat.EnableKeyword("_EMISSION");
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Road")
        {
            _touchingRoad = false;
            // Reset the timer
            _timer = 5.0f;
            if (_mat)
                _mat.DisableKeyword("_EMISSION");
        }
    }
}