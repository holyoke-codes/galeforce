﻿using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

public class DoorController : MonoBehaviourPunCallbacks
{
    private Animator _anim;

    public int DoorID = 0;

    public bool DoorOpen = false;

    private void OnTriggerEnter(Collider obj)
    {
        if (obj.GetComponent<Collider>().tag == "Player")
        {
            DoorOpen = true;
        }
    }

    private void OnTriggerStay(Collider obj)
    {
        if (obj.GetComponent<Collider>().tag == "Player")
        {
            DoorOpen = true;
        }
    }

    private void OnTriggerExit(Collider obj)
    {
        if (obj.GetComponent<Collider>().tag == "Player")
        {
            DoorOpen = false;
        }
    }

    // Start is called before the first frame update
    private void Start()
    {
        _anim = transform.parent.GetComponent<Animator>();
        InvokeRepeating("AttemptCloseDoor", 5f, 5f);
    }

    private void AttemptCloseDoor()
    {
        DoorOpen = false;
    }

    private void Update()
    {

        if (_anim.GetBool("IsOpening") != DoorOpen)
        {
            Hashtable doorHash = new Hashtable() { { "DoorID", DoorID }, { "DoorOpen", DoorOpen } };
            if (PhotonNetwork.CurrentRoom != null)
                PhotonNetwork.CurrentRoom.SetCustomProperties(doorHash);
        }

        _anim.SetBool("IsOpening", DoorOpen);

    }

    public override void OnRoomPropertiesUpdate(Hashtable props)
    {
        if (!props.ContainsKey("DoorID")) return;
        if ((int)props["DoorID"] == DoorID)
        {
            DoorOpen = (bool)props["DoorOpen"];
        }
    }
}