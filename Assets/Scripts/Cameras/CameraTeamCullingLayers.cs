﻿using UnityEngine;

public class CameraTeamCullingLayers : MonoBehaviour
{
    public bool ShowRightCameraLayers;
    public bool ShowLeftCameraLayers;

    private int _mapLayer;
    private int _water;
    private int _leftCam;
    private int _rightCam;
    private Camera _cam;

    // Start is called before the first frame update
    void Start()
    {
        _cam = GetComponent<Camera>();
        _mapLayer = LayerMask.NameToLayer("Map");
        _water = LayerMask.NameToLayer("Water");
        _leftCam = LayerMask.NameToLayer("Left Camera");
        _rightCam = LayerMask.NameToLayer("Right Camera");
    }

    // Update is called once per frame
    void Update()
    {
        // Always show the map layer and the water.
        _cam.cullingMask = (1 << _mapLayer) | (1 << _water);

        // Add the team layer as appropriate
        if (PlayerInstance.Instance.ActiveTeam != "")
        {
            int teamLayer = LayerMask.NameToLayer(PlayerInstance.Instance.ActiveTeam);
            _cam.cullingMask |= 1 << teamLayer;
        }

        // Add in left/right camera layers
        if (ShowLeftCameraLayers)
            _cam.cullingMask |= 1 << _leftCam;
        if (ShowRightCameraLayers)
            _cam.cullingMask |= 1 << _rightCam;
    }
}
