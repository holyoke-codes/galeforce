﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptimizedMap : MonoBehaviour
{

    private Camera _camera;

    public int FPS = 10;

    // Start is called before the first frame update
    void Start()
    {
        _camera = GetComponent<Camera>();

        InvokeRepeating("RenderCamera", 0, 1.0f/FPS);

    }



    public void RenderCamera()
    {
        _camera.Render();
    }

}
