﻿using UnityEngine;

public class ResizeWindow : MonoBehaviour
{
    private int _lastHeight;
    private int _lastWidth;

    private void Update()
    {
        var width = Screen.width;
        var height = Screen.height;

        if (_lastWidth != width)
        {
            var heightAccordingToWidth = width / 16.0 * 9.0;
            Screen.SetResolution(width, (int) Mathf.Round((float) heightAccordingToWidth), false, 0);
        }
        else if (_lastHeight != height)
        {
            var widthAccordingToHeight = height / 9.0 * 16.0;
            Screen.SetResolution((int) Mathf.Round((float) widthAccordingToHeight), height, false, 0);
        }

        _lastWidth = width;
        _lastHeight = height;
    }
}