﻿using System.Collections;
using UnityEngine;
using Photon.Pun;

public class Anim_Controller : MonoBehaviour
{
    public Animator Anim;
    private PhotonView _pv;
    private MouseMovement _mouseMovement;
    private float inputV;
    private float inputH;
    public bool _isRunning;
    public int _isKneeling = 0;

    void Start()
    {
        // Get's animator component.
        Anim = GetComponent<Animator>();
        _pv = GetComponent<PhotonView>();
        _mouseMovement = GameObject.FindObjectOfType<MouseMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!_pv)
            _pv = GetComponent<PhotonView>();

        if (!_pv.IsMine) return;

        //Animation Code!

        if (_mouseMovement.AtWorkstation) return;

        //Get user input.
        if (Input.GetKeyDown("1"))
        {
            Anim.SetTrigger("Silly Dance");
        }

        if (Input.GetKeyDown("2"))
        {
            Anim.SetTrigger("Twist Dance");
        }

        if (Input.GetKeyDown("3"))
        {
            Anim.SetTrigger("Step Dance");
        }

        if (Input.GetKeyDown("4"))
        {
            Anim.SetTrigger("Waving");
        }

        if (Input.GetKeyDown("5"))
        {
            Anim.SetTrigger("Cheering");
        }

        if (Input.GetKeyDown("6"))
        {
            Anim.SetTrigger("Fist Pump");
        }

        if (Input.GetKeyDown("7"))        {            Anim.SetTrigger("Backflip");        }        if (Input.GetKeyDown(KeyCode.Space))        {            Anim.SetTrigger("Jump");        }

        // Use LeftShift to toggle running
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            if (_isRunning)
            {
                _isRunning = false;
                Anim.ResetTrigger("IsRunning");
            }
            else
            {
                _isRunning = true;
                Anim.SetTrigger("IsRunning");
            }
        }

        inputH = Input.GetAxis("Horizontal");
        inputV = Input.GetAxis("Vertical");

        // You can kneel when you aren't moving
        Vector3 move = transform.right * inputH + transform.forward * inputV;
        if (move == Vector3.zero)
        {
            if (Input.GetKey(KeyCode.C))
            {
                if (_isKneeling < 5)
                {
                    Anim.ResetTrigger("Stand");
                    Anim.SetTrigger("Kneel");
                    _isKneeling += 1;
                }
            }
            else
            {
                if (_isKneeling >= 1 || isPlaying("Kneeling"))
                {
                    Anim.ResetTrigger("Kneel");
                    Anim.SetTrigger("Stand");
                    _isKneeling = 0;
                }
            }
        }
        else
        {
            if (isPlaying("Kneeling"))
            {
                Anim.ResetTrigger("Kneel");
                Anim.SetTrigger("Stand");
                _isKneeling = 0;
            }
            Anim.SetFloat("InputH", inputH);
            Anim.SetFloat("InputV", inputV);
        }
    }

    bool isPlaying(string stateName)
    {
        int animLayer = 0;
        if (Anim.GetCurrentAnimatorStateInfo(animLayer).IsName(stateName) &&
                Anim.GetCurrentAnimatorStateInfo(animLayer).normalizedTime < 1.0f)
            return true;
        else
            return false;
    }
}
