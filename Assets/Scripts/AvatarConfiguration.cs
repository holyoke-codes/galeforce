﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class AvatarConfiguration : MonoBehaviour
{
    public Transform PlayerPosition;
    public GameObject Avatar;
    public GameObject Hair;
    public Color HairColor;
    public Color SkinColor;
    public Color ShirtColor;
    public Color PantsColor;

    private List<Object> _avatars = new List<Object>();
    private List<Object> _hairModels = new List<Object>();
    public int I = 0;
    public int HairIndex = 0;
    private Rigidbody _rb;

    // Start is called before the first frame update
    void Start()
    {
        ColorUtility.TryParseHtmlString("#AA7248", out SkinColor);
        ColorUtility.TryParseHtmlString("#4d90bc", out PantsColor);
        _avatars = Resources.LoadAll("Players").ToList();
        foreach (Transform child in PlayerPosition)
        {
            if (child.name.Contains("Clone"))
            {
                Avatar = child.gameObject;
            }
        }
        HandleUpdate();
    }

    private void FixedUpdate()
    {
        if (!Avatar)
        {
            _avatars = Resources.LoadAll("Players").ToList();
            HandleUpdate();
        }

        Quaternion deltaRotation = Quaternion.Euler(new Vector3(0, 20, 0) * Time.fixedDeltaTime);
        _rb.MoveRotation(_rb.rotation * deltaRotation);
    }

    public void HandleUpdate()
    {
        Destroy(Avatar);
        Avatar = Instantiate(_avatars[I], PlayerPosition) as GameObject;
        Debug.Log(Avatar);
        _rb = Avatar.GetComponent<Rigidbody>();
        HandleHairUpdate();
        HandleColorChange("skinColor", SkinColor);
        HandleColorChange("shirtColor", ShirtColor);
        HandleColorChange("pantsColor", PantsColor);
    }

    private void HandleHairUpdate()
    {
        _hairModels.Clear();
        // Make a list of all the submodels that are tagged "hair"
        foreach (Transform child in Avatar.transform)
        {
            if (child.tag == "hair")
            {
                _hairModels.Add(child.gameObject);
            }
        }
        if (_hairModels.Count() > 0)
        {
            // Set the hair as inactive
            foreach (GameObject hair in _hairModels)
            {
                hair.SetActive(false);
            }
            // Set the hair color and activate the new hair model
            if (HairIndex > _hairModels.Count() - 1)
            {
                HairIndex = 0;
            }
            Hair = _hairModels[HairIndex] as GameObject;
            foreach (Material material in Hair.GetComponent<Renderer>().materials)
            {
                if (material.name.Replace(" (Instance)", "") == "Hair")
                {
                    material.color = HairColor;
                }
                // Mohawk has skin material
                if (material.name.Replace(" (Instance)", "") == "Skin")
                {
                    material.color = SkinColor;
                }
            }
            Hair.SetActive(true);
        }
    }

    public void HandleNextAvatar()
    {
        I = (I + 1) % _avatars.Count;
        HandleUpdate();
    }

    public void HandlePrevAvatar()
    {
        I = (I == 0) ? _avatars.Count - 1 : I - 1;
        HandleUpdate();
    }


    public void HandleNextHair()
    {
        // Show the next hair, or wrap to the beginning
        HairIndex = (HairIndex + 1) % _hairModels.Count();
        HandleHairUpdate();
    }

    public void HandlePrevHair()
    {
        // Show the previous hair, or wrap to the end
        HairIndex = (HairIndex == 0) ? _hairModels.Count - 1 : HairIndex - 1;
        HandleHairUpdate();
    }

    public void HandleColorChange(string objName, Color color)
    {
        switch (objName)
        {
            case "hairColor":
                HairColor = color;
                // Original models have hair as a separate gameobject
                // New models have a Hair material on the body
                if (Hair)
                {
                    Hair.GetComponent<Renderer>().material.color = color;
                    return;
                }
                break;
            // Other parts are materials on the main Body model
            // Translate to the names of those materials.
            case "skinColor":
                objName = "Skin";
                SkinColor = color;
                break;
            case "pantsColor":
                objName = "Pants";
                PantsColor = color;
                break;
            case "shirtColor":
                objName = "OverShirt";
                ShirtColor = color;
                break;
            default:
                objName = "UnderShirt";
                ShirtColor = color;
                break;
        }

        // Get the renderer for the Body, find the material with the specified Name, set the color of that material
        // update that material in the list, and change the materials on the Body.
        Renderer renderer = Avatar.transform.Find("Body").GetComponent<Renderer>();
        List<Material> materials = renderer.materials.ToList();
        // The material name has some extra text in it
        Material objMat = materials.Find(x => x.name.Replace(" (Instance)", "") == (objName) );
        if (objMat)
        {
            int i = materials.IndexOf(objMat);
            objMat.color = color;
            materials[i] = objMat;
            renderer.materials = materials.ToArray<Material>();
        }
        else
            Debug.Log("Not Found objMat " + objName);
    }

    void OnDestroy()
    {
        //Destroy(_hairMat);
    }
}
