﻿using UnityEngine;

public class ToggleDestruction : MonoBehaviour
{
    public bool Destroyed = false;
    public GameObject DestroyedBuilding;
    public GameObject NormalBuilding;

    // Update is called once per frame
    private void Update()
    {
        NormalBuilding.SetActive(!Destroyed);
        DestroyedBuilding.SetActive(Destroyed);
    }
}