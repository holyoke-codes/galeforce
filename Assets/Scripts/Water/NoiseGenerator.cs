﻿using UnityEngine;

public class NoiseGenerator : MonoBehaviour
{
    private MeshFilter _mf;

    public float power = 1.4f;
    public float scale = 1;
    public float timeScale = 1;

    private float _xOffset;
    private float _yOffset;

    // Start is called before the first frame update
    private void Start()
    {
        _mf = GetComponent<MeshFilter>();
        MakeNoise();
    }

    // Update is called once per frame
    private void Update()
    {
        MakeNoise();
        _xOffset += Time.deltaTime * timeScale;
        _yOffset += Time.deltaTime * timeScale;
    }

    private void MakeNoise()
    {
        var vertices = _mf.mesh.vertices;

        for (var i = 0; i < vertices.Length; i++)
            vertices[i].y = CalculateHeight(vertices[i].x, vertices[i].z) * power;

        _mf.mesh.vertices = vertices;
    }

    private float CalculateHeight(float x, float y)
    {
        var xCord = x * scale * _xOffset;
        var yCord = y * scale * _yOffset;


        return Mathf.PerlinNoise(xCord, yCord);
    }
}