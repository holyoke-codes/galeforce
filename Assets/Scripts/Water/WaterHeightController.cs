﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WaterHeightController : MonoBehaviour
{

    public float DefaultHeight = 26f;
    public WaterLevel[] WaterLevels = new WaterLevel[0];

    private Transform _transform;

    void Start()
    {
        TeacherDB.Instance.DbUpdated += NewDbUpdate;
        _transform = GetComponent<Transform>();
    }

    public void NewDbUpdate(object sender, DbUpdatedEventArgs args)
    {
        // TODO: Get stages for correct team
        var matchingTeamList = args.db.teams.Where(t => t.team_name == PlayerInstance.Instance.ActiveTeam).ToArray();

        var stage = "";
        
        if (matchingTeamList.Length > 0)
        {
            var currentTeamInfo = matchingTeamList[0];
            stage = SceneManager.GetActiveScene().name == "TrainingComplex" ? currentTeamInfo.training_stage : currentTeamInfo.control_stage;
        }
        
        foreach (var level in WaterLevels)
        {
            if (stage == level.StageName)
            {
                Vector3 curPos = transform.localPosition;
                curPos.y = level.WaterHeight;
                _transform.localPosition = curPos;
                break;
            }
            else
            {
                Vector3 curPos = transform.localPosition;
                curPos.y = DefaultHeight;
                _transform.localPosition = curPos;
            }
        }
    }
}

[Serializable]
public class WaterLevel
{
    public string StageName = "";
    public float WaterHeight = 0f;
}
