﻿using System;
using System.Web;
using UnityEngine;
using Vuplex.WebView;
using System.Collections.Generic;
using UnityEngine.SceneManagement;


public class JSTranslator : MonoBehaviour
{
    private CanvasWebViewPrefab _webViewPrefab;

    public BlockRobotController ActiveRobot;

    public GameObject ProgrammingCanvas;

    public bool RemoteDebugging = false;

    public MissionList MissionList;

    private string _activeRobotName;
    private string _missionId;
    public string PlayerName;
    public string TeamName = "TeamZero";

    // Start is called before the first frame update
    private void Awake()
    {
        if (RemoteDebugging)
        {
            try
            {
                // Open http://localhost:8080 in chrome to view console
                StandaloneWebView.EnableRemoteDebugging(8080);
            }
            catch (Exception e)
            {
                Debug.Log(e);
            }
        }
        Debug.Log("starting JS Translator");
        _webViewPrefab = GetComponent<CanvasWebViewPrefab>();
        _webViewPrefab.Initialized += (sender, e) =>
        {
            _webViewPrefab.WebView.MessageEmitted += WebViewMessageEmitted;
        };

        // Disable the programming window after init
        ProgrammingCanvas.SetActive(false);
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("scene loaded");
    }

    // Update Firebase with mission success, player name, team name
    public void HandleFirebaseUpdate(string action, string value)
    {
        if (_webViewPrefab)
        {
            BlockEvent _msg = new BlockEvent();
            _msg.action = action;
            _msg.value = value;
            //Debug.Log("Posting " + action + " : " + value);
            _webViewPrefab.WebView.PostMessage(JsonUtility.ToJson(_msg));
        }
    }

    // Grid events are their own methods. These are called by button clicks in the UI.
    // Can you add parameters to click events on a button?
    public void TurnGridOn()
    {
        HandleFirebaseUpdate("grid", "on");
    }

    public void TurnGridOff()
    {
        HandleFirebaseUpdate("grid", "off");
    }

    public void RightOverHeadCamOn()
    {
        HandleFirebaseUpdate("camera", "right overhead camera");
    }


    public void RightRearOverHeadCamOn()
    {
        HandleFirebaseUpdate("camera", "right rear overhead camera");
    }


    public void RightCamOn()
    {
        HandleFirebaseUpdate("camera", "right camera");
    }

    public void MapOn()
    {
        HandleFirebaseUpdate("camera", "map view");
    }

    private void Update()
    {
        if (!ActiveRobot)
        {
            MissionController activeMission = GameObject.FindGameObjectWithTag("ActiveMission").GetComponent<MissionController>();
            // Get the active robot if the mission hasn't been completed yet
            if (MissionList.RobotList[PlayerInstance.Instance.ActiveTeam][activeMission.missionID])
            {
                ActiveRobot = MissionList.RobotList[PlayerInstance.Instance.ActiveTeam][activeMission.missionID].GetComponent<BlockRobotController>();

                // New robot, tell the coding environment
                _activeRobotName = ActiveRobot.name;
                HandleFirebaseUpdate("robot", _activeRobotName);

                // New  mission, tell the coding environment
                _missionId = ActiveRobot.MissionId;
                HandleFirebaseUpdate("mission", _missionId);

                // Also send mission description (BH)
                HandleFirebaseUpdate("missiondescription", activeMission.missionDescription);
            }

        }
        if (ActiveRobot)
        {
            // Update sensor values to JavaScript
            List<string> hits = ActiveRobot.SensorSees();
            if (_webViewPrefab.isActiveAndEnabled)
                _webViewPrefab.WebView.PostMessage(Newtonsoft.Json.JsonConvert.SerializeObject(hits));
        }
    }

    private void WebViewMessageEmitted(object sender, EventArgs<string> eventArgs)
    {
        // Plain JSON String
        string jsonString = eventArgs.Value;

        // Cast string to C# Object
        BlockEvent jsonBlockEvent = JsonUtility.FromJson<BlockEvent>(jsonString);

        if (jsonBlockEvent.action == "info")
        {
            HandleFirebaseUpdate("session", PlayerPrefs.GetString("sessionCode"));
            HandleFirebaseUpdate("mission", _missionId);
            HandleFirebaseUpdate("player", PlayerName);
            HandleFirebaseUpdate("team", TeamName);
            return;
        }

        if (!ActiveRobot)
        {
            Debug.Log("No Robot Found");
            return;
        }

        switch (jsonBlockEvent.action)
        {
            case "reset":
                // Go back to start position
                ActiveRobot.Reset().Then(done =>
                {
                    GameObject.FindGameObjectWithTag("ActiveMission").GetComponent<MissionController>().ResetMission();
                    _webViewPrefab.WebView.PostMessage(JsonUtility.ToJson(jsonBlockEvent));
                });
                break;

            case "drive forward":
                var distance = float.Parse(jsonBlockEvent.value);

                // Drive Robot Forward Distance
                ActiveRobot.Drive(distance, 1)
                   .Then(done =>
                   {
                       _webViewPrefab.WebView.PostMessage(JsonUtility.ToJson(jsonBlockEvent));
                   })
                   .Catch(exception =>
                   {
                       BlockEvent err = new BlockEvent();
                       err.action = "exception";
                       _webViewPrefab.WebView.PostMessage(JsonUtility.ToJson(err));
                   });

                break;

            case "drive reverse":
                var distanceReverse = float.Parse(jsonBlockEvent.value);

                // Drive Robot backwards Distance
                ActiveRobot.Drive(distanceReverse, -1)
                    .Then(done =>
                    {
                        _webViewPrefab.WebView.PostMessage(JsonUtility.ToJson(jsonBlockEvent));
                    })
                   .Catch(exception =>
                   {
                       BlockEvent err = new BlockEvent();
                       err.action = "exception";
                       _webViewPrefab.WebView.PostMessage(JsonUtility.ToJson(err));
                   });

                break;

            case "stop":
                // Stop driving
                ActiveRobot.StopDriving()
                    .Then(done =>
                    {
                        _webViewPrefab.WebView.PostMessage(JsonUtility.ToJson(jsonBlockEvent));
                    })
                   .Catch(exception =>
                   {
                       BlockEvent err = new BlockEvent();
                       err.action = "exception";
                       _webViewPrefab.WebView.PostMessage(JsonUtility.ToJson(err));
                   });
                break;

            case "turn":
                var degrees = float.Parse(jsonBlockEvent.value);

                // Turn Robot Degrees
                ActiveRobot.Turn(degrees)
                    .Then(done =>
                    {
                        _webViewPrefab.WebView.PostMessage(JsonUtility.ToJson(jsonBlockEvent));
                    })
                   .Catch(exception =>
                   {
                       BlockEvent err = new BlockEvent();
                       err.action = "exception";
                       _webViewPrefab.WebView.PostMessage(JsonUtility.ToJson(err));
                   });
                break;

            case "follow curb for":
                // Follow the curb for X meters
                distance = float.Parse(jsonBlockEvent.value);
                ActiveRobot.FollowCurbFor(distance)
                    .Then(done =>
                    {
                        _webViewPrefab.WebView.PostMessage(JsonUtility.ToJson(jsonBlockEvent));
                    })
                   .Catch(exception =>
                   {
                       BlockEvent err = new BlockEvent();
                       err.action = "exception";
                       _webViewPrefab.WebView.PostMessage(JsonUtility.ToJson(err));
                   });
                break;

            case "follow curb":
                // Follow the curb
                ActiveRobot.FollowCurb()
                    .Then(done =>
                    {
                        _webViewPrefab.WebView.PostMessage(JsonUtility.ToJson(jsonBlockEvent));
                    })
                   .Catch(exception =>
                   {
                       BlockEvent err = new BlockEvent();
                       err.action = "exception";
                       _webViewPrefab.WebView.PostMessage(JsonUtility.ToJson(err));
                   });
                break;

            // Drone code 
            case "fly up":
                distance = float.Parse(jsonBlockEvent.value);

                // Fly drone up Distance
                ActiveRobot.Altitude(distance, 1)
                    .Then(done =>
                    {
                        _webViewPrefab.WebView.PostMessage(JsonUtility.ToJson(jsonBlockEvent));
                    })
                   .Catch(exception =>
                   {
                       BlockEvent err = new BlockEvent();
                       err.action = "exception";
                       _webViewPrefab.WebView.PostMessage(JsonUtility.ToJson(err));
                   });

                break;

            case "fly down":
                distance = float.Parse(jsonBlockEvent.value);

                // Fly drone up Distance
                ActiveRobot.Altitude(distance, -1)
                    .Then(done =>
                    {
                        _webViewPrefab.WebView.PostMessage(JsonUtility.ToJson(jsonBlockEvent));
                    })
                   .Catch(exception =>
                   {
                       BlockEvent err = new BlockEvent();
                       err.action = "exception";
                       _webViewPrefab.WebView.PostMessage(JsonUtility.ToJson(err));
                   });

                break;

            case "fly right":
                distance = float.Parse(jsonBlockEvent.value);

                // Fly drone right Distance
                ActiveRobot.Strafe(distance, 1)
                    .Then(done =>
                    {
                        _webViewPrefab.WebView.PostMessage(JsonUtility.ToJson(jsonBlockEvent));
                    })
                   .Catch(exception =>
                   {
                       BlockEvent err = new BlockEvent();
                       err.action = "exception";
                       _webViewPrefab.WebView.PostMessage(JsonUtility.ToJson(err));
                   });

                break;

            case "fly left":
                distance = float.Parse(jsonBlockEvent.value);

                // Fly drone left Distance
                ActiveRobot.Strafe(distance, -1)
                    .Then(done =>
                    {
                        _webViewPrefab.WebView.PostMessage(JsonUtility.ToJson(jsonBlockEvent));
                    })
                   .Catch(exception =>
                   {
                       BlockEvent err = new BlockEvent();
                       err.action = "exception";
                       _webViewPrefab.WebView.PostMessage(JsonUtility.ToJson(err));
                   });

                break;

            case "fly forward":
                distance = float.Parse(jsonBlockEvent.value);

                // Fly drone forward Distance
                ActiveRobot.Drive(distance, 1)
                    .Then(done =>
                    {
                        _webViewPrefab.WebView.PostMessage(JsonUtility.ToJson(jsonBlockEvent));
                    })
                   .Catch(exception =>
                   {
                       BlockEvent err = new BlockEvent();
                       err.action = "exception";
                       _webViewPrefab.WebView.PostMessage(JsonUtility.ToJson(err));
                   });

                break;

            case "fly reverse":
                distance = float.Parse(jsonBlockEvent.value);

                // Fly drone backward Distance
                ActiveRobot.Drive(distance, -1)
                    .Then(done =>
                    {
                        _webViewPrefab.WebView.PostMessage(JsonUtility.ToJson(jsonBlockEvent));
                    })
                   .Catch(exception =>
                   {
                       BlockEvent err = new BlockEvent();
                       err.action = "exception";
                       _webViewPrefab.WebView.PostMessage(JsonUtility.ToJson(err));
                   });

                break;

            case "fly to":
                Coords coords = Newtonsoft.Json.JsonConvert.DeserializeObject<Coords>(jsonBlockEvent.value);


                // Fly drone to location specified by x,y,z coordinates
                ActiveRobot.FlyTo(coords)
                    .Then(done =>
                    {
                        _webViewPrefab.WebView.PostMessage(JsonUtility.ToJson(jsonBlockEvent));
                    })
                   .Catch(exception =>
                   {
                       BlockEvent err = new BlockEvent();
                       err.action = "exception";
                       _webViewPrefab.WebView.PostMessage(JsonUtility.ToJson(err));
                   });

                break;

            case "pick up":
                ActiveRobot.Pickup()
                    .Then(done =>
                    {
                        _webViewPrefab.WebView.PostMessage(JsonUtility.ToJson(jsonBlockEvent));
                    })
                   .Catch(exception =>
                   {
                       BlockEvent err = new BlockEvent();
                       err.action = "exception";
                       _webViewPrefab.WebView.PostMessage(JsonUtility.ToJson(err));
                   });
                break;

            case "deliver":
                ActiveRobot.DropOff()
                    .Then(done =>
                    {
                        _webViewPrefab.WebView.PostMessage(JsonUtility.ToJson(jsonBlockEvent));
                    })
                   .Catch(exception =>
                   {
                       BlockEvent err = new BlockEvent();
                       err.action = "exception";
                       _webViewPrefab.WebView.PostMessage(JsonUtility.ToJson(err));
                   });
                break;

            case "land":
                ActiveRobot.Land()
                    .Then(done =>
                    {
                        _webViewPrefab.WebView.PostMessage(JsonUtility.ToJson(jsonBlockEvent));
                    })
                   .Catch(exception =>
                   {
                       BlockEvent err = new BlockEvent();
                       err.action = "exception";
                       _webViewPrefab.WebView.PostMessage(JsonUtility.ToJson(err));
                   });

                break;
        }
    }
}

// Represents JSON structure of Block Event
[Serializable]
public class BlockEvent
{
    public string action;
    public string value;
}

public struct Coords
{
    public float x;
    public float y;
    public float z;
}