﻿using UnityEngine;
using Vuplex.WebView;
using Vuplex.WebView.Demos;

public class KeyboardForwarder : MonoBehaviour
{
    private CanvasWebViewPrefab _canvasWebViewPrefab;
    private HardwareKeyboardListener _hardwareKeyboardListener;

    // Start is called before the first frame update
    private void Start()
    {
        _canvasWebViewPrefab = GetComponent<CanvasWebViewPrefab>();
        SetUpHardwareKeyboard();
    }

    private void SetUpHardwareKeyboard()
    {
        _hardwareKeyboardListener = HardwareKeyboardListener.Instantiate();
        _hardwareKeyboardListener.KeyDownReceived += (sender, eventArgs) =>
        {
            var webViewWithKeyDown = _canvasWebViewPrefab.WebView as IWithKeyDownAndUp;
            if (webViewWithKeyDown == null)
                _canvasWebViewPrefab.WebView.HandleKeyboardInput(eventArgs.Value);
            else
                webViewWithKeyDown.KeyDown(eventArgs.Value, eventArgs.Modifiers);
        };
        _hardwareKeyboardListener.KeyUpReceived += (sender, eventArgs) =>
        {
            var webViewWithKeyUp = _canvasWebViewPrefab.WebView as IWithKeyDownAndUp;
            if (webViewWithKeyUp != null) webViewWithKeyUp.KeyUp(eventArgs.Value, eventArgs.Modifiers);
        };
    }
}