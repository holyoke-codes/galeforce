﻿using UnityEngine;
using Cinemachine;

public class MouseMovement : MonoBehaviour
{
    private const float _maxX = 300f;
    private const float _maxY = 2f;
    public float MouseSensitivity = 50;
    public Transform PlayerTransform;
    public bool TrackingMouse = true;
    public bool AtWorkstation = false;

    private float _xRotation;

    public bool _isFreeLooking = true;
    private CinemachineVirtualCamera _firstPersonCam;
    private CinemachineFreeLook _freeLookCam;
    
    // Start is called before the first frame update
    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;

        _firstPersonCam = transform.Find("CM vcam1").GetComponent<CinemachineVirtualCamera>();
        _freeLookCam = transform.Find("CM FreeLook1").GetComponent<CinemachineFreeLook>();
        _freeLookCam.m_YAxis.m_MaxSpeed = _maxY;
        _freeLookCam.m_XAxis.m_MaxSpeed = _maxX;
    }

    public void UnlockMouse()
    {
        if (_freeLookCam)
        {
            Cursor.lockState = CursorLockMode.None;
            _freeLookCam.m_YAxis.m_MaxSpeed = 0;
            _freeLookCam.m_XAxis.m_MaxSpeed = 0;
            TrackingMouse = false;
        }
    }

    public void LockMouse()
    {
        if (_freeLookCam)
        {
            Cursor.lockState = CursorLockMode.Locked;
            _freeLookCam.m_YAxis.m_MaxSpeed = _maxY;
            _freeLookCam.m_XAxis.m_MaxSpeed = _maxX;
            TrackingMouse = true;
        }
    }

    // Update is called once per frame
    private void Update()
    {
        if (AtWorkstation) return;

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (TrackingMouse)
            {
                UnlockMouse();
                Debug.Log("unlocking");
            }
            else
            {
                LockMouse();
                Debug.Log("locking");
            }
        }

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (_isFreeLooking)
            {
                _freeLookCam.enabled = false;
                _firstPersonCam.enabled = true;
                _isFreeLooking = false;
            }
            else
            {

                _freeLookCam.enabled = true;
                _firstPersonCam.enabled = false;
                _isFreeLooking = true;
            }
        }

        if (!TrackingMouse) return;

        var mouseX = Input.GetAxis("Mouse X") * MouseSensitivity * Time.deltaTime;
        var mouseY = Input.GetAxis("Mouse Y") * MouseSensitivity * Time.deltaTime;

        _xRotation -= mouseY;

        _xRotation = Mathf.Clamp(_xRotation, -45, 45);

        transform.localRotation = Quaternion.Euler(_xRotation, 0f, 0f);
        PlayerTransform.Rotate(Vector3.up * mouseX);
    }
}