﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerInstance : MonoBehaviour
{

    public static PlayerInstance Instance { get; private set; }

    public string LastScene = "";
    public string CurrentScene = "";
    public string ActiveTeam = "";

    public string PreviousRoom = "";
    public bool GoingToMap = false;

    private void Awake()
    {
        if (Instance == null) { Instance = this; }
        else { Destroy(gameObject); }
        DontDestroyOnLoad(this.gameObject);
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log($"Entered {scene.name} from {PreviousRoom}");
        StartCoroutine(SetPosition());
    }

    private IEnumerator SetPosition()
    {

        // Team1 - -7 2.1 -15.6
        // Team2 - -16 2.1 -13.4
        // Team3 - -26 2.1 -15.6
        // Team4 - -35.3 2.1 -13.4
        // Back from Map - -36 2.1 14.5

        switch (PreviousRoom)
        {
            case "":
                transform.position = new Vector3(-3.15f, 1.5f, 0.04f);
                transform.localScale = new Vector3(1f, 1f, 1f);
                break;
            case "ControlRoom_Team1":
                transform.position = new Vector3(-7f, 2.1f, -15.6f);
                transform.localScale = new Vector3(1f, 1f, 1f);
                break;
            case "ControlRoom_Team2":
                transform.position = new Vector3(-16f, 2.1f, -13.4f);
                transform.localScale = new Vector3(1f, 1f, 1f);
                break;
            case "ControlRoom_Team3":
                transform.position = new Vector3(-26f, 2.1f, -15.6f);
                transform.localScale = new Vector3(1f, 1f, 1f);
                break;
            case "ControlRoom_Team4":
                transform.position = new Vector3(-35.3f, 2.1f, -13.4f);
                transform.localScale = new Vector3(1f, 1f, 1f);
                break;
            case "ControlRoomTeamMap":
                transform.position = new Vector3(-36f, 2.1f, -14.5f);
                transform.localScale = new Vector3(1f, 1f, 1f);
                break;
            case "TrainingComplex":
                transform.position = new Vector3(-6.2f, 1.5f, 0.04f);
                transform.localScale = new Vector3(1f, 1f, 1f);
                break;
            default:
                transform.position = new Vector3(-3.15f, 1.5f, 0.04f);
                transform.localScale = new Vector3(1f, 1f, 1f);
                break;
        }

        if (GoingToMap && PreviousRoom == "TrainingComplex")
        {
            transform.position = new Vector3(-41f, 0.6f, -110.33f);
            transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            GoingToMap = false;
        }

        yield return new WaitForSeconds(1f);
        GetComponent<GravityScript>().enabled = true;
    }
}
