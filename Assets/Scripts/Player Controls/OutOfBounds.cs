﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class OutOfBounds : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Debug.LogError("Out of bounds: Resetting position");
            other.transform.position = new Vector3(-3.15f, 1.5f, 0.04f);
            other.transform.localScale = new Vector3(1f, 1f, 1f);
        }
    }
}

