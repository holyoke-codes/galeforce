﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceCameraScript : MonoBehaviour
{
    private Camera _camera;
    private GameObject _fp;

    // Start is called before the first frame update
    void Start()
    {
        _fp = GameObject.Find("FPPlayer");
        _camera = _fp.transform.Find("Camera").GetComponent<Camera>();
    }

    void LateUpdate()
    {
        transform.LookAt(transform.position + _camera.transform.rotation * Vector3.forward, _camera.transform.rotation * Vector3.up);
    }
}
