﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityScript : MonoBehaviour
{
    public float JumpHeight = 100f;

    private CharacterController _characterController;
    private Animator _anim;
    private Vector3 _velocity;
    private MouseMovement _mouseMovement;
    public bool Grounded = false;

    // Start is called before the first frame update
    void Start()
    {
        _characterController = transform.GetComponent<CharacterController>();
        _mouseMovement = GetComponentInChildren<MouseMovement>();
    }

    // Update is called once per frame
    void Update()
    {

        if (!_anim)
        {
            // The player model with animator doesn't exist when start is called
            // so get it here in update.
            foreach (Transform child in transform)
            {
                if (child.tag != "person") continue;
                _anim = child.GetComponent<Animator>();
                break;
            }
        }

        if (!_anim) return;

        Grounded = _characterController.isGrounded;
        if (Grounded && _velocity.y < 0)
        {
            _velocity.y = 0f;
        }

        if (!_mouseMovement.AtWorkstation)
        {
            if (Input.GetKeyDown(KeyCode.Space) && Grounded)
            {
                _anim.SetTrigger("Jump");
                _velocity.y += Mathf.Sqrt(JumpHeight * -3.0f * Physics.gravity.y);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha7) && Grounded)
            {
                // TODO: Backflip is always grounded. Make it jump while flipping.
                _anim.SetTrigger("Backflip");
            }
        }

        _velocity.y += Physics.gravity.y * Time.deltaTime;
        _characterController.Move(_velocity * Time.deltaTime);
    }
}
