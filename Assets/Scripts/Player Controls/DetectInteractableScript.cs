﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DetectInteractableScript : MonoBehaviour
{
    public float ClickDistance = 6f;
    public float FadeTime = 100f;
    public Camera PlayerCamera;

    public Color defaultColor;
    public Color hoverColor;

    private Image _image;
    private MouseMovement _mouse;

    private void Start()
    {
        _image = transform.GetComponent<Image>();
        _mouse = PlayerCamera.GetComponent<MouseMovement>();
    }

    private void Update()
    {
        if (_mouse._isFreeLooking)
        {
            // transparent
            _image.color = new Color(0, 0, 0, 0);
            return;
        }

        Ray ray = PlayerCamera.ScreenPointToRay(Input.mousePosition);
        bool _interactable = false;

        if (Physics.Raycast(ray, out var hit, ClickDistance))
        {
            if (hit.transform.parent)
            {
                if (hit.transform.parent.name.StartsWith("Station"))
                {
                    _interactable = true;
                }
                if (hit.transform.parent.name.StartsWith("WebView"))
                {
                    _interactable = true;
                }
            }
            if (hit.collider.name == "DisplayButton")
            {
                _interactable = true;
            }
        }
        if (_interactable)
        {
            _image.color = Color.Lerp(_image.color, hoverColor, FadeTime * Time.deltaTime);
        }
        else
        {
            _image.color = Color.Lerp(_image.color, defaultColor, FadeTime * Time.deltaTime);
        }
    }
}
