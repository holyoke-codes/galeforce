﻿using UnityEngine;

public class RotatePlayer : MonoBehaviour
{
    private bool _hasStepped;

    public int Step = 15;

    // Update is called once per frame
    private void Update()
    {
        var horizontal = Input.GetAxis("Horizontal");

        if (horizontal <= -0.95)
        {
            if (_hasStepped) return;
            gameObject.transform.Rotate(new Vector3(0, -Step, 0));
            _hasStepped = true;
        }
        else if (horizontal >= 0.95)
        {
            if (_hasStepped) return;
            gameObject.transform.Rotate(new Vector3(0, Step, 0));
            _hasStepped = true;
        }
        else
        {
            _hasStepped = false;
        }
    }
}