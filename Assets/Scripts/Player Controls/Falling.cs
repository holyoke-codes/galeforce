﻿using UnityEngine;

public class Falling : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        if (transform.position.y < -100)
        {
            transform.position = new Vector3(-3.15f, 1.5f, 0.04f);
            transform.localScale = new Vector3(1f, 1f, 1f);
        }
    }
}
