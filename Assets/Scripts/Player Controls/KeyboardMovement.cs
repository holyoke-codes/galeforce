﻿using MyBox;
using UnityEngine;

public class KeyboardMovement : MonoBehaviour
{
    public Animator CharacterAnim;

    public CharacterController Controller;

    public float Speed = 2.5f;
    public float RunSpeed = 8f;
    public float rotSpeed = 10;

    private Transform _cam;
    private MouseMovement _mouseMovement;
    private float _walkSpeed;
    private bool _isRunning;
    private bool _isKneeling;

    // starting value for the speed lerp
    static float t = 0.0f;

    private void Start()
    {
        _cam = transform.Find("Camera").GetComponent<Transform>();
        _mouseMovement = transform.Find("Camera").GetComponent<MouseMovement>();
        _walkSpeed = Speed;
    }

    // Update is called once per frame

    private void Update()
    {
        if (CharacterAnim != null)
        {
            // Use LeftShift to toggle running
            if (Input.GetKeyDown(KeyCode.LeftShift))
            {
                if (_isRunning)
                {
                    _isRunning = false;
                    CharacterAnim.ResetTrigger("IsRunning");
                    Speed = _walkSpeed;
                    t = 0;
                }
                else
                {
                    _isRunning = true;
                    CharacterAnim.SetTrigger("IsRunning");
                }
            }
            // Gradually increase speed when running
            if (_isRunning)
            {
                if (Input.GetAxis("Vertical") != 0)
                {
                    Speed = Mathf.Lerp(_walkSpeed, RunSpeed, t);
                    t += 0.25f * Time.deltaTime;
                }
                else
                {
                    // Reset speed when not moving forward
                    Speed = _walkSpeed;
                }
            }


            // Animation Code!
            // Jump and backflip animations are initiated in GravityScript

            if (Input.GetKeyDown("1"))
            {
                CharacterAnim.SetTrigger("Silly Dance");
            }

            if (Input.GetKeyDown("2"))
            {
                CharacterAnim.SetTrigger("Twist Dance");
            }

            if (Input.GetKeyDown("3"))
            {
                CharacterAnim.SetTrigger("Step Dance");
            }

            if (Input.GetKeyDown("4"))
            {
                CharacterAnim.SetTrigger("Waving");
            }

            if (Input.GetKeyDown("5"))
            {
                CharacterAnim.SetTrigger("Cheering");
            }

            if (Input.GetKeyDown("6"))
            {
                CharacterAnim.SetTrigger("Fist Pump");
            }
        }
    }

    private void FixedUpdate()
    {
        var x = Input.GetAxis("Horizontal");
        var z = Input.GetAxis("Vertical");

        if (CharacterAnim != null)
        {
            CharacterAnim.SetFloat("InputH", x);
            CharacterAnim.SetFloat("InputV", z);
        }

        var move = transform.right * x + transform.forward * z;

        if (move.magnitude > 1) move.Normalize();

        if (move != Vector3.zero)
        {

            if (_mouseMovement._isFreeLooking && _mouseMovement.TrackingMouse)
            {
                // Rotate to face the direction the camera is looking
                Vector3 dir = _cam.forward;
                dir.y = 0;
                Quaternion direction = Quaternion.LookRotation(dir);

                transform.rotation = Quaternion.Lerp(transform.rotation, direction, rotSpeed * Time.deltaTime);
            }
            // Move, unless you are kneeling
            if (!_isKneeling)
            {
                Controller.Move(move * Speed * Time.deltaTime);
            }
        }
        else
        {
            // You can kneel when you aren't moving
            if (Input.GetKey(KeyCode.C))
            {
                if (!_isKneeling)
                {
                    CharacterAnim?.ResetTrigger("Stand");
                    CharacterAnim?.SetTrigger("Kneel");
                    _isKneeling = true;
                }
            }
            else
            {
                if (_isKneeling)
                {
                    CharacterAnim?.ResetTrigger("Kneel");
                    CharacterAnim?.SetTrigger("Stand");
                    _isKneeling = false;
                }
            }
        }
    }
}
