﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitGame : MonoBehaviour
{
    void OnGUI()
    {
        // Quit game with Ctrl-Q or Cmd-Q
        if (!Event.current.isKey || Event.current.keyCode == KeyCode.None) return;
        switch (Event.current.type)
        {
            case EventType.KeyDown:
                if (Event.current.modifiers == EventModifiers.Control || Event.current.modifiers == EventModifiers.Command)
                {
                    switch (Event.current.keyCode)
                    {
                        case KeyCode.Q:
                            Application.Quit();
                            break;
                        default:
                            break;
                    }
                }
                break;
            default:
                break;
        }
    }
}