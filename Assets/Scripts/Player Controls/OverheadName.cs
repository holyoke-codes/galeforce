﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Photon.Pun;

public class OverheadName : MonoBehaviour
{

    private PlayerNetworkStyle _playerStyle;
    public TMP_Text OverheadText;
    private PhotonView _pv;
    private NetworkPlayerMissionIdHandler _networkPlayerMissionIdHandler;

    // Start is called before the first frame update
    public void Start()
    {
        _playerStyle = GetComponent<PlayerNetworkStyle>();

        _pv = GetComponent<PhotonView>();
        if (_pv.IsMine)
            gameObject.tag = "myNetworkPlayer";

        _networkPlayerMissionIdHandler = GetComponent<NetworkPlayerMissionIdHandler>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_pv.IsMine)
            gameObject.tag = "myNetworkPlayer";
        string missionId = _networkPlayerMissionIdHandler.MissionId;
        if (missionId == "")
        {
            OverheadText.text = _playerStyle.PlayerDisplayName;
        }
        else
        {
            OverheadText.text = _playerStyle.PlayerDisplayName + " (" + _networkPlayerMissionIdHandler.MissionId + ")";
        }
    }
}
