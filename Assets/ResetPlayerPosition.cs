﻿using System.Collections.Generic;
using UnityEngine;

public class ResetPlayerPosition : MonoBehaviour
{
    private GravityScript _grav;

    private void Start()
    {
        _grav = GetComponent<GravityScript>();
    }
    void OnGUI()
    {
        // Reset position to start point with Ctrl/CMD-T
        if (!Event.current.isKey || Event.current.keyCode == KeyCode.None) return;
        switch (Event.current.type)
        {
            case EventType.KeyDown:
                if (Event.current.modifiers == EventModifiers.Control || Event.current.modifiers == EventModifiers.Command)
                {
                    switch (Event.current.keyCode)
                    {
                        case KeyCode.T:
                            Debug.Log("ctrl-t");
                            _grav.enabled = false;
                            StartCoroutine(waiter());
                            break;
                        default:
                            break;
                    }
                }
                break;
            default:
                break;
        }
    }

    private IEnumerator<WaitForSeconds> waiter()
    {
        yield return new WaitForSeconds(0.5f);

        transform.position = new Vector3(-3.15f, 1.5f, 0.04f);
        transform.localScale = new Vector3(1f, 1f, 1f);
        _grav.enabled = true;
    }
}